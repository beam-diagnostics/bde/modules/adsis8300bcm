#=================================================================#
# Template file: sis8300bcm-lut-common.template
# Database for the records specific to the common beam mode
# beam destination LUT parameters
# Hinko Kocevar
# November 26, 2019

record(stringout, "$(P)$(R)Lut$(N)ModeName")
{
    field(VAL,  "$(MODE_NAME)")
    info(autosaveFields, "VAL")
}

record(longout, "$(P)$(R)Lut$(N)ModeID")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MODE_ID")
    field(VAL,  "0")
    field(ASG,  "bcm-critical")
    info(autosaveFieldsLutIDs, "VAL")
    field(FLNK, "$(P)$(R)Lut$(N)ModeIDSeq.PROC PP MS")
}

# Mode ID will trigger writing update on:
# 1 - Minimum Repetition Period 
# 2 - Upper Threshold (per ACCT channel)
# 3 - Lower Threshold (per ACCT channel)
# 4 - Max Pulse Length (per ACCT channel)
record(seq, "$(P)$(R)Lut$(N)ModeIDSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(P)$(R)Lut$(N)MinTriggerPeriod.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)Lut$(N)AcctUpperThresholdSeq.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)Lut$(N)AcctLowerThresholdSeq.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)Lut$(N)AcctMaxPulseLengthSeq.PROC PP MS")
}

record(seq, "$(P)$(R)Lut$(N)AcctUpperThresholdSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(SYSTEM1_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(SYSTEM2_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(SYSTEM3_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(SYSTEM4_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL5, "1")
    field(LNK5, "$(SYSTEM5_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL6, "1")
    field(LNK6, "$(SYSTEM6_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL7, "1")
    field(LNK7, "$(SYSTEM7_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL8, "1")
    field(LNK8, "$(SYSTEM8_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOL9, "1")
    field(LNK9, "$(SYSTEM9_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
    field(DOLA, "1")
    field(LNKA, "$(SYSTEM10_PREFIX)Lut$(N)UpperThreshold.PROC PP MS")
}

record(seq, "$(P)$(R)Lut$(N)AcctLowerThresholdSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(SYSTEM1_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(SYSTEM2_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(SYSTEM3_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(SYSTEM4_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL5, "1")
    field(LNK5, "$(SYSTEM5_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL6, "1")
    field(LNK6, "$(SYSTEM6_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL7, "1")
    field(LNK7, "$(SYSTEM7_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL8, "1")
    field(LNK8, "$(SYSTEM8_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOL9, "1")
    field(LNK9, "$(SYSTEM9_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
    field(DOLA, "1")
    field(LNKA, "$(SYSTEM10_PREFIX)Lut$(N)LowerThreshold.PROC PP MS")
}

record(seq, "$(P)$(R)Lut$(N)AcctMaxPulseLengthSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(SYSTEM1_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(SYSTEM2_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(SYSTEM3_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(SYSTEM4_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL5, "1")
    field(LNK5, "$(SYSTEM5_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL6, "1")
    field(LNK6, "$(SYSTEM6_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL7, "1")
    field(LNK7, "$(SYSTEM7_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL8, "1")
    field(LNK8, "$(SYSTEM8_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOL9, "1")
    field(LNK9, "$(SYSTEM9_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
    field(DOLA, "1")
    field(LNKA, "$(SYSTEM10_PREFIX)Lut$(N)MaxPulseLength.PROC PP MS")
}

record(longin, "$(P)$(R)Lut$(N)ModeIDR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MODE_ID")
    field(SCAN, "I/O Intr")
}

record(stringout, "$(P)$(R)Lut$(N)DestinationName")
{
    field(VAL,  "$(DEST_NAME)")
    info(autosaveFields, "VAL")
}

record(longout, "$(P)$(R)Lut$(N)DestinationID")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).DEST_ID")
    field(VAL,  "0")
    field(ASG,  "bcm-critical")
    info(autosaveFieldsLutIDs, "VAL")
    field(FLNK, "$(P)$(R)Lut$(N)DestinationIDSeq.PROC PP MS")
}

# Destination ID will trigger writing update
# on both Differential LUT Interlock Enable
# and ACCT LUT Beam Exists PVs
record(seq, "$(P)$(R)Lut$(N)DestinationIDSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(P)$(R)Lut$(N)DiffInterlockEnableSeq.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)Lut$(N)AcctBeamExistsSeq.PROC PP MS")
}

record(seq, "$(P)$(R)Lut$(N)DiffInterlockEnableSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(P)$(R)DF1-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)DF2-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)DF3-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)DF4-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL5, "1")
    field(LNK5, "$(P)$(R)DF5-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)DF6-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL7, "1")
    field(LNK7, "$(P)$(R)DF7-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL8, "1")
    field(LNK8, "$(P)$(R)DF8-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOL9, "1")
    field(LNK9, "$(P)$(R)DF9-Lut$(N)InterlockEnable.PROC PP MS")
    field(DOLA, "1")
    field(LNKA, "$(P)$(R)DF10-Lut$(N)InterlockEnable.PROC PP MS")
}

record(seq, "$(P)$(R)Lut$(N)AcctBeamExistsSeq")
{
    field(DOL1, "1")
    field(LNK1, "$(SYSTEM1_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL2, "1")
    field(LNK2, "$(SYSTEM2_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL3, "1")
    field(LNK3, "$(SYSTEM3_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL4, "1")
    field(LNK4, "$(SYSTEM4_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL5, "1")
    field(LNK5, "$(SYSTEM5_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL6, "1")
    field(LNK6, "$(SYSTEM6_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL7, "1")
    field(LNK7, "$(SYSTEM7_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL8, "1")
    field(LNK8, "$(SYSTEM8_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOL9, "1")
    field(LNK9, "$(SYSTEM9_PREFIX)Lut$(N)BeamExists.PROC PP MS")
    field(DOLA, "1")
    field(LNKA, "$(SYSTEM10_PREFIX)Lut$(N)BeamExists.PROC PP MS")
}

record(longin, "$(P)$(R)Lut$(N)DestinationIDR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).DEST_ID")
    field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)Lut$(N)MinTriggerPeriod")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MIN_TRIGGER_PERIOD")
    field(EGU,  "ms")
    field(PREC, "4")
    field(VAL,  "0.0")
    field(ASG,  "bcm-critical")
    info(autosaveFields, "VAL")
}

record(ai, "$(P)$(R)Lut$(N)MinTriggerPeriodR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.LUT$(N).MIN_TRIGGER_PERIOD")
    field(EGU,  "ms")
    field(PREC, "4")
    field(SCAN, "I/O Intr")
}
