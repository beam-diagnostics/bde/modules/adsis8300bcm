/* sis8300bcm.cpp
 *
 * This is a driver for a BCM based on Struck SIS8300 digitizer.
 *
 * Author: Hinko Kocevar
 *         Joao Paulo Martins
 *         ESS ERIC, Lund, Sweden
 *
 * Created:  October 8, 2018
 *
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <string>
#include <stdarg.h>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <iocsh.h>

#include <asynNDArrayDriver.h>
#include <epicsExport.h>

#include <sis8300bcm_reg.h>
#include <sis8300bcm.h>
#include <sis8300.h>


static const char *driverName = "sis8300bcm";

/* asyn address definition
 *  0        acq 1,  raw 1 data,  acct 1,  diff 1, probe 1, sis8300, sis8300bcm
 *  1        acq 2,  raw 2 data,  acct 2,  diff 2, probe 2
 *  2        acq 3,  raw 3 data,  acct 3,  diff 3, probe 3
 *  3        acq 4,  raw 4 data,  acct 4,  diff 4, probe 4
 *  4        acq 5,  raw 5 data,  acct 5,  diff 5
 *  5        acq 6,  raw 6 data,  acct 6,  diff 6
 *  6        acq 7,  raw 7 data,  acct 7,  diff 7
 *  7        acq 8,  raw 8 data,  acct 8,  diff 8
 *  8        acq 9,  raw 9 data,  acct 9,  diff 9
 *  9        acq 10, raw 10 data, acct 10, diff 10
 *  10       acq 11, processed 1 data
 *  11       acq 12, processed 2 data
 *  12       acq 13, processed 3 data
 *  13       acq 14, processed 4 data
 *  14       acq 15, processed 5 data
 *  15       acq 16, processed 6 data
 *  16       acq 17, processed 7 data
 *  17       acq 18, processed 8 data
 *  18       acq 19, processed 9 data
 *  19       acq 20, processed 10 data
 *  20       acq 21, probe 1 1 data
 *  21       acq 22, probe 1 2 data
 *  22       acq 23, probe 1 3 data
 *  23       acq 24, probe 1 4 data
 *  24       probe 1 5 data
 *  25       probe 1 6 data
 *  26       probe 1 7 data
 *  27       probe 1 8 data
 *  28       probe 1 9 data
 *  29       probe 1 10 data
 *  30       probe 2 1 data
 *  31       probe 2 2 data
 *  32       probe 2 3 data
 *  33       probe 2 4 data
 *  34       probe 2 5 data
 *  35       probe 2 6 data
 *  36       probe 2 7 data
 *  37       probe 2 8 data
 *  38       probe 2 9 data
 *  39       probe 2 10 data
 *  40       probe 3 1 data
 *  41       probe 3 2 data
 *  42       probe 3 3 data
 *  43       probe 3 4 data
 *  44       probe 3 5 data
 *  45       probe 3 6 data
 *  46       probe 3 7 data
 *  47       probe 3 8 data
 *  48       probe 3 9 data
 *  49       probe 3 10 data
 *  50       probe 4 1 data
 *  51       probe 4 2 data
 *  52       probe 4 3 data
 *  53       probe 4 4 data
 *  54       probe 4 5 data
 *  55       probe 4 6 data
 *  56       probe 4 7 data
 *  57       probe 4 8 data
 *  58       probe 4 9 data
 *  59       probe 4 10 data
 */

/** Constructor for sis8300bcm; most parameters are simply passed to sis8300::sis8300.
  * After calling the base class constructor this method creates a thread to compute the simulated detector data,
  * and sets reasonable default values for parameters defined in this class and SIS8300.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] devicePath The path to the /dev entry.
  * \param[in] numSamples The initial number of samples.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
sis8300bcm::sis8300bcm(const char *portName, const char *devicePath,
        int numSamples, int maxBuffers, size_t maxMemory,
        int priority, int stackSize)

    : sis8300(portName, devicePath,
            numSamples,
            50, // need 50 extra ports on top of 10 sis8300 default
            maxBuffers, maxMemory,
            priority, stackSize)

{
    // BCM system wide parameters
    createParam(BCMFwVersionString,                         asynParamInt32,     &BCMFwVersion);
    createParam(BCMFwGitHashString,                         asynParamInt32,     &BCMFwGitHash);
    createParam(BCMClockFrequencyMeasString,                asynParamFloat64,   &BCMClockFrequencyMeas);
    createParam(BCMTriggerPeriodMeasString,                 asynParamFloat64,   &BCMTriggerPeriodMeas);
    createParam(BCMTriggerWidthMeasString,                  asynParamFloat64,   &BCMTriggerWidthMeas);
    createParam(BCMAcqCfgMemoryAddressString,               asynParamInt32,     &BCMAcqCfgMemoryAddress);
    createParam(BCMAcqCfgNumSamplesString,                  asynParamInt32,     &BCMAcqCfgNumSamples);
    createParam(BCMAcqCfgFractionBitsString,                asynParamInt32,     &BCMAcqCfgFractionBits);
    createParam(BCMAcqCfgFactorString,                      asynParamFloat64,   &BCMAcqCfgFactor);
    createParam(BCMAcqCfgOffsetString,                      asynParamFloat64,   &BCMAcqCfgOffset);
    createParam(BCMAcqCfgDecimationString,                  asynParamInt32,     &BCMAcqCfgDecimation);
    createParam(BCMAcqCfgScalingString,                     asynParamInt32,     &BCMAcqCfgScaling);
    createParam(BCMAcqCfgConvertingString,                  asynParamInt32,     &BCMAcqCfgConverting);
    createParam(BCMAcqCfgRecordingString,                   asynParamInt32,     &BCMAcqCfgRecording);
    createParam(BCMAcqCfgTickValueString,                   asynParamFloat64,   &BCMAcqCfgTickValue);
    createParam(BCMMinTriggerPeriodString,                  asynParamFloat64,   &BCMMinTriggerPeriod);
    createParam(BCMMaxPulseWidthString,                     asynParamInt32,     &BCMMaxPulseWidth);
    createParam(BCMAlarmsClearString,                       asynParamInt32,     &BCMAlarmsClear);
    createParam(BCMAlarmsControlString,                     asynParamInt32,     &BCMAlarmsControl);
    createParam(BCMAlarmAuxiliaryClockControlString,        asynParamInt32,     &BCMAlarmAuxiliaryClockControl);
    createParam(BCMAlarmProcessingClockControlString,       asynParamInt32,     &BCMAlarmProcessingClockControl);
    createParam(BCMAlarmTriggerTooWideControlString,        asynParamInt32,     &BCMAlarmTriggerTooWideControl);
    createParam(BCMAlarmTriggerTooNarrowControlString,      asynParamInt32,     &BCMAlarmTriggerTooNarrowControl);
    createParam(BCMAlarmTriggerPeriodTooShortControlString, asynParamInt32,     &BCMAlarmTriggerPeriodTooShortControl);
    createParam(BCMAlarmAuxiliaryClockHoldString,           asynParamInt32,     &BCMAlarmAuxiliaryClockHold);
    createParam(BCMAlarmProcessingClockHoldString,          asynParamInt32,     &BCMAlarmProcessingClockHold);
    createParam(BCMAlarmTriggerTooWideHoldString,           asynParamInt32,     &BCMAlarmTriggerTooWideHold);
    createParam(BCMAlarmTriggerTooNarrowHoldString,         asynParamInt32,     &BCMAlarmTriggerTooNarrowHold);
    createParam(BCMAlarmTriggerPeriodTooShortHoldString,    asynParamInt32,     &BCMAlarmTriggerPeriodTooShortHold);
    createParam(BCMAlarmLUTMinTriggerPeriodHoldString,      asynParamInt32,     &BCMAlarmLUTMinTriggerPeriodHold);
    createParam(BCMAlarmAuxiliaryClockFirstString,          asynParamInt32,     &BCMAlarmAuxiliaryClockFirst);
    createParam(BCMAlarmProcessingClockFirstString,         asynParamInt32,     &BCMAlarmProcessingClockFirst);
    createParam(BCMAlarmTriggerTooWideFirstString,          asynParamInt32,     &BCMAlarmTriggerTooWideFirst);
    createParam(BCMAlarmTriggerTooNarrowFirstString,        asynParamInt32,     &BCMAlarmTriggerTooNarrowFirst);
    createParam(BCMAlarmTriggerPeriodTooShortFirstString,   asynParamInt32,     &BCMAlarmTriggerPeriodTooShortFirst);
    createParam(BCMAlarmLUTMinTriggerPeriodFirstString,     asynParamInt32,     &BCMAlarmLUTMinTriggerPeriodFirst);
    createParam(BCMAlarmAuxiliaryClockDirectString,         asynParamInt32,     &BCMAlarmAuxiliaryClockDirect);
    createParam(BCMAlarmProcessingClockDirectString,        asynParamInt32,     &BCMAlarmProcessingClockDirect);
    createParam(BCMAlarmTriggerTooWideDirectString,         asynParamInt32,     &BCMAlarmTriggerTooWideDirect);
    createParam(BCMAlarmTriggerTooNarrowDirectString,       asynParamInt32,     &BCMAlarmTriggerTooNarrowDirect);
    createParam(BCMAlarmTriggerPeriodTooShortDirectString,  asynParamInt32,     &BCMAlarmTriggerPeriodTooShortDirect);
    createParam(BCMAlarmLUTMinTriggerPeriodDirectString,    asynParamInt32,     &BCMAlarmLUTMinTriggerPeriodDirect);
    createParam(BCMAlarmDiffLUTDestinationModeDirectString, asynParamInt32,     &BCMAlarmDiffLUTDestinationModeDirect);
    createParam(BCMMainClockMissingString,                  asynParamInt32,     &BCMMainClockMissing);
    createParam(BCMAuxiliaryClockMissingString,             asynParamInt32,     &BCMAuxiliaryClockMissing);
    createParam(BCMReadyString,                             asynParamInt32,     &BCMReady);
    createParam(BCMLUTControlString,                        asynParamInt32,     &BCMLUTControl);
    createParam(BCMAcquisitionTriggerSourceString,          asynParamInt32,     &BCMAcquisitionTriggerSource);
    createParam(BCMCrateIDString,                           asynParamInt32,     &BCMCrateID);
    createParam(BCMPulseWidthFilterString,                  asynParamFloat64,   &BCMPulseWidthFilter);
    createParam(BCMBeamTriggerSourceString,                 asynParamInt32,     &BCMBeamTriggerSource);
    createParam(BCMReadyFPGAString,                         asynParamInt32,     &BCMReadyFPGA);
    createParam(BCMBeamPermitFPGAString,                    asynParamInt32,     &BCMBeamPermitFPGA);
    createParam(BCMHighVoltagePresenceString,               asynParamInt32,     &BCMHighVoltagePresence);
    createParam(BCMHighVoltageOKString,                     asynParamInt32,     &BCMHighVoltageOK);
    createParam(BCMEnableCalibrationPulseString,            asynParamInt32,     &BCMEnableCalibrationPulse);
    // ACCT block, channel specific
    createParam(BCMAcctTriggerSourceString,                 asynParamInt32,     &BCMAcctTriggerSource);
    createParam(BCMAcctPulseChargeString,                   asynParamFloat64,   &BCMAcctPulseCharge);
    createParam(BCMAcctFlatTopChargeString,                 asynParamFloat64,   &BCMAcctFlatTopCharge);
    createParam(BCMAcctPulseWidthString,                    asynParamFloat64,   &BCMAcctPulseWidth);
    createParam(BCMAcctAdcScaleString,                      asynParamFloat64,   &BCMAcctAdcScale);
    createParam(BCMAcctAdcOffsetString,                     asynParamInt32,     &BCMAcctAdcOffset);
    createParam(BCMAcctFlatTopStartString,                  asynParamFloat64,   &BCMAcctFlatTopStart);
    createParam(BCMAcctFlatTopEndString,                    asynParamFloat64,   &BCMAcctFlatTopEnd);
    createParam(BCMAcctFlatTopCurrentString,                asynParamFloat64,   &BCMAcctFlatTopCurrent);
    createParam(BCMAcctFineDelayString,                     asynParamFloat64,   &BCMAcctFineDelay);
    createParam(BCMAcctDroopRateString,                     asynParamFloat64,   &BCMAcctDroopRate);
    createParam(BCMAcctDroopCompensatingString,             asynParamInt32,     &BCMAcctDroopCompensating);
    createParam(BCMAcctNoiseFilteringString,                asynParamInt32,     &BCMAcctNoiseFiltering);
    createParam(BCMAcctBaseliningBeforeString,              asynParamInt32,     &BCMAcctBaseliningBefore);
    createParam(BCMAcctBaseliningAfterString,               asynParamInt32,     &BCMAcctBaseliningAfter);
    createParam(BCMAcctDcBlockingString,                    asynParamInt32,     &BCMAcctDcBlocking);
    createParam(BCMAcctUpperThresholdString,                asynParamFloat64,   &BCMAcctUpperThreshold);
    createParam(BCMAcctLowerThresholdString,                asynParamFloat64,   &BCMAcctLowerThreshold);
    createParam(BCMAcctErrantThresholdString,               asynParamFloat64,   &BCMAcctErrantThreshold);
    createParam(BCMAcctAlarmUpperControlString,             asynParamInt32,     &BCMAcctAlarmUpperControl);
    createParam(BCMAcctAlarmLowerControlString,             asynParamInt32,     &BCMAcctAlarmLowerControl);
    createParam(BCMAcctAlarmErrantControlString,            asynParamInt32,     &BCMAcctAlarmErrantControl);
    createParam(BCMAcctAlarmTriggerControlString,           asynParamInt32,     &BCMAcctAlarmTriggerControl);
    createParam(BCMAcctAlarmLimitControlString,             asynParamInt32,     &BCMAcctAlarmLimitControl);
    createParam(BCMAcctAlarmAdcOverflowControlString,       asynParamInt32,     &BCMAcctAlarmAdcOverflowControl);
    createParam(BCMAcctAlarmAdcUnderflowControlString,      asynParamInt32,     &BCMAcctAlarmAdcUnderflowControl);
    createParam(BCMAcctAlarmAdcStuckControlString,          asynParamInt32,     &BCMAcctAlarmAdcStuckControl);
    createParam(BCMAcctAlarmAiuFaultControlString,          asynParamInt32,     &BCMAcctAlarmAiuFaultControl);
    createParam(BCMAcctAlarmChargeTooHighControlString,     asynParamInt32,     &BCMAcctAlarmChargeTooHighControl);
    createParam(BCMAcctAlarmUpperHoldString,                asynParamInt32,     &BCMAcctAlarmUpperHold);
    createParam(BCMAcctAlarmLowerHoldString,                asynParamInt32,     &BCMAcctAlarmLowerHold);
    createParam(BCMAcctAlarmErrantHoldString,               asynParamInt32,     &BCMAcctAlarmErrantHold);
    createParam(BCMAcctAlarmTriggerHoldString,              asynParamInt32,     &BCMAcctAlarmTriggerHold);
    createParam(BCMAcctAlarmLimitHoldString,                asynParamInt32,     &BCMAcctAlarmLimitHold);
    createParam(BCMAcctAlarmAdcOverflowHoldString,          asynParamInt32,     &BCMAcctAlarmAdcOverflowHold);
    createParam(BCMAcctAlarmAdcUnderflowHoldString,         asynParamInt32,     &BCMAcctAlarmAdcUnderflowHold);
    createParam(BCMAcctAlarmAdcStuckHoldString,             asynParamInt32,     &BCMAcctAlarmAdcStuckHold);
    createParam(BCMAcctAlarmAiuFaultHoldString,             asynParamInt32,     &BCMAcctAlarmAiuFaultHold);
    createParam(BCMAcctAlarmChargeTooHighHoldString,        asynParamInt32,     &BCMAcctAlarmChargeTooHighHold);
    createParam(BCMAcctAlarmLUTUpperThresholdHoldString,    asynParamInt32,     &BCMAcctAlarmLUTUpperThresholdHold);
    createParam(BCMAcctAlarmLUTLowerThresholdHoldString,    asynParamInt32,     &BCMAcctAlarmLUTLowerThresholdHold);
    createParam(BCMAcctAlarmLUTPulseLengthHoldString,       asynParamInt32,     &BCMAcctAlarmLUTPulseLengthHold);
    createParam(BCMAcctAlarmLUTDestinationModeHoldString,   asynParamInt32,     &BCMAcctAlarmLUTDestinationModeHold);
    createParam(BCMAcctAlarmUpperFirstString,               asynParamInt32,     &BCMAcctAlarmUpperFirst);
    createParam(BCMAcctAlarmLowerFirstString,               asynParamInt32,     &BCMAcctAlarmLowerFirst);
    createParam(BCMAcctAlarmErrantFirstString,              asynParamInt32,     &BCMAcctAlarmErrantFirst);
    createParam(BCMAcctAlarmTriggerFirstString,             asynParamInt32,     &BCMAcctAlarmTriggerFirst);
    createParam(BCMAcctAlarmLimitFirstString,               asynParamInt32,     &BCMAcctAlarmLimitFirst);
    createParam(BCMAcctAlarmAdcOverflowFirstString,         asynParamInt32,     &BCMAcctAlarmAdcOverflowFirst);
    createParam(BCMAcctAlarmAdcUnderflowFirstString,        asynParamInt32,     &BCMAcctAlarmAdcUnderflowFirst);
    createParam(BCMAcctAlarmAdcStuckFirstString,            asynParamInt32,     &BCMAcctAlarmAdcStuckFirst);
    createParam(BCMAcctAlarmAiuFaultFirstString,            asynParamInt32,     &BCMAcctAlarmAiuFaultFirst);
    createParam(BCMAcctAlarmChargeTooHighFirstString,       asynParamInt32,     &BCMAcctAlarmChargeTooHighFirst);
    createParam(BCMAcctAlarmLUTUpperThresholdFirstString,   asynParamInt32,     &BCMAcctAlarmLUTUpperThresholdFirst);
    createParam(BCMAcctAlarmLUTLowerThresholdFirstString,   asynParamInt32,     &BCMAcctAlarmLUTLowerThresholdFirst);
    createParam(BCMAcctAlarmLUTPulseLengthFirstString,      asynParamInt32,     &BCMAcctAlarmLUTPulseLengthFirst);
    createParam(BCMAcctAlarmLUTDestinationModeFirstString,  asynParamInt32,     &BCMAcctAlarmLUTDestinationModeFirst);
    createParam(BCMAcctAlarmUpperDirectString,              asynParamInt32,     &BCMAcctAlarmUpperDirect);
    createParam(BCMAcctAlarmLowerDirectString,              asynParamInt32,     &BCMAcctAlarmLowerDirect);
    createParam(BCMAcctAlarmErrantDirectString,             asynParamInt32,     &BCMAcctAlarmErrantDirect);
    createParam(BCMAcctAlarmTriggerDirectString,            asynParamInt32,     &BCMAcctAlarmTriggerDirect);
    createParam(BCMAcctAlarmLimitDirectString,              asynParamInt32,     &BCMAcctAlarmLimitDirect);
    createParam(BCMAcctAlarmAdcOverflowDirectString,        asynParamInt32,     &BCMAcctAlarmAdcOverflowDirect);
    createParam(BCMAcctAlarmAdcUnderflowDirectString,       asynParamInt32,     &BCMAcctAlarmAdcUnderflowDirect);
    createParam(BCMAcctAlarmAdcStuckDirectString,           asynParamInt32,     &BCMAcctAlarmAdcStuckDirect);
    createParam(BCMAcctAlarmAiuFaultDirectString,           asynParamInt32,     &BCMAcctAlarmAiuFaultDirect);
    createParam(BCMAcctAlarmChargeTooHighDirectString,      asynParamInt32,     &BCMAcctAlarmChargeTooHighDirect);
    createParam(BCMAcctAlarmLUTUpperThresholdDirectString,  asynParamInt32,     &BCMAcctAlarmLUTUpperThresholdDirect);
    createParam(BCMAcctAlarmLUTLowerThresholdDirectString,  asynParamInt32,     &BCMAcctAlarmLUTLowerThresholdDirect);
    createParam(BCMAcctAlarmLUTPulseLengthDirectString,     asynParamInt32,     &BCMAcctAlarmLUTPulseLengthDirect);
    createParam(BCMAcctAlarmLUTDestinationModeDirectString, asynParamInt32,     &BCMAcctAlarmLUTDestinationModeDirect);
    createParam(BCMAcctBeamExistsString,                    asynParamInt32,     &BCMAcctBeamExists);
    createParam(BCMAcctMaxPulseLengthString,                asynParamInt32,     &BCMAcctMaxPulseLength);
    createParam(BCMAcctLowerWindowStartString,              asynParamFloat64,   &BCMAcctLowerWindowStart);
    createParam(BCMAcctLowerWindowEndString,                asynParamFloat64,   &BCMAcctLowerWindowEnd);
    createParam(BCMAcctErrantWindowStartString,             asynParamFloat64,   &BCMAcctErrantWindowStart);
    createParam(BCMAcctErrantWindowEndString,               asynParamFloat64,   &BCMAcctErrantWindowEnd);
    createParam(BCMAcctCalibrationSample1String,            asynParamFloat64,   &BCMAcctCalibrationSample1);
    createParam(BCMAcctCalibrationSample2String,            asynParamFloat64,   &BCMAcctCalibrationSample2);
    createParam(BCMAcctCalibrationSample3String,            asynParamFloat64,   &BCMAcctCalibrationSample3);
    createParam(BCMAcctCalibrationSample4String,            asynParamFloat64,   &BCMAcctCalibrationSample4);
    createParam(BCMAcctAuxUpperThresholdString,             asynParamFloat64,   &BCMAcctAuxUpperThreshold);
    createParam(BCMAcctAuxLowerThresholdString,             asynParamFloat64,   &BCMAcctAuxLowerThreshold);
    createParam(BCMAcctAuxHysteresisThresholdString,        asynParamFloat64,   &BCMAcctAuxHysteresisThreshold);
    createParam(BCMAcctBeamAboveThresholdString,            asynParamFloat64,   &BCMAcctBeamAboveThreshold);
    createParam(BCMAcctLeakyCoefficientString,              asynParamFloat64,   &BCMAcctLeakyCoefficient);
    createParam(BCMAcctLeakyThresholdString,                asynParamFloat64,   &BCMAcctLeakyThreshold);
    createParam(BCMAcctTriggerWidthString,                  asynParamFloat64,   &BCMAcctTriggerWidth);
    createParam(BCMAcctAutoFlatTopStartString,              asynParamFloat64,   &BCMAcctAutoFlatTopStart);
    createParam(BCMAcctAutoFlatTopEndString,                asynParamFloat64,   &BCMAcctAutoFlatTopEnd);
    createParam(BCMAcctAutoFlatTopEnableString,             asynParamInt32,     &BCMAcctAutoFlatTopEnable);
    createParam(BCMAcctBeamAbsenceString,                   asynParamInt32,     &BCMAcctBeamAbsence);
    // DIFF block, channel specific
    createParam(BCMDiffSourceAString,                       asynParamInt32,     &BCMDiffSourceA);
    createParam(BCMDiffSourceBString,                       asynParamInt32,     &BCMDiffSourceB);
    createParam(BCMDiffDelayString,                         asynParamFloat64,   &BCMDiffDelay);
    createParam(BCMDiffFastThresholdString,                 asynParamFloat64,   &BCMDiffFastThreshold);
    createParam(BCMDiffFastMinimumString,                   asynParamFloat64,   &BCMDiffFastMinimum);
    createParam(BCMDiffFastMaximumString,                   asynParamFloat64,   &BCMDiffFastMaximum);
    createParam(BCMDiffAlarmFastControlString,              asynParamInt32,     &BCMDiffAlarmFastControl);
    createParam(BCMDiffAlarmFastHoldString,                 asynParamInt32,     &BCMDiffAlarmFastHold);
    createParam(BCMDiffAlarmFastFirstString,                asynParamInt32,     &BCMDiffAlarmFastFirst);
    createParam(BCMDiffAlarmFastDirectString,               asynParamInt32,     &BCMDiffAlarmFastDirect);
    createParam(BCMDiffMediumThresholdString,               asynParamFloat64,   &BCMDiffMediumThreshold);
    createParam(BCMDiffMediumMinimumString,                 asynParamFloat64,   &BCMDiffMediumMinimum);
    createParam(BCMDiffMediumMaximumString,                 asynParamFloat64,   &BCMDiffMediumMaximum);
    createParam(BCMDiffAlarmMediumControlString,            asynParamInt32,     &BCMDiffAlarmMediumControl);
    createParam(BCMDiffAlarmMediumHoldString,               asynParamInt32,     &BCMDiffAlarmMediumHold);
    createParam(BCMDiffAlarmMediumFirstString,              asynParamInt32,     &BCMDiffAlarmMediumFirst);
    createParam(BCMDiffAlarmMediumDirectString,             asynParamInt32,     &BCMDiffAlarmMediumDirect);
    createParam(BCMDiffSlowThresholdString,                 asynParamFloat64,   &BCMDiffSlowThreshold);
    createParam(BCMDiffSlowMinimumString,                   asynParamFloat64,   &BCMDiffSlowMinimum);
    createParam(BCMDiffSlowMaximumString,                   asynParamFloat64,   &BCMDiffSlowMaximum);
    createParam(BCMDiffAlarmSlowControlString,              asynParamInt32,     &BCMDiffAlarmSlowControl);
    createParam(BCMDiffAlarmSlowHoldString,                 asynParamInt32,     &BCMDiffAlarmSlowHold);
    createParam(BCMDiffAlarmSlowFirstString,                asynParamInt32,     &BCMDiffAlarmSlowFirst);
    createParam(BCMDiffAlarmSlowDirectString,               asynParamInt32,     &BCMDiffAlarmSlowDirect);
    createParam(BCMDiffRisingWindowStartString,             asynParamFloat64,   &BCMDiffRisingWindowStart);
    createParam(BCMDiffRisingWindowEndString,               asynParamFloat64,   &BCMDiffRisingWindowEnd);
    createParam(BCMDiffFallingWindowStartString,            asynParamFloat64,   &BCMDiffFallingWindowStart);
    createParam(BCMDiffFallingWindowEndString,              asynParamFloat64,   &BCMDiffFallingWindowEnd);
    createParam(BCMDiffWsBetweenString,                     asynParamInt32,     &BCMDiffWsBetween);
    createParam(BCMDiffEmuBetweenString,                    asynParamInt32,     &BCMDiffEmuBetween);
    createParam(BCMDiffRfqBetweenString,                    asynParamInt32,     &BCMDiffRfqBetween);
    createParam(BCMDiffFastWindowWidthString,               asynParamFloat64,   &BCMDiffFastWindowWidth);
    createParam(BCMDiffMediumWindowWidthString,             asynParamFloat64,   &BCMDiffMediumWindowWidth);
    createParam(BCMDiffFastWindowWidthInverseString,        asynParamFloat64,   &BCMDiffFastWindowWidthInverse);
    createParam(BCMDiffMediumWindowWidthInverseString,      asynParamFloat64,   &BCMDiffMediumWindowWidthInverse);
    createParam(BCMDiffLeakyCoefficientString,              asynParamFloat64,   &BCMDiffLeakyCoefficient);
    createParam(BCMDiffLeakyThresholdString,                asynParamFloat64,   &BCMDiffLeakyThreshold);
    createParam(BCMDiffAlarmLeakyControlString,             asynParamInt32,     &BCMDiffAlarmLeakyControl);
    createParam(BCMDiffAlarmLeakyHoldString,                asynParamInt32,     &BCMDiffAlarmLeakyHold);
    createParam(BCMDiffAlarmLeakyFirstString,               asynParamInt32,     &BCMDiffAlarmLeakyFirst);
    createParam(BCMDiffAlarmLeakyDirectString,              asynParamInt32,     &BCMDiffAlarmLeakyDirect);
    createParam(BCMDiffInterlockEnableString,               asynParamInt32,     &BCMDiffInterlockEnable);
    // PROBE block, channel specific
    createParam(BCMProbeChannelString,                      asynParamInt32,     &BCMProbeChannel);
    createParam(BCMProbeSourceString,                       asynParamInt32,     &BCMProbeSource);
    // FIBER block, channel specific
    createParam(BCMFiberOutDataSelectString,                asynParamInt32,     &BCMFiberOutDataSelect);
    createParam(BCMFiberOutDataEnableString,                asynParamInt32,     &BCMFiberOutDataEnable);
    createParam(BCMFiberSFPPresentString,                   asynParamInt32,     &BCMFiberSFPPresent);
    createParam(BCMFiberLaneUpString,                       asynParamInt32,     &BCMFiberLaneUp);
    createParam(BCMFiberChannelUpString,                    asynParamInt32,     &BCMFiberChannelUp);
    createParam(BCMFiberHardwareErrorString,                asynParamInt32,     &BCMFiberHardwareError);
    createParam(BCMFiberSoftwareErrorString,                asynParamInt32,     &BCMFiberSoftwareError);
    createParam(BCMFiberResetString,                        asynParamInt32,     &BCMFiberReset);
    // LUT, channel specific
    char buf[255];
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutDestinationIDString, i);
        createParam(buf,                         asynParamInt32,     &BCMLutDestinationID[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutBeamExistsString, i);
        createParam(buf,                         asynParamInt32,     &BCMLutBeamExists[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutModeIDString, i);
        createParam(buf,                         asynParamInt32,     &BCMLutModeID[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutMaxPulseLengthString, i);
        createParam(buf,                         asynParamInt32,     &BCMLutMaxPulseLength[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutLowerThresholdString, i);
        createParam(buf,                         asynParamFloat64,   &BCMLutLowerThreshold[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutUpperThresholdString, i);
        createParam(buf,                         asynParamFloat64,   &BCMLutUpperThreshold[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutMinTriggerPeriodString, i);
        createParam(buf,                         asynParamFloat64,   &BCMLutMinTriggerPeriod[i]);
    }
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        snprintf(buf, 254, BCMLutInterlockEnableString, i);
        createParam(buf,                         asynParamInt32,     &BCMLutInterlockEnable[i]);
    }

    int ret = initDevice();
    if (ret) {
        asynPrintError(pasynUserSelf, "initialization FAILED");
        setStringParam(SIS8300DriverMessage, "initialization FAILED");
    } else {
        asynPrintDeviceInfo(pasynUserSelf, "initialization PASSED");
        setStringParam(SIS8300DriverMessage, "initialization PASSED");
    }
}

sis8300bcm::~sis8300bcm()
{
    asynPrintDeviceInfo(pasynUserSelf, "nothing to do here..");
}

// BCM does not use arm feature of the generic Struck firmware
// override, otherwise it messes up the register writes (errors
// due to device being armed)
int sis8300bcm::armDevice()
{
    return 0;
}

int sis8300bcm::setupProbe(unsigned int addr)
{
    int source, channel;
    getIntegerParam(addr, BCMProbeSource, &source);
    getIntegerParam(addr, BCMProbeChannel, &channel);
    int probe = source + channel;
    int ret = sis8300drvbcm_set_probe_setup(mDeviceHandle, addr, probe);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_probe_setup returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTmaxPulseLength(unsigned int addr, int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutMaxPulseLength[i]);
        if (BCMLutMaxPulseLength[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    int intValue;
    int mode;
    getIntegerParam(BCMLutModeID[index], &mode);
    getIntegerParam(addr, BCMLutMaxPulseLength[index], &intValue);
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: beam mode %d, pulse length %d", index, mode, intValue);
    int ret = sis8300drvbcm_set_lut_maximum_pulse_length(mDeviceHandle, addr, index, mode, intValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_maximum_pulse_length returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTlowerThreshold(unsigned int addr, int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutLowerThreshold[i]);
        if (BCMLutLowerThreshold[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    double doubleValue;
    int mode;
    getIntegerParam(BCMLutModeID[index], &mode);
    getDoubleParam(addr, BCMLutLowerThreshold[index], &doubleValue);
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: beam mode %d, lower threshold %f", index, mode, doubleValue);
    int ret = sis8300drvbcm_set_lut_lower_threshold(mDeviceHandle, addr, index, mode, doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_lower_threshold returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTupperThreshold(unsigned int addr, int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutUpperThreshold[i]);
        if (BCMLutUpperThreshold[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    double doubleValue;
    int mode;
    getIntegerParam(BCMLutModeID[index], &mode);
    getDoubleParam(addr, BCMLutUpperThreshold[index], &doubleValue);
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: beam mode %d, upper threshold %f", index, mode, doubleValue);
    int ret = sis8300drvbcm_set_lut_upper_threshold(mDeviceHandle, addr, index, mode, doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_upper_threshold returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTminTriggerPeriod(int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutMinTriggerPeriod[i]);
        if (BCMLutMinTriggerPeriod[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    double doubleValue;
    int mode;
    getIntegerParam(BCMLutModeID[index], &mode);
    getDoubleParam(BCMLutMinTriggerPeriod[index], &doubleValue);
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: beam mode %d, min trigger period %f", index, mode, doubleValue);
    int ret = sis8300drvbcm_set_lut_min_trigger_period(mDeviceHandle, index, mode, doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_min_trigger_period returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTmasking(int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutBeamExists[i]);
        if (BCMLutBeamExists[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    int dest;
    int exists;
    unsigned int mask;
    getIntegerParam(BCMLutDestinationID[index], &dest);

    mask = 0;
    // XXX add two FIBER channels to the mask !!
    for (int addr = 0; addr < SIS8300BCM_NUM_PROC_CHANNELS; addr++) {
        getIntegerParam(addr, BCMLutBeamExists[index], &exists);
        if (exists) {
            mask |= (1 << addr);
        }
    }
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: destination %d, interlock mask %04X", index, dest, mask);
    int ret = sis8300drvbcm_set_lut_interlock_masking(mDeviceHandle, index, dest, mask);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_interlock_masking returned %d", ret);
        return ret;
    }

    return ret;
}

int sis8300bcm::setupLUTinterlockEnable(int function)
{
    int index = -1;
    // find the LUT index by looking at the parameter function value
    for (int i = 0; i < BCM_LUT_SIZE; i++) {
        asynPrintDeviceInfo(pasynUserSelf, "LUT %d, want %d, have %d", i, function, BCMLutInterlockEnable[i]);
        if (BCMLutInterlockEnable[i] == function) {
            index = i;
            break;
        }
    }

    if (index == -1) {
        asynPrintError(pasynUserSelf, "Invalid LUT index, function was %d", function);
        return -1;
    }

    int dest;
    int enable;
    unsigned int mask;
    getIntegerParam(BCMLutDestinationID[index], &dest);

    mask = 0;
    for (int addr = 0; addr < SIS8300BCM_NUM_DIFF_CHANNELS; addr++) {
        getIntegerParam(addr, BCMLutInterlockEnable[index], &enable);
        if (enable) {
            mask |= (1 << addr);
        }
    }
    asynPrintDeviceInfo(pasynUserSelf, "LUT %d: destination %d, interlock enable mask %04X", index, dest, mask);
    int ret = sis8300drvbcm_set_lut_interlock_enable(mDeviceHandle, index, dest, mask);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_interlock_enable returned %d", ret);
        return ret;
    }

    return ret;
}

// perform (read/write) FPGA register readback *before* the acquisition start
// note that (read-only) FPGA register readout is performed *after* the acquisition ends
int sis8300bcm::readbackParameters()
{
    // do not call base class method since most of the low-level
    // calls it makes are not valid for BCM !!!!

    double doubleValue;
    unsigned int uintValue;
    int intValue;

    int ret = sis8300drvbcm_get_clock_source(mDeviceHandle, (sis8300drv_clk_src *)&uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_clock_source returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300ClockSource, uintValue);

    // readback system wide parameters
    asynPrintDeviceInfo(pasynUserSelf, "readback system wide parameters");
    ret = sis8300drvbcm_get_minimum_trigger_period(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_minimum_trigger_period returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMMinTriggerPeriod, doubleValue);
    ret = sis8300drvbcm_get_maximum_pulse_width(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_maximum_pulse_width returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMMaxPulseWidth, uintValue);
    ret = sis8300drvbcm_get_alarms_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarms_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmsControl, uintValue);

    ret = sis8300drvbcm_get_alarm_auxiliary_clock_out_of_range_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_auxiliary_clock_out_of_range_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmAuxiliaryClockControl, uintValue);
    ret = sis8300drvbcm_get_alarm_processing_clock_out_of_range_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_processing_clock_out_of_range_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmProcessingClockControl, uintValue);
    ret = sis8300drvbcm_get_alarm_trigger_too_wide_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_trigger_too_wide_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmTriggerTooWideControl, uintValue);
    ret = sis8300drvbcm_get_alarm_trigger_too_narrow_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_trigger_too_narrow_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmTriggerTooNarrowControl, uintValue);
    ret = sis8300drvbcm_get_alarm_trigger_period_too_short_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_trigger_period_too_short_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmTriggerPeriodTooShortControl, uintValue);
    ret = sis8300drvbcm_get_beam_mode(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_beam_mode returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300BeamModeValue, uintValue);
    ret = sis8300drvbcm_get_beam_destination(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_beam_destination returned %d", ret);
        return ret;
    }
    setIntegerParam(SIS8300BeamDestinationValue, uintValue);
    ret = sis8300drvbcm_get_lut_control(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_lut_control returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMLUTControl, uintValue);

    ret = sis8300drvbcm_get_acquisition_trigger_source(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_acquisition_trigger_source returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAcquisitionTriggerSource, uintValue);

    ret = sis8300drvbcm_get_crate_id(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_crate_id returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMCrateID, uintValue);

    ret = sis8300drvbcm_get_pulse_width_filter(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_pulse_width_filter returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMPulseWidthFilter, doubleValue);

    ret = sis8300drvbcm_get_beam_trigger_source(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_beam_trigger_source returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMBeamTriggerSource, uintValue);

    // readback channel configuration parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback acq %d parameters", addr);

        ret = sis8300drvbcm_get_channel_number_of_samples(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_number_of_samples returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgNumSamples, uintValue);
        ret = sis8300drvbcm_get_channel_memory_address(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_memory_address returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgMemoryAddress, uintValue);
        ret = sis8300drvbcm_get_channel_sample_fraction_bits(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_sample_fraction_bits returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgFractionBits, uintValue);
        ret = sis8300drvbcm_get_channel_conversion_factor(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_conversion_factor returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcqCfgFactor, doubleValue);
        ret = sis8300drvbcm_get_channel_conversion_offset(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_conversion_offset returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcqCfgOffset, doubleValue);
        ret = sis8300drvbcm_get_channel_recording(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_recording returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgRecording, uintValue);
        ret = sis8300drvbcm_get_channel_scaling(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_scaling returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgScaling, uintValue);
        ret = sis8300drvbcm_get_channel_data_type_converting(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_data_type_converting returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgConverting, uintValue);
        ret = sis8300drvbcm_get_channel_decimation(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_decimation returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcqCfgDecimation, uintValue);
    }

    // readback ACCT parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_PROC_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback ACCT %d parameters", addr);

        ret = sis8300drvbcm_get_channel_adc_scale(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_adc_scale returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAdcScale, doubleValue);
        ret = sis8300drvbcm_get_channel_adc_offset(mDeviceHandle, addr, &intValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_adc_offset returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAdcOffset, intValue);
        ret = sis8300drvbcm_get_channel_flattop_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_flattop_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctFlatTopStart, doubleValue);
        ret = sis8300drvbcm_get_channel_flattop_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_flattop_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctFlatTopEnd, doubleValue);
        ret = sis8300drvbcm_get_channel_fine_delay(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_fine_delay returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctFineDelay, doubleValue);

        ret = sis8300drvbcm_get_channel_upper_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_upper_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctUpperThreshold, doubleValue);
        ret = sis8300drvbcm_get_channel_lower_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_lower_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctLowerThreshold, doubleValue);
        ret = sis8300drvbcm_get_channel_errant_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_errant_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctErrantThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_alarm_upper_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_upper_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmUpperControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_lower_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_lower_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmLowerControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_errant_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_errant_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmErrantControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_trigger_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_trigger_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmTriggerControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_limit_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_limit_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmLimitControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_adc_overflow_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_adc_overflow_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmAdcOverflowControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_adc_underflow_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_adc_underflow_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmAdcUnderflowControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_adc_stuck_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_adc_stuck_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmAdcStuckControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_aiu_fault_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_aiu_fault_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmAiuFaultControl, uintValue);
        ret = sis8300drvbcm_get_channel_alarm_charge_too_high_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_charge_too_high_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAlarmChargeTooHighControl, uintValue);

        ret = sis8300drvbcm_get_channel_lower_window_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_lower_window_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctLowerWindowStart, doubleValue);
        ret = sis8300drvbcm_get_channel_lower_window_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_lower_window_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctLowerWindowEnd, doubleValue);
        ret = sis8300drvbcm_get_channel_errant_window_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_errant_window_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctErrantWindowStart, doubleValue);
        ret = sis8300drvbcm_get_channel_errant_window_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_errant_window_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctErrantWindowEnd, doubleValue);

        ret = sis8300drvbcm_get_calibration_sample(mDeviceHandle, addr, 1, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_calibration_sample 1 returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctCalibrationSample1, doubleValue);

        ret = sis8300drvbcm_get_calibration_sample(mDeviceHandle, addr, 2, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_calibration_sample 2 returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctCalibrationSample2, doubleValue);

        ret = sis8300drvbcm_get_calibration_sample(mDeviceHandle, addr, 3, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_calibration_sample 3 returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctCalibrationSample3, doubleValue);

        ret = sis8300drvbcm_get_calibration_sample(mDeviceHandle, addr, 4, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_calibration_sample 4 returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctCalibrationSample4, doubleValue);

        ret = sis8300drvbcm_get_channel_aux_upper_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_aux_upper_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAuxUpperThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_aux_lower_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_aux_lower_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAuxLowerThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_aux_hysteresis_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_aux_hysteresis_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAuxHysteresisThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_beam_above_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_beam_above_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctBeamAboveThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_leaky_coefficient(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_leaky_coefficient returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctLeakyCoefficient, doubleValue);

        ret = sis8300drvbcm_get_channel_leaky_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_leaky_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctLeakyThreshold, doubleValue);

        ret = sis8300drvbcm_get_channel_auto_flattop_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_auto_flattop_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAutoFlatTopStart, doubleValue);
        ret = sis8300drvbcm_get_channel_auto_flattop_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_auto_flattop_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctAutoFlatTopEnd, doubleValue);
        ret = sis8300drvbcm_get_channel_auto_flattop_enable(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_auto_flattop_enable returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctAutoFlatTopEnable, uintValue);
        ret = sis8300drvbcm_get_channel_droop_rate(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_droop_rate returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctDroopRate, doubleValue);
        ret = sis8300drvbcm_get_channel_droop_compensating(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_droop_compensating returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctDroopCompensating, uintValue);
        ret = sis8300drvbcm_get_channel_noise_filtering(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_noise_filtering returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctNoiseFiltering, uintValue);
        ret = sis8300drvbcm_get_channel_baselining_after_droop_compensation(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_baselining_after_droop_compensation returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctBaseliningAfter, uintValue);
        ret = sis8300drvbcm_get_channel_baselining_before_droop_compensation(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_baselining_before_droop_compensation returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctBaseliningBefore, uintValue);
        ret = sis8300drvbcm_get_channel_dc_blocking(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_dc_blocking returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctDcBlocking, uintValue);

        ret = sis8300drvbcm_get_channel_trigger_source(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_trigger_source returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctTriggerSource, uintValue);
        ret = sis8300drvbcm_get_channel_beam_absence(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_beam_absence returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctBeamAbsence, uintValue);
    }

    // readback DIFF parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_DIFF_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback DIFF %d parameters", addr);

        ret = sis8300drvbcm_get_differential_source_a(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_source_a returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffSourceA, uintValue);
        ret = sis8300drvbcm_get_differential_source_b(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_source_b returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffSourceB, uintValue);
        ret = sis8300drvbcm_get_differential_delay(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_delay returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffDelay, doubleValue);
        ret = sis8300drvbcm_get_differential_fast_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_fast_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffFastThreshold, doubleValue);
        ret = sis8300drvbcm_get_differential_medium_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_medium_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffMediumThreshold, doubleValue);
        ret = sis8300drvbcm_get_differential_slow_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_slow_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffSlowThreshold, doubleValue);
        ret = sis8300drvbcm_get_differential_alarm_fast_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_fast_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffAlarmFastControl, uintValue);
        ret = sis8300drvbcm_get_differential_alarm_medium_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_medium_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffAlarmMediumControl, uintValue);
        ret = sis8300drvbcm_get_differential_alarm_slow_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_slow_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffAlarmSlowControl, uintValue);
        ret = sis8300drvbcm_get_differential_alarm_leaky_control(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_leaky_control returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffAlarmLeakyControl, uintValue);
        ret = sis8300drvbcm_get_differential_rising_window_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_rising_window_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffRisingWindowStart, doubleValue);
        ret = sis8300drvbcm_get_differential_rising_window_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_rising_window_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffRisingWindowEnd, doubleValue);
        ret = sis8300drvbcm_get_differential_falling_window_start(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_falling_window_start returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffFallingWindowStart, doubleValue);
        ret = sis8300drvbcm_get_differential_falling_window_end(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_falling_window_end returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffFallingWindowEnd, doubleValue);
        ret = sis8300drvbcm_get_differential_ws_between(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_ws_between returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffWsBetween, uintValue);
        ret = sis8300drvbcm_get_differential_emu_between(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_emu_between returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffEmuBetween, uintValue);
        ret = sis8300drvbcm_get_differential_rfq_between(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_rfq_between returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffRfqBetween, uintValue);
        ret = sis8300drvbcm_get_differential_fast_window_width(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_fast_window_width returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffFastWindowWidth, doubleValue);
        ret = sis8300drvbcm_get_differential_medium_window_width(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_medium_window_width returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffMediumWindowWidth, doubleValue);
        ret = sis8300drvbcm_get_differential_fast_window_width_inverse(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_fast_window_width_inverse returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffFastWindowWidthInverse, doubleValue);
        ret = sis8300drvbcm_get_differential_medium_window_width_inverse(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_medium_window_width_inverse returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffMediumWindowWidthInverse, doubleValue);
        ret = sis8300drvbcm_get_differential_leaky_coefficient(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_leaky_coefficient returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffLeakyCoefficient, doubleValue);
        ret = sis8300drvbcm_get_differential_leaky_threshold(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_leaky_threshold returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMDiffLeakyThreshold, doubleValue);
    }

    // readback PROBE parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_PROBE_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback PROBE %d parameters", addr);

        ret = sis8300drvbcm_get_probe_setup(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_probe_setup returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMProbeSource, (unsigned int)(uintValue / 10) * 10);
        setIntegerParam(addr, BCMProbeChannel, uintValue % 10);
    }

    callParamCallbacks();

    return ret;
}

// perform (read-only) FPGA register readout which update *after* the acquisition ends
// note that user (read/write) FPGA register readback is performed *before* the acquisition start
int sis8300bcm::deviceDone()
{
    int ret = sis8300::deviceDone();
    if (ret) {
       return ret;
    }

    double doubleValue;
    unsigned int uintValue;
    double flattopStart, flattopEnd, flattopCharge;
    int autoFlattopEnable;
    double autoFlattopStart, autoFlattopEnd, triggerWidth;

    // readout system wide parameters
    asynPrintDeviceInfo(pasynUserSelf, "readout system wide parameters");
    ret = sis8300drvbcm_get_measured_sampling_frequency(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_sampling_frequency returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMClockFrequencyMeas, doubleValue);
    ret = sis8300drvbcm_get_measured_trigger_period(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_trigger_period returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMTriggerPeriodMeas, doubleValue);
    ret = sis8300drvbcm_get_measured_trigger_width(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_trigger_width returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMTriggerWidthMeas, doubleValue);

    ret = sis8300drvbcm_get_alarm_hold(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_hold returned %d", ret);
        return ret;
    }
    asynPrintDeviceInfo(pasynUserSelf, "alarms hold 0x%02X", uintValue);
    setIntegerParam(BCMAlarmAuxiliaryClockHold, (uintValue & 0x01) ? 1 : 0);
    setIntegerParam(BCMAlarmProcessingClockHold, (uintValue & 0x02) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooWideHold, (uintValue & 0x04) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooNarrowHold, (uintValue & 0x08) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerPeriodTooShortHold, (uintValue & 0x10) ? 1 : 0);
    setIntegerParam(BCMAlarmLUTMinTriggerPeriodHold, (uintValue & 0x20) ? 1 : 0);

    ret = sis8300drvbcm_get_alarm_first(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_first returned %d", ret);
        return ret;
    }
    asynPrintDeviceInfo(pasynUserSelf, "alarms first 0x%02X", uintValue);
    setIntegerParam(BCMAlarmAuxiliaryClockFirst, (uintValue & 0x01) ? 1 : 0);
    setIntegerParam(BCMAlarmProcessingClockFirst, (uintValue & 0x02) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooWideFirst, (uintValue & 0x04) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooNarrowFirst, (uintValue & 0x08) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerPeriodTooShortFirst, (uintValue & 0x10) ? 1 : 0);
    setIntegerParam(BCMAlarmLUTMinTriggerPeriodFirst, (uintValue & 0x20) ? 1 : 0);

    ret = sis8300drvbcm_get_alarm_direct(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_alarm_direct returned %d", ret);
        return ret;
    }
    asynPrintDeviceInfo(pasynUserSelf, "alarms direct 0x%02X", uintValue);
    setIntegerParam(BCMAlarmAuxiliaryClockDirect, (uintValue & 0x01) ? 1 : 0);
    setIntegerParam(BCMAlarmProcessingClockDirect, (uintValue & 0x02) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooWideDirect, (uintValue & 0x04) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerTooNarrowDirect, (uintValue & 0x08) ? 1 : 0);
    setIntegerParam(BCMAlarmTriggerPeriodTooShortDirect, (uintValue & 0x10) ? 1 : 0);
    setIntegerParam(BCMAlarmLUTMinTriggerPeriodDirect, (uintValue & 0x20) ? 1 : 0);

    ret = sis8300drvbcm_get_status_bits(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_status_bits returned %d", ret);
        return ret;
    }
    asynPrintDeviceInfo(pasynUserSelf, "BCM status 0x%04X", uintValue);
    setIntegerParam(BCMMainClockMissing, (uintValue & 0x01) ? 1 : 0);
    setIntegerParam(BCMAuxiliaryClockMissing, (uintValue & 0x02) ? 1 : 0);
    setIntegerParam(BCMReady, (uintValue & 0x04) ? 1 : 0);
    setIntegerParam(BCMReadyFPGA, (uintValue & 0x10) ? 1 : 0);
    setIntegerParam(BCMBeamPermitFPGA, (uintValue & 0x20) ? 1 : 0);
    setIntegerParam(BCMHighVoltagePresence, (uintValue & 0x80) ? 1 : 0);
    setIntegerParam(BCMHighVoltageOK, (uintValue & 0x100) ? 1 : 0);

    // BCMAlarmDiffLUTDestinationModeDirect is on bit 12 of
    // SIS8300BCM_DIFF_INTERLOCK_ENABLE_REG register
    ret = sis8300drvbcm_get_differential_interlock_enable(mDeviceHandle, 12, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_interlock_enable bit 12 returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMAlarmDiffLUTDestinationModeDirect, uintValue);

    // readout ACCT parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_PROC_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback ACCT %d parameters", addr);

        ret = sis8300drvbcm_get_channel_measured_pulse_width(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_measured_pulse_width returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctPulseWidth, doubleValue);
        ret = sis8300drvbcm_get_channel_measured_pulse_charge(mDeviceHandle, addr, &doubleValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_measured_pulse_charge returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctPulseCharge, doubleValue);
        ret = sis8300drvbcm_get_channel_measured_flattop_charge(mDeviceHandle, addr, &flattopCharge);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_measured_flattop_charge returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctFlatTopCharge, flattopCharge);
        ret = sis8300drvbcm_get_channel_trigger_width(mDeviceHandle, addr, &triggerWidth);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_trigger_width returned %d", ret);
            return ret;
        }
        setDoubleParam(addr, BCMAcctTriggerWidth, triggerWidth);

        // flat top current, check if automatic mode is enabled
        getIntegerParam(addr, BCMAcctAutoFlatTopEnable, &autoFlattopEnable);
        asynPrintDeviceInfo(pasynUserSelf, "Channel %d auto flattop enable is %d", addr, autoFlattopEnable);
        if(autoFlattopEnable){
            // auto flat top start and end have been read-back before the start of acquisition
            getDoubleParam(addr, BCMAcctAutoFlatTopStart, &autoFlattopStart);
            getDoubleParam(addr, BCMAcctAutoFlatTopEnd, &autoFlattopEnd);
            // calculate the average current over automatic flattop (window in ms)
            ret = sis8300drvbcm_get_channel_flattop_current(mDeviceHandle, addr, triggerWidth - (autoFlattopStart/1e6) + (autoFlattopEnd/1e6), flattopCharge, &doubleValue);
            if (ret) {
                asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_flattop_current auto returned %d", ret);
                return ret;
            }
            asynPrintDeviceInfo(pasynUserSelf, "Channel %d auto flattop current %f mA", addr, doubleValue);
        }else{
            // flat top start and end have been read-back before the start of acquisition
            getDoubleParam(addr, BCMAcctFlatTopStart, &flattopStart);
            getDoubleParam(addr, BCMAcctFlatTopEnd, &flattopEnd);
            // calculate the average current over flattop
            ret = sis8300drvbcm_get_channel_flattop_current(mDeviceHandle, addr, flattopEnd - flattopStart, flattopCharge, &doubleValue);
            if (ret) {
                asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_flattop_current returned %d", ret);
                return ret;
            }
            asynPrintDeviceInfo(pasynUserSelf, "Channel %d flattop current %f mA", addr, doubleValue);
        }
        setDoubleParam(addr, BCMAcctFlatTopCurrent, doubleValue);

        ret = sis8300drvbcm_get_channel_alarm_hold(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_hold returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "alarms hold 0x%02X", uintValue);
        setIntegerParam(addr, BCMAcctAlarmUpperHold, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLowerHold, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmErrantHold, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmTriggerHold, (uintValue & 0x08) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLimitHold, (uintValue & 0x10) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcOverflowHold, (uintValue & 0x20) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcUnderflowHold, (uintValue & 0x40) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcStuckHold, (uintValue & 0x80) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAiuFaultHold, (uintValue & 0x100) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmChargeTooHighHold, (uintValue & 0x200) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTUpperThresholdHold, (uintValue & 0x400) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTLowerThresholdHold, (uintValue & 0x800) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTPulseLengthHold, (uintValue & 0x1000) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTDestinationModeHold, (uintValue & 0x2000) ? 1 : 0);

        ret = sis8300drvbcm_get_channel_alarm_first(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_first returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "alarms first 0x%02X", uintValue);
        setIntegerParam(addr, BCMAcctAlarmUpperFirst, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLowerFirst, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmErrantFirst, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmTriggerFirst, (uintValue & 0x08) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLimitFirst, (uintValue & 0x10) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcOverflowFirst, (uintValue & 0x20) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcUnderflowFirst, (uintValue & 0x40) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcStuckFirst, (uintValue & 0x80) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAiuFaultFirst, (uintValue & 0x100) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmChargeTooHighFirst, (uintValue & 0x200) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTUpperThresholdFirst, (uintValue & 0x400) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTLowerThresholdFirst, (uintValue & 0x800) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTPulseLengthFirst, (uintValue & 0x1000) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTDestinationModeFirst, (uintValue & 0x2000) ? 1 : 0);

        ret = sis8300drvbcm_get_channel_alarm_direct(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_alarm_direct returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "alarms direct 0x%02X", uintValue);
        setIntegerParam(addr, BCMAcctAlarmUpperDirect, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLowerDirect, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmErrantDirect, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmTriggerDirect, (uintValue & 0x08) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLimitDirect, (uintValue & 0x10) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcOverflowDirect, (uintValue & 0x20) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcUnderflowDirect, (uintValue & 0x40) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAdcStuckDirect, (uintValue & 0x80) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmAiuFaultDirect, (uintValue & 0x100) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmChargeTooHighDirect, (uintValue & 0x200) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTUpperThresholdDirect, (uintValue & 0x400) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTLowerThresholdDirect, (uintValue & 0x800) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTPulseLengthDirect, (uintValue & 0x1000) ? 1 : 0);
        setIntegerParam(addr, BCMAcctAlarmLUTDestinationModeDirect, (uintValue & 0x2000) ? 1 : 0);

	ret = sis8300drvbcm_get_channel_beam_exists(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_beam_exists returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctBeamExists, uintValue);

        ret = sis8300drvbcm_get_channel_maximum_pulse_length(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_channel_maximum_pulse_length returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMAcctMaxPulseLength, uintValue);
    }

    // readout DIFF parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_DIFF_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback DIFF %d parameters", addr);

        ret = sis8300drvbcm_get_differential_alarm_hold(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_hold returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "DIFF %d alarms hold 0x%02X", addr, uintValue);
        setIntegerParam(addr, BCMDiffAlarmFastHold, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmMediumHold, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmSlowHold, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmLeakyHold, (uintValue & 0x08) ? 1 : 0);

        ret = sis8300drvbcm_get_differential_alarm_first(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_first returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "DIFF %d alarms first 0x%02X", addr, uintValue);
        setIntegerParam(addr, BCMDiffAlarmFastFirst, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmMediumFirst, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmSlowFirst, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmLeakyFirst, (uintValue & 0x08) ? 1 : 0);

        ret = sis8300drvbcm_get_differential_alarm_direct(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_alarm_direct returned %d", ret);
            return ret;
        }
        asynPrintDeviceInfo(pasynUserSelf, "DIFF %d alarms direct 0x%02X", addr, uintValue);
        setIntegerParam(addr, BCMDiffAlarmFastDirect, (uintValue & 0x01) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmMediumDirect, (uintValue & 0x02) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmSlowDirect, (uintValue & 0x04) ? 1 : 0);
        setIntegerParam(addr, BCMDiffAlarmLeakyDirect, (uintValue & 0x08) ? 1 : 0);

        ret = sis8300drvbcm_get_differential_interlock_enable(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_differential_interlock_enable returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMDiffInterlockEnable, uintValue);
    }

    // readback FIBER parameters
    for (int addr = 0; addr < SIS8300BCM_NUM_FIBER_CHANNELS; addr++) {
        asynPrintDeviceInfo(pasynUserSelf, "readback FIBER %d parameters", addr);

        ret = sis8300drvbcm_get_fiber_out_data_select(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_out_data_select returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberOutDataSelect, uintValue);
        ret = sis8300drvbcm_get_fiber_out_data_enable(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_out_data_enable returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberOutDataEnable, uintValue);
        ret = sis8300drvbcm_get_fiber_sfp_present(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_sfp_present returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberSFPPresent, uintValue);
        ret = sis8300drvbcm_get_fiber_lane_up(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_lane_up returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberLaneUp, uintValue);
        ret = sis8300drvbcm_get_fiber_channel_up(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_channel_up returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberChannelUp, uintValue);
        ret = sis8300drvbcm_get_fiber_hardware_error(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_hardware_error returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberHardwareError, uintValue);
        ret = sis8300drvbcm_get_fiber_software_error(mDeviceHandle, addr, &uintValue);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_get_fiber_software_error returned %d", ret);
            return ret;
        }
        setIntegerParam(addr, BCMFiberSoftwareError, uintValue);
    }

    return ret;
}

int sis8300bcm::acquireArrays()
{
    int numSamples;
    getIntegerParam(SIS8300NumSamples, &numSamples);
    size_t dims[1];
    dims[0] = numSamples;

    int convert;
    int memAddress, memSize;
    int record;
    memSize = numSamples * 4;

    // data acquisition:
    //  - raw data addr 0 .. 9
    //  - processed addr 10 .. 19
    //  - probe1 addr 20 .. 29
    //  - probe2 addr 30 .. 39
    //  - probe3 addr 40 .. 49
    //  - probe4 addr 50 .. 59

    // release all the buffers
    for (int addr = 0; addr < maxAddr; addr++) {
        if (this->pArrays[addr]) {
            this->pArrays[addr]->release();
        }
        this->pArrays[addr] = NULL;
    }

    int ret = 0;
    for (int addr = 0; addr < SIS8300BCM_NUM_CHANNELS; addr++) {
        getIntegerParam(addr, BCMAcqCfgConverting, &convert);
        getIntegerParam(addr, BCMAcqCfgMemoryAddress, &memAddress);
        getIntegerParam(addr, BCMAcqCfgRecording, &record);
        if (! record) {
            continue;
        }

        if (addr < SIS8300BCM_CHANNEL_PROBE1) {
            asynPrintDeviceInfo(pasynUserSelf, "acct addr %d, record %d, samples %d, address 0x%x",
                addr, record, numSamples, memAddress);
            // raw and processed data channels
            if (convert) {
                // data conversion enabled, data samples are 32 bit float
                this->pArrays[addr] = pNDArrayPool->alloc(1, dims, NDFloat32, 0, 0);
                ret = sis8300drvbcm_read_ram(mDeviceHandle, memAddress, memSize, this->pArrays[addr]->pData);
                if (ret) {
                    asynPrintError(pasynUserSelf, "sis8300drvbcm_read_ram returned %d", ret);
                    return ret;
                }
            } else {
                // data conversion disabled, data samples are 32 bit integers (ADC samples)
                this->pArrays[addr] = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
                ret = sis8300drvbcm_read_ram(mDeviceHandle, memAddress, memSize, this->pArrays[addr]->pData);
                if (ret) {
                    asynPrintError(pasynUserSelf, "sis8300drvbcm_read_ram returned %d", ret);
                    return ret;
                }
            }

        } else {
            // probe data channels
            int probeAddr = addr - SIS8300BCM_CHANNEL_PROBE1;
            int probeSource, probeChannel;
            getIntegerParam(probeAddr, BCMProbeSource, &probeSource);
            getIntegerParam(probeAddr, BCMProbeChannel, &probeChannel);
            int probe = probeSource + probeChannel;
            asynPrintDeviceInfo(pasynUserSelf,
                "probe addr %d, record %d, samples %d, address 0x%x, source %d, channel %d, probe %d",
                addr, record, numSamples, memAddress, probeSource, probeChannel, probe);

            if (probe >= 0 && probe < 10) {
                // data conversion disabled, data samples are 32 bit integers (ADC samples)
                NDArray *tmpArray = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
                epicsInt32 *pTmpData = (epicsInt32 *)tmpArray->pData;
                ret = sis8300drvbcm_read_ram(mDeviceHandle, memAddress, memSize, tmpArray->pData);
                if (ret) {
                    asynPrintError(pasynUserSelf, "sis8300drvbcm_read_ram returned %d", ret);
                    return ret;
                }
                probeAddr = probeAddr * 10 + 20;
                // each data sample is a bifield, high 16 bits are used
                for (int bit = 16; bit < 26; bit++) {
                    this->pArrays[probeAddr] = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
                    epicsInt32 *pData = (epicsInt32 *)this->pArrays[probeAddr]->pData;
                    for (int i = 0; i < numSamples; i++) {
                        *(pData + i) = (*(pTmpData + i) & (1 << bit) ? 1 : 0);
                    }
                    probeAddr++;
                }
                tmpArray->release();
            } else {
                probeAddr = probeAddr * 10 + 20;
                // data conversion is always disabled for probe channels, data samples are 32 bit integers (ADC samples)
                // this->pArrays[probeAddr] = pNDArrayPool->alloc(1, dims, NDInt32, 0, 0);
                // data conversion is always enabled for probe channels, data samples are 32 bit floats
                this->pArrays[probeAddr] = pNDArrayPool->alloc(1, dims, NDFloat32, 0, 0);
                ret = sis8300drvbcm_read_ram(mDeviceHandle, memAddress, memSize, this->pArrays[probeAddr]->pData);
                if (ret) {
                    asynPrintError(pasynUserSelf, "sis8300drvbcm_read_ram returned %d", ret);
                    return ret;
                }
            }
        }
    }

    return ret;
}

int sis8300bcm::initDevice()
{
    unsigned int uintValue;
    int ret = sis8300drvbcm_get_firmware_version(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_firmware_version returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMFwVersion, uintValue);
    ret = sis8300drvbcm_get_firmware_git_hash(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_firmware_git_hash returned %d", ret);
        return ret;
    }
    setIntegerParam(BCMFwGitHash, uintValue);
    // disable LUT
    setIntegerParam(BCMLUTControl, 1);
    ret = sis8300drvbcm_set_lut_control(mDeviceHandle, 1);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_lut_control returned %d", ret);
        return ret;
    }
    // set the clock source early to backplane A
    ret = sis8300drvbcm_set_clock_source(mDeviceHandle, clk_src_backplane_a);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_set_clock_source returned %d", ret);
        return ret;
    }

    // report the BCM status
    ret = sis8300drvbcm_get_status_bits(mDeviceHandle, &uintValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_status_bits returned %d", ret);
        return ret;
    }
    asynPrintDeviceInfo(pasynUserSelf, "BCM status 0x%02X", uintValue);
    setIntegerParam(BCMMainClockMissing, (uintValue & 0x01) ? 1 : 0);
    setIntegerParam(BCMAuxiliaryClockMissing, (uintValue & 0x02) ? 1 : 0);
    setIntegerParam(BCMReady, (uintValue & 0x04) ? 1 : 0);

    double doubleValue;
    ret = sis8300drvbcm_get_measured_sampling_frequency(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_sampling_frequency returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMClockFrequencyMeas, doubleValue);
    ret = sis8300drvbcm_get_measured_trigger_period(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_trigger_period returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMTriggerPeriodMeas, doubleValue);
    ret = sis8300drvbcm_get_measured_trigger_width(mDeviceHandle, &doubleValue);
    if (ret) {
        asynPrintError(pasynUserSelf, "sis8300drvbcm_get_measured_trigger_width returned %d", ret);
        return ret;
    }
    setDoubleParam(BCMTriggerWidthMeas, doubleValue);

    // perform readout of the read-only parameters
    // XXX consider factoring out the readout from deviceDone() method
    //     into a new method called from here and from deviceDone()..
    ret = deviceDone();
    // perform readback of the read/write parameters
    // XXX consider factoring out the readout from refreshParameters() method
    //     into a new method called from here and from refreshParameters()..
    ret = readbackParameters();

    return ret;
}

// called when asyn clients call pasynInt32->write()
asynStatus sis8300bcm::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    const char *name;
    asynStatus status = asynSuccess;
    int ret = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %d", name, function, value);

    status = setIntegerParam(addr, function, value);

    if (function == SIS8300NumSamples) {
        // override generic number of samples handling from sis8300
        for (unsigned a = 0; a < SIS8300BCM_NUM_CHANNELS; a++) {
            // set the number of samples for all channels
            ret = sis8300drvbcm_set_channel_number_of_samples(mDeviceHandle, a, value);
            if (ret) {
                asynPrintError(pasynUser, "sis8300drvbcm_set_channel_number_of_samples returned %d", ret);
            }
        }
    } else if (function == SIS8300ClockDivider) {
        // override generic clock source handling from sis8300
    } else if (function == SIS8300ClockSource) {
        // override generic clock source handling from sis8300
        // Backplane A is only valid choice
        ret = sis8300drvbcm_set_clock_source(mDeviceHandle, (sis8300drv_clk_src)value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_clock_source returned %d", ret);
        } else {
            updateTickValue();
        }
    } else if (function == SIS8300TriggerDelay) {
        // override generic clock source handling from sis8300
    } else if (function == BCMAcqCfgNumSamples) {
        ret = sis8300drvbcm_set_channel_number_of_samples(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_number_of_samples returned %d", ret);
        }
    } else if (function == BCMAcqCfgMemoryAddress) {
        ret = sis8300drvbcm_set_channel_memory_address(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_memory_address returned %d", ret);
        }
    } else if (function == BCMAcqCfgFractionBits) {
        ret = sis8300drvbcm_set_channel_sample_fraction_bits(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_sample_fraction_bits returned %d", ret);
        }
    } else if (function == BCMAcqCfgDecimation) {
        ret = sis8300drvbcm_set_channel_decimation(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_decimation returned %d", ret);
        }
        // decimation of the samples affects tick value, used in time axis
        // calculation outside this module (see admisc, NDTrace)
        double tick;
        getDoubleParam(SIS8300TickValue, &tick);
        asynPrintDeviceInfo(pasynUserSelf, "addr %d time axis tick set to %g s", addr, value * tick);
        setDoubleParam(addr, BCMAcqCfgTickValue, value * tick);
    } else if (function == BCMAcqCfgRecording) {
        ret = sis8300drvbcm_set_channel_recording(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_recording returned %d", ret);
        }
    } else if (function == BCMAcqCfgScaling) {
        ret = sis8300drvbcm_set_channel_scaling(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_scaling returned %d", ret);
        }
    } else if (function == BCMAcqCfgConverting) {
        ret = sis8300drvbcm_set_channel_data_type_converting(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_data_type_converting returned %d", ret);
        }
    } else if (function == BCMAcctAdcOffset) {
        ret = sis8300drvbcm_set_channel_adc_offset(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_adc_offset returned %d", ret);
        }
    } else if (function == BCMEnableCalibrationPulse) {
        ret = sis8300drvbcm_set_enable_calibration_pulse(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_enable_calibration_pulse returned %d", ret);
        }
    } else if (function == SIS8300TriggerSource || function == SIS8300TriggerExternalLine) {
        // override generic trigger source/line handling from sis8300
    } else if (function == BCMAcctDroopCompensating) {
        ret = sis8300drvbcm_set_channel_droop_compensating(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_droop_compensating returned %d", ret);
        }
    } else if (function == BCMAcctNoiseFiltering) {
        ret = sis8300drvbcm_set_channel_noise_filtering(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_noise_filtering returned %d", ret);
        }
    } else if (function == BCMAcctBaseliningBefore) {
        ret = sis8300drvbcm_set_channel_baselining_before_droop_compensation(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_baselining_before_droop_compensation returned %d", ret);
        }
    } else if (function == BCMAcctBaseliningAfter) {
        ret = sis8300drvbcm_set_channel_baselining_after_droop_compensation(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_baselining_after_droop_compensation returned %d", ret);
        }
    } else if (function == BCMAcctDcBlocking) {
        ret = sis8300drvbcm_set_channel_dc_blocking(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_dc_blocking returned %d", ret);
        }
    } else if (function == BCMAcctAlarmUpperControl) {
        ret = sis8300drvbcm_set_channel_alarm_upper_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_upper_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmLowerControl) {
        ret = sis8300drvbcm_set_channel_alarm_lower_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_lower_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmErrantControl) {
        ret = sis8300drvbcm_set_channel_alarm_errant_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_errant_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmTriggerControl) {
        ret = sis8300drvbcm_set_channel_alarm_trigger_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_trigger_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmLimitControl) {
        ret = sis8300drvbcm_set_channel_alarm_limit_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_limit_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmAdcOverflowControl) {
        ret = sis8300drvbcm_set_channel_alarm_adc_overflow_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_adc_overflow_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmAdcUnderflowControl) {
        ret = sis8300drvbcm_set_channel_alarm_adc_underflow_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_adc_underflow_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmAdcStuckControl) {
        ret = sis8300drvbcm_set_channel_alarm_adc_stuck_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_adc_stuck_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmAiuFaultControl) {
        ret = sis8300drvbcm_set_channel_alarm_aiu_fault_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_aiu_fault_control returned %d", ret);
        }
    } else if (function == BCMAcctAlarmChargeTooHighControl) {
        ret = sis8300drvbcm_set_channel_alarm_charge_too_high_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_alarm_charge_too_high_control returned %d", ret);
        }
    } else if (function == BCMMaxPulseWidth) {
        ret = sis8300drvbcm_set_maximum_pulse_width(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_maximum_pulse_width returned %d", ret);
        }
    } else if (function == BCMAlarmsClear) {
        ret = sis8300drvbcm_clear_alarms(mDeviceHandle);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_clear_alarms returned %d", ret);
        }
    } else if (function == BCMAlarmsControl) {
        ret = sis8300drvbcm_set_alarms_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarms_control returned %d", ret);
        }
    } else if (function == BCMAlarmAuxiliaryClockControl) {
        ret = sis8300drvbcm_set_alarm_auxiliary_clock_out_of_range_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarm_auxiliary_clock_out_of_range_control returned %d", ret);
        }
    } else if (function == BCMAlarmProcessingClockControl) {
        ret = sis8300drvbcm_set_alarm_processing_clock_out_of_range_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarm_processing_clock_out_of_range_control returned %d", ret);
        }
    } else if (function == BCMAlarmTriggerTooWideControl) {
        ret = sis8300drvbcm_set_alarm_trigger_too_wide_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarm_trigger_too_wide_control returned %d", ret);
        }
    } else if (function == BCMAlarmTriggerTooNarrowControl) {
        ret = sis8300drvbcm_set_alarm_trigger_too_narrow_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarm_trigger_too_narrow_control returned %d", ret);
        }
    } else if (function == BCMAlarmTriggerPeriodTooShortControl) {
        ret = sis8300drvbcm_set_alarm_trigger_period_too_short_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_alarm_trigger_period_too_short_control returned %d", ret);
        }
    } else if (function == BCMReady) {
        ret = sis8300drvbcm_set_status_ready(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_status_ready returned %d", ret);
        }
    } else if (function == SIS8300BeamModeValue) {
        int beamModeSource;
        getIntegerParam(SIS8300BeamModeSource, &beamModeSource);
        if (beamModeSource == 1) {
            // beam mode is coming from EVR and is valid only if EVR link is alive
            if (! checkEvrLink()) {
                // EVR link down?!? EVR IOC dead?!? Force error code!
                value = 0xFE;
            }
        }
        ret = sis8300drvbcm_set_beam_mode(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_beam_mode returned %d", ret);
        }
    } else if (function == SIS8300BeamDestinationValue) {
        int beamDestSource;
        getIntegerParam(SIS8300BeamDestinationSource, &beamDestSource);
        if (beamDestSource == 1) {
            // beam destination is coming from EVR and is valid only if EVR link is alive
            if (! checkEvrLink()) {
                // EVR link down?!? EVR IOC dead?!? Force error code!
                value = 0xFE;
            }
        }
        ret = sis8300drvbcm_set_beam_destination(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_beam_destination returned %d", ret);
        }
    } else if (function == BCMProbeSource || function == BCMProbeChannel) {
        ret = setupProbe(addr);
    } else if (function == BCMDiffSourceA) {
        ret = sis8300drvbcm_set_differential_source_a(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_source_a returned %d", ret);
        }
    } else if (function == BCMDiffSourceB) {
        ret = sis8300drvbcm_set_differential_source_b(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_source_b returned %d", ret);
        }
    } else if (function == BCMDiffAlarmFastControl) {
        ret = sis8300drvbcm_set_differential_alarm_fast_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_alarm_fast_control returned %d", ret);
        }
    } else if (function == BCMDiffAlarmMediumControl) {
        ret = sis8300drvbcm_set_differential_alarm_medium_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_alarm_medium_control returned %d", ret);
        }
    } else if (function == BCMDiffAlarmSlowControl) {
        ret = sis8300drvbcm_set_differential_alarm_slow_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_alarm_slow_control returned %d", ret);
        }
    } else if (function == BCMDiffAlarmLeakyControl) {
        ret = sis8300drvbcm_set_differential_alarm_leaky_control(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_alarm_leaky_control returned %d", ret);
        }
    } else if (function == BCMDiffWsBetween) {
        ret = sis8300drvbcm_set_differential_ws_between(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_ws_between returned %d", ret);
        }
    } else if (function == BCMDiffEmuBetween) {
        ret = sis8300drvbcm_set_differential_emu_between(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_emu_between returned %d", ret);
        }
    } else if (function == BCMDiffRfqBetween) {
        ret = sis8300drvbcm_set_differential_rfq_between(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_rfq_between returned %d", ret);
        }
    } else if (function == BCMAcctMaxPulseLength) {
        ret = sis8300drvbcm_set_channel_maximum_pulse_length(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_maximum_pulse_length returned %d", ret);
        }
    } else if (function == BCMLUTControl) {
        ret = sis8300drvbcm_set_lut_control(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_lut_control returned %d", ret);
        }
    } else if (function >= BCMLutDestinationID[0] && function <= BCMLutDestinationID[BCM_LUT_SIZE-1]) {
        // XXX should the LUT destination ID change cause update of the LUT?
        //     currently we can not just call setupLUTmasking() cause function lookup will fail!
    } else if (function >= BCMLutBeamExists[0] && function <= BCMLutBeamExists[BCM_LUT_SIZE-1]) {
        ret = setupLUTmasking(function);
    } else if (function >= BCMLutInterlockEnable[0] && function <= BCMLutInterlockEnable[BCM_LUT_SIZE-1]) {
        ret = setupLUTinterlockEnable(function);
    } else if (function >= BCMLutMaxPulseLength[0] && function <= BCMLutMaxPulseLength[BCM_LUT_SIZE-1]) {
        ret = setupLUTmaxPulseLength(addr, function);
    } else if (function >= BCMLutModeID[0] && function <= BCMLutModeID[BCM_LUT_SIZE-1]) {
        // XXX should the LUT beam ID change cause update of the LUT?
        //     currently we can not just call setupLUTmaxPulseLength() cause function lookup will fail!
    } else if (function == BCMAcquisitionTriggerSource) {
        ret = sis8300drvbcm_set_acquisition_trigger_source(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_acquisition_trigger_source returned %d", ret);
        }
    } else if (function == BCMCrateID) {
        ret = sis8300drvbcm_set_crate_id(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_crate_id returned %d", ret);
        }
    } else if (function == BCMBeamTriggerSource) {
        ret = sis8300drvbcm_set_beam_trigger_source(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_beam_trigger_source returned %d", ret);
        }
    } else if (function == BCMAcctTriggerSource) {
        ret = sis8300drvbcm_set_channel_trigger_source(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_set_channel_trigger_source returned %d", ret);
        }
    } else if (function == BCMAcctAutoFlatTopEnable) {
        ret = sis8300drvbcm_set_channel_auto_flattop_enable(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_auto_flattop_enable returned %d", ret);
        }
    } else if (function == BCMAcctBeamAbsence) {
        ret = sis8300drvbcm_set_channel_beam_absence(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_beam_absence returned %d", ret);
        }
    } else if (function == BCMFiberOutDataSelect) {
        ret = sis8300drvbcm_set_fiber_out_data_select(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_set_fiber_out_data_select returned %d", ret);
        }
    } else if (function == BCMFiberOutDataEnable) {
        ret = sis8300drvbcm_set_fiber_out_data_enable(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_set_fiber_out_data_enable returned %d", ret);
        }

    } else if (function == BCMFiberReset) {
        ret = sis8300drvbcm_set_fiber_reset(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUserSelf, "sis8300drvbcm_set_fiber_reset returned %d", ret);
        }
    } else {
        if (function < BCM_FIRST_PARAM) {
            status = sis8300::writeInt32(pasynUser, value);
        }
    }
    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %d", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%d", status, function, value);
    }

    // read back the firmware register values
    // XXX is ignoring the errors here OK?!
    readbackParameters();

    return status;
}

// called when asyn clients call pasynFloat64->write()
asynStatus sis8300bcm::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr;
    const char *name;
    asynStatus status = asynSuccess;
    int ret = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %f", name, function, value);

    status = setDoubleParam(addr, function, value);

    if (function == SIS8300SamplingFrequency) {
        // override generic clock frequency handling from sis8300
        asynPrintDeviceInfo(pasynUserSelf, "using sampling frequency %g", value);
        // FIXME: can this fail?
        sis8300drvbcm_set_sampling_frequency(mDeviceHandle, value);
        // since sampling frequency changed, update the tick value as well
        double tick = 1.0 / value;
        setDoubleParam(SIS8300TickValue, tick);
        asynPrintDeviceInfo(pasynUserSelf, "time axis tick set to %g s", tick);
    } else if (function == BCMAcqCfgFactor) {
        ret = sis8300drvbcm_set_channel_conversion_factor(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_conversion_factor returned %d", ret);
        }
    } else if (function == BCMAcqCfgOffset) {
        ret = sis8300drvbcm_set_channel_conversion_offset(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_conversion_offset returned %d", ret);
        }
    } else if (function == BCMAcctAdcScale) {
        ret = sis8300drvbcm_set_channel_adc_scale(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_adc_scale returned %d", ret);
        }
    } else if (function == BCMAcctFlatTopStart) {
        ret = sis8300drvbcm_set_channel_flattop_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_flattop_start returned %d", ret);
        }
    } else if (function == BCMAcctFlatTopEnd) {
        ret = sis8300drvbcm_set_channel_flattop_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_flattop_end returned %d", ret);
        }
    } else if (function == BCMAcctFineDelay) {
        ret = sis8300drvbcm_set_channel_fine_delay(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_fine_delay returned %d", ret);
        }
    } else if (function == BCMAcctDroopRate) {
        ret = sis8300drvbcm_set_channel_droop_rate(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_droop_rate returned %d", ret);
        }
    } else if (function == BCMAcctUpperThreshold) {
        ret = sis8300drvbcm_set_channel_upper_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_upper_threshold returned %d", ret);
        }
    } else if (function == BCMAcctLowerThreshold) {
        ret = sis8300drvbcm_set_channel_lower_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_lower_threshold returned %d", ret);
        }
    } else if (function == BCMAcctErrantThreshold) {
        ret = sis8300drvbcm_set_channel_errant_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_errant_threshold returned %d", ret);
        }
    } else if (function == BCMAcctLowerWindowStart) {
        ret = sis8300drvbcm_set_channel_lower_window_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_lower_window_start returned %d", ret);
        }
    } else if (function == BCMAcctLowerWindowEnd) {
        ret = sis8300drvbcm_set_channel_lower_window_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_lower_window_end returned %d", ret);
        }
    } else if (function == BCMAcctErrantWindowStart) {
        ret = sis8300drvbcm_set_channel_errant_window_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_errant_window_start returned %d", ret);
        }
    } else if (function == BCMAcctErrantWindowEnd) {
        ret = sis8300drvbcm_set_channel_errant_window_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_errant_window_end returned %d", ret);
        }
    } else if (function == BCMAcctAuxUpperThreshold) {
        ret = sis8300drvbcm_set_channel_aux_upper_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_aux_upper_threshold returned %d", ret);
        }
    } else if (function == BCMAcctAuxLowerThreshold) {
        ret = sis8300drvbcm_set_channel_aux_lower_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_aux_lower_threshold returned %d", ret);
        }
    } else if (function == BCMAcctAuxHysteresisThreshold) {
        ret = sis8300drvbcm_set_channel_aux_hysteresis_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_aux_hysteresis_threshold returned %d", ret);
        }
    } else if (function == BCMAcctBeamAboveThreshold) {
        asynPrintDeviceInfo(pasynUserSelf, "set_channel_beam_above_threshold: addr %d, value %f", addr, value);
        ret = sis8300drvbcm_set_channel_beam_above_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_beam_above_threshold returned %d", ret);
        }
    } else if (function == BCMAcctLeakyCoefficient) {
        ret = sis8300drvbcm_set_channel_leaky_coefficient(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_leaky_coefficient returned %d", ret);
        }
    } else if (function == BCMAcctLeakyThreshold) {
        ret = sis8300drvbcm_set_channel_leaky_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_leaky_threshold returned %d", ret);
        }
    } else if (function == BCMDiffDelay) {
        ret = sis8300drvbcm_set_differential_delay(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_delay returned %d", ret);
        }
    } else if (function == BCMDiffFastThreshold) {
        ret = sis8300drvbcm_set_differential_fast_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_fast_threshold returned %d", ret);
        }
    } else if (function == BCMDiffMediumThreshold) {
        ret = sis8300drvbcm_set_differential_medium_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_medium_threshold returned %d", ret);
        }
    } else if (function == BCMDiffSlowThreshold) {
        ret = sis8300drvbcm_set_differential_slow_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_slow_threshold returned %d", ret);
        }
    } else if (function == BCMDiffRisingWindowStart) {
        ret = sis8300drvbcm_set_differential_rising_window_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_rising_window_start returned %d", ret);
        }
    } else if (function == BCMDiffRisingWindowEnd) {
        ret = sis8300drvbcm_set_differential_rising_window_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_rising_window_end returned %d", ret);
        }
    } else if (function == BCMDiffFallingWindowStart) {
        ret = sis8300drvbcm_set_differential_falling_window_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_falling_window_start returned %d", ret);
        }
    } else if (function == BCMDiffFallingWindowEnd) {
        ret = sis8300drvbcm_set_differential_falling_window_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_falling_window_end returned %d", ret);
        }
    } else if (function >= BCMLutLowerThreshold[0] && function <= BCMLutLowerThreshold[BCM_LUT_SIZE-1]) {
        ret = setupLUTlowerThreshold(addr, function);
    } else if (function >= BCMLutUpperThreshold[0] && function <= BCMLutUpperThreshold[BCM_LUT_SIZE-1]) {
        ret = setupLUTupperThreshold(addr, function);
    } else if (function >= BCMLutMinTriggerPeriod[0] && function <= BCMLutMinTriggerPeriod[BCM_LUT_SIZE-1]) {
        ret = setupLUTminTriggerPeriod(function);
    } else if (function == BCMDiffFastWindowWidth) {
        ret = sis8300drvbcm_set_differential_fast_window_width(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_fast_window_width returned %d", ret);
        }
        ret = sis8300drvbcm_set_differential_fast_window_width_inverse(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_fast_window_width_inverse returned %d", ret);
        }
    } else if (function == BCMDiffMediumWindowWidth) {
        ret = sis8300drvbcm_set_differential_medium_window_width(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_medium_window_width returned %d", ret);
        }
        ret = sis8300drvbcm_set_differential_medium_window_width_inverse(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_medium_window_width_inverse returned %d", ret);
        }
    } else if (function == BCMDiffLeakyCoefficient) {
        ret = sis8300drvbcm_set_differential_leaky_coefficient(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_leaky_coefficient returned %d", ret);
        }
    } else if (function == BCMDiffLeakyThreshold) {
        ret = sis8300drvbcm_set_differential_leaky_threshold(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_differential_leaky_threshold returned %d", ret);
        }
    } else if (function == BCMMinTriggerPeriod) {
        ret = sis8300drvbcm_set_minimum_trigger_period(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_minimum_trigger_period returned %d", ret);
        }
    } else if (function == BCMPulseWidthFilter) {
        ret = sis8300drvbcm_set_pulse_width_filter(mDeviceHandle, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_pulse_width_filter returned %d", ret);
        }
    } else if (function == BCMAcctAutoFlatTopStart) {
        ret = sis8300drvbcm_set_channel_auto_flattop_start(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_auto_flattop_start returned %d", ret);
        }
    } else if (function == BCMAcctAutoFlatTopEnd) {
        ret = sis8300drvbcm_set_channel_auto_flattop_end(mDeviceHandle, addr, value);
        if (ret) {
            asynPrintError(pasynUser, "sis8300drvbcm_set_channel_auto_flattop_end returned %d", ret);
        }
    } else {
        if (function < BCM_FIRST_PARAM) {
            status = sis8300::writeFloat64(pasynUser, value);
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %f", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%f", status, function, value);
    }

    // read back the firmware register values
    // XXX is ignoring the errors here OK?!
    readbackParameters();

    return status;
}

// called when asyn clients call pasynOctet->write()
asynStatus sis8300bcm::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual)
{
    int addr;
    int function = pasynUser->reason;
    const char *name;
    asynStatus status = asynSuccess;
    int ret = 0;

    getAddress(pasynUser, &addr);
    getParamName(function, &name);
    asynPrintDriverInfo(pasynUser, "handling parameter %s(%d) = %s", name, function, value);

    status = setStringParam(addr, function, (char *)value);

    // special handling of EVR provided timestamp
    if (function == SIS8300EvrTimestamp) {
        // let base class do its work first..
        status = sis8300::writeOctet(pasynUser, value, nChars, nActual);
        if (status == 0) {
            // ..now send the updated timestamp to the firmware
            ret = sis8300drvbcm_set_timestamp(mDeviceHandle, mTimeStampSec, mTimeStampNsec);
            if (ret) {
                asynPrintError(pasynUser, "sis8300drvbcm_set_timestamp returned %d", ret);
            }
        }
    }

    if (ret) {
        status = asynError;
        snprintf(mMessageBuffer, MAX_MESSAGE_LENGTH, "[%s,%d,%s] set failed: value %s", portName, addr, name, value);
        setStringParam(SIS8300DriverMessage, mMessageBuffer);
    }

    // do callbacks so higher layers see any changes
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "status=%d function=%d, value=%s", status, function, value);
    }

    // *not* reading back the firmware register values
    // readbackParameters();

    *nActual = nChars;
    return status;
}

// report status of the driver, prints details about the driver if details>0
void sis8300bcm::report(FILE *fp, int details)
{
    // invoke the base class method
    sis8300::report(fp, details);
}

// configuration command, called directly or from iocsh
extern "C" int sis8300bcmConfig(const char *portName, const char *devicePath,
        int numSamples, int maxBuffers, int maxMemory,
        int priority, int stackSize)
{
    new sis8300bcm(portName, devicePath,
            numSamples,
            (maxBuffers < 0) ? 0 : maxBuffers,
            (maxMemory < 0) ? 0 : maxMemory,
            priority, stackSize);
    return(asynSuccess);
}

// code for iocsh registration
static const iocshArg arg0 = {"Port name",     iocshArgString};
static const iocshArg arg1 = {"Device path",   iocshArgString};
static const iocshArg arg2 = {"Num samples",   iocshArgInt};
static const iocshArg arg3 = {"maxBuffers",    iocshArgInt};
static const iocshArg arg4 = {"maxMemory",     iocshArgInt};
static const iocshArg arg5 = {"priority",      iocshArgInt};
static const iocshArg arg6 = {"stackSize",     iocshArgInt};
static const iocshArg * const configArgs[] =  {
    &arg0, &arg1, &arg2, &arg3, &arg4, &arg5, &arg6};
static const iocshFuncDef config = {"sis8300bcmConfigure", 7, configArgs};
static void configCallFunc(const iocshArgBuf *args)
{
    sis8300bcmConfig(args[0].sval, args[1].sval, args[2].ival, args[3].ival,
            args[4].ival, args[5].ival, args[6].ival);
}

static void sis8300bcmRegister(void)
{
    iocshRegister(&config, configCallFunc);
}

extern "C" {
epicsExportRegistrar(sis8300bcmRegister);
}
