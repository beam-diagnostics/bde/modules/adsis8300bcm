/* sis8300bcm.h
 *
 * This is a driver for a BCM based on Struck SIS8300 digitizer.
 *
 * Authors: Hinko Kocevar
 *          Joao Paulo Martins
 *          ESS ERIC, Lund, Sweden
 *
 * Created:  October 8, 2018
 *
 */

#include <stdint.h>
#include <epicsEvent.h>
#include <epicsTime.h>
#include <asynNDArrayDriver.h>

#include <sis8300.h>
#include <sis8300drvbcm.h>

#ifndef SIS8300BCM_H_
#define SIS8300BCM_H_

#define BCM_IRQ_WAIT_TIME                       0
#define BCM_LUT_SIZE                            16

// BCM system wide parameters
#define BCMFwVersionString                           "BCM.FW_VERSION"
// FIXME: FW GIT hash PV should be in adsis8300 module
#define BCMFwGitHashString                           "BCM.FW_GIT_HASH"
#define BCMClockFrequencyMeasString                  "BCM.CLOCK_FREQUENCY_MEAS"
#define BCMTriggerPeriodMeasString                   "BCM.TRIGGER_PERIOD_MEAS"
#define BCMTriggerWidthMeasString                    "BCM.TRIGGER_WIDTH_MEAS"
#define BCMAcqCfgMemoryAddressString                 "BCM.ACQCFG.MEMORY_ADDRESS"
#define BCMAcqCfgNumSamplesString                    "BCM.ACQCFG.NUM_SAMPLES"
#define BCMAcqCfgFractionBitsString                  "BCM.ACQCFG.FRACTION_BITS"
#define BCMAcqCfgFactorString                        "BCM.ACQCFG.FACTOR"
#define BCMAcqCfgOffsetString                        "BCM.ACQCFG.OFFSET"
#define BCMAcqCfgDecimationString                    "BCM.ACQCFG.DECIMATION"
#define BCMAcqCfgScalingString                       "BCM.ACQCFG.SCALING"
#define BCMAcqCfgConvertingString                    "BCM.ACQCFG.CONVERTING"
#define BCMAcqCfgRecordingString                     "BCM.ACQCFG.RECORDING"
#define BCMAcqCfgTickValueString                     "BCM.ACQCFG.TICK_VALUE"
#define BCMMinTriggerPeriodString                    "BCM.MIN_TRIGGER_PERIOD"
#define BCMMaxPulseWidthString                       "BCM.MAX_PULSE_WIDTH"
#define BCMAlarmsClearString                         "BCM.ALARMS_CLEAR"
#define BCMAlarmsControlString                       "BCM.ALARMS_CONTROL"
#define BCMAlarmAuxiliaryClockControlString          "BCM.ALARM.AUXILIARY_CLOCK_CONTROL"
#define BCMAlarmProcessingClockControlString         "BCM.ALARM.PROCESSING_CLOCK_CONTROL"
#define BCMAlarmTriggerTooWideControlString          "BCM.ALARM.TRIGGER_TOO_WIDE_CONTROL"
#define BCMAlarmTriggerTooNarrowControlString        "BCM.ALARM.TRIGGER_TOO_NARROW_CONTROL"
#define BCMAlarmTriggerPeriodTooShortControlString   "BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_CONTROL"
#define BCMAlarmAuxiliaryClockHoldString             "BCM.ALARM.AUXILIARY_CLOCK_HOLD"
#define BCMAlarmProcessingClockHoldString            "BCM.ALARM.PROCESSING_CLOCK_HOLD"
#define BCMAlarmTriggerTooWideHoldString             "BCM.ALARM.TRIGGER_TOO_WIDE_HOLD"
#define BCMAlarmTriggerTooNarrowHoldString           "BCM.ALARM.TRIGGER_TOO_NARROW_HOLD"
#define BCMAlarmTriggerPeriodTooShortHoldString      "BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_HOLD"
#define BCMAlarmAuxiliaryClockFirstString            "BCM.ALARM.AUXILIARY_CLOCK_FIRST"
#define BCMAlarmProcessingClockFirstString           "BCM.ALARM.PROCESSING_CLOCK_FIRST"
#define BCMAlarmTriggerTooWideFirstString            "BCM.ALARM.TRIGGER_TOO_WIDE_FIRST"
#define BCMAlarmTriggerTooNarrowFirstString          "BCM.ALARM.TRIGGER_TOO_NARROW_FIRST"
#define BCMAlarmTriggerPeriodTooShortFirstString     "BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_FIRST"
#define BCMAlarmAuxiliaryClockDirectString           "BCM.ALARM.AUXILIARY_CLOCK_DIRECT"
#define BCMAlarmProcessingClockDirectString          "BCM.ALARM.PROCESSING_CLOCK_DIRECT"
#define BCMAlarmTriggerTooWideDirectString           "BCM.ALARM.TRIGGER_TOO_WIDE_DIRECT"
#define BCMAlarmTriggerTooNarrowDirectString         "BCM.ALARM.TRIGGER_TOO_NARROW_DIRECT"
#define BCMAlarmTriggerPeriodTooShortDirectString    "BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_DIRECT"
#define BCMAlarmLUTMinTriggerPeriodHoldString        "BCM.ALARM.LUT_MIN_TRIGGER_PERIOD_HOLD"
#define BCMAlarmLUTMinTriggerPeriodFirstString       "BCM.ALARM.LUT_MIN_TRIGGER_PERIOD_FIRST"
#define BCMAlarmLUTMinTriggerPeriodDirectString      "BCM.ALARM.LUT_MIN_TRIGGER_PERIOD_DIRECT"
#define BCMAlarmDiffLUTDestinationModeDirectString   "BCM.ALARM.DIFF_LUT_DESTINATION_MODE_DIRECT"
#define BCMMainClockMissingString                    "BCM.MAIN_CLOCK_MISSING"
#define BCMAuxiliaryClockMissingString               "BCM.AUXILIARY_CLOCK_MISSING"
#define BCMReadyString                               "BCM.READY"
#define BCMLUTControlString                          "BCM.LUT_CONTROL"
#define BCMAcquisitionTriggerSourceString            "BCM.ACQUISITION_TRIGGER_SOURCE"
#define BCMCrateIDString                             "BCM.CRATE_ID"
#define BCMPulseWidthFilterString                    "BCM.PULSE_WIDTH_FILTER"
#define BCMBeamTriggerSourceString                   "BCM.BEAM_TRIGGER_SOURCE"
#define BCMReadyFPGAString                           "BCM.READY_FPGA"
#define BCMBeamPermitFPGAString                      "BCM.BEAM_PERMIT_FPGA"
#define BCMHighVoltagePresenceString                 "BCM.HIGH_VOLTAGE_PRESENCE"
#define BCMHighVoltageOKString                       "BCM.HIGH_VOLTAGE_OK"
#define BCMEnableCalibrationPulseString              "BCM.ENABLE_CALIBRATION_PULSE"
// ACCT block, channel specific
#define BCMAcctTriggerSourceString                   "BCM.ACCT.TRIGGER_SOURCE"
#define BCMAcctPulseChargeString                     "BCM.ACCT.PULSE_CHARGE"
#define BCMAcctFlatTopChargeString                   "BCM.ACCT.FLATTOP_CHARGE"
#define BCMAcctPulseWidthString                      "BCM.ACCT.PULSE_WIDTH"
#define BCMAcctAdcScaleString                        "BCM.ACCT.ADC_SCALE"
#define BCMAcctAdcOffsetString                       "BCM.ACCT.ADC_OFFSET"
#define BCMAcctFlatTopStartString                    "BCM.ACCT.FLATTOP_START"
#define BCMAcctFlatTopEndString                      "BCM.ACCT.FLATTOP_END"
#define BCMAcctFlatTopCurrentString                  "BCM.ACCT.FLATTOP_CURRENT"
#define BCMAcctFineDelayString                       "BCM.ACCT.FINE_DELAY"
#define BCMAcctDroopRateString                       "BCM.ACCT.DROOP_RATE"
#define BCMAcctDroopCompensatingString               "BCM.ACCT.DROOP_COMPENSATING"
#define BCMAcctNoiseFilteringString                  "BCM.ACCT.NOISE_FILTERING"
#define BCMAcctBaseliningBeforeString                "BCM.ACCT.BASELINING_BEFORE"
#define BCMAcctBaseliningAfterString                 "BCM.ACCT.BASELINING_AFTER"
#define BCMAcctDcBlockingString                      "BCM.ACCT.DC_BLOCKING"
#define BCMAcctUpperThresholdString                  "BCM.ACCT.UPPER_THRESHOLD"
#define BCMAcctLowerThresholdString                  "BCM.ACCT.LOWER_THRESHOLD"
#define BCMAcctErrantThresholdString                 "BCM.ACCT.ERRANT_THRESHOLD"
#define BCMAcctAlarmUpperControlString               "BCM.ACCT.ALARM.UPPER_CONTROL"
#define BCMAcctAlarmLowerControlString               "BCM.ACCT.ALARM.LOWER_CONTROL"
#define BCMAcctAlarmErrantControlString              "BCM.ACCT.ALARM.ERRANT_CONTROL"
#define BCMAcctAlarmTriggerControlString             "BCM.ACCT.ALARM.TRIGGER_CONTROL"
#define BCMAcctAlarmLimitControlString               "BCM.ACCT.ALARM.LIMIT_CONTROL"
#define BCMAcctAlarmAdcOverflowControlString         "BCM.ACCT.ALARM.ADC_OVERFLOW_CONTROL"
#define BCMAcctAlarmAdcUnderflowControlString        "BCM.ACCT.ALARM.ADC_UNDERFLOW_CONTROL"
#define BCMAcctAlarmAdcStuckControlString            "BCM.ACCT.ALARM.ADC_STUCK_CONTROL"
#define BCMAcctAlarmAiuFaultControlString            "BCM.ACCT.ALARM.AIU_FAULT_CONTROL"
#define BCMAcctAlarmChargeTooHighControlString       "BCM.ACCT.ALARM.CHARGE_TOO_HIGH_CONTROL"
#define BCMAcctAlarmUpperHoldString                  "BCM.ACCT.ALARM.UPPER_HOLD"
#define BCMAcctAlarmLowerHoldString                  "BCM.ACCT.ALARM.LOWER_HOLD"
#define BCMAcctAlarmErrantHoldString                 "BCM.ACCT.ALARM.ERRANT_HOLD"
#define BCMAcctAlarmTriggerHoldString                "BCM.ACCT.ALARM.TRIGGER_HOLD"
#define BCMAcctAlarmLimitHoldString                  "BCM.ACCT.ALARM.LIMIT_HOLD"
#define BCMAcctAlarmAdcOverflowHoldString            "BCM.ACCT.ALARM.ADC_OVERFLOW_HOLD"
#define BCMAcctAlarmAdcUnderflowHoldString           "BCM.ACCT.ALARM.ADC_UNDERFLOW_HOLD"
#define BCMAcctAlarmAdcStuckHoldString               "BCM.ACCT.ALARM.ADC_STUCK_HOLD"
#define BCMAcctAlarmAiuFaultHoldString               "BCM.ACCT.ALARM.AIU_FAULT_HOLD"
#define BCMAcctAlarmChargeTooHighHoldString          "BCM.ACCT.ALARM.CHARGE_TOO_HIGH_HOLD"
#define BCMAcctAlarmLUTUpperThresholdHoldString      "BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_HOLD"
#define BCMAcctAlarmLUTLowerThresholdHoldString      "BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_HOLD"
#define BCMAcctAlarmLUTPulseLengthHoldString         "BCM.ACCT.ALARM.LUT_PULSE_LENGTH_HOLD"
#define BCMAcctAlarmLUTDestinationModeHoldString     "BCM.ACCT.ALARM.LUT_DESTINATION_MODE_HOLD"
#define BCMAcctAlarmUpperFirstString                 "BCM.ACCT.ALARM.UPPER_FIRST"
#define BCMAcctAlarmLowerFirstString                 "BCM.ACCT.ALARM.LOWER_FIRST"
#define BCMAcctAlarmErrantFirstString                "BCM.ACCT.ALARM.ERRANT_FIRST"
#define BCMAcctAlarmTriggerFirstString               "BCM.ACCT.ALARM.TRIGGER_FIRST"
#define BCMAcctAlarmLimitFirstString                 "BCM.ACCT.ALARM.LIMIT_FIRST"
#define BCMAcctAlarmAdcOverflowFirstString           "BCM.ACCT.ALARM.ADC_OVERFLOW_FIRST"
#define BCMAcctAlarmAdcUnderflowFirstString          "BCM.ACCT.ALARM.ADC_UNDERFLOW_FIRST"
#define BCMAcctAlarmAdcStuckFirstString              "BCM.ACCT.ALARM.ADC_STUCK_FIRST"
#define BCMAcctAlarmAiuFaultFirstString              "BCM.ACCT.ALARM.AIU_FAULT_FIRST"
#define BCMAcctAlarmChargeTooHighFirstString         "BCM.ACCT.ALARM.CHARGE_TOO_HIGH_FIRST"
#define BCMAcctAlarmLUTUpperThresholdFirstString     "BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_FIRST"
#define BCMAcctAlarmLUTLowerThresholdFirstString     "BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_FIRST"
#define BCMAcctAlarmLUTPulseLengthFirstString        "BCM.ACCT.ALARM.LUT_PULSE_LENGTH_FIRST"
#define BCMAcctAlarmLUTDestinationModeFirstString    "BCM.ACCT.ALARM.LUT_DESTINATION_MODE_FIRST"
#define BCMAcctAlarmUpperDirectString                "BCM.ACCT.ALARM.UPPER_DIRECT"
#define BCMAcctAlarmLowerDirectString                "BCM.ACCT.ALARM.LOWER_DIRECT"
#define BCMAcctAlarmErrantDirectString               "BCM.ACCT.ALARM.ERRANT_DIRECT"
#define BCMAcctAlarmTriggerDirectString              "BCM.ACCT.ALARM.TRIGGER_DIRECT"
#define BCMAcctAlarmLimitDirectString                "BCM.ACCT.ALARM.LIMIT_DIRECT"
#define BCMAcctAlarmAdcOverflowDirectString          "BCM.ACCT.ALARM.ADC_OVERFLOW_DIRECT"
#define BCMAcctAlarmAdcUnderflowDirectString         "BCM.ACCT.ALARM.ADC_UNDERFLOW_DIRECT"
#define BCMAcctAlarmAdcStuckDirectString             "BCM.ACCT.ALARM.ADC_STUCK_DIRECT"
#define BCMAcctAlarmAiuFaultDirectString             "BCM.ACCT.ALARM.AIU_FAULT_DIRECT"
#define BCMAcctAlarmChargeTooHighDirectString        "BCM.ACCT.ALARM.CHARGE_TOO_HIGH_DIRECT"
#define BCMAcctAlarmLUTUpperThresholdDirectString    "BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_DIRECT"
#define BCMAcctAlarmLUTLowerThresholdDirectString    "BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_DIRECT"
#define BCMAcctAlarmLUTPulseLengthDirectString       "BCM.ACCT.ALARM.LUT_PULSE_LENGTH_DIRECT"
#define BCMAcctAlarmLUTDestinationModeDirectString   "BCM.ACCT.ALARM.LUT_DESTINATION_MODE_DIRECT"
#define BCMAcctBeamExistsString                      "BCM.ACCT.BEAM_EXISTS"
#define BCMAcctMaxPulseLengthString                  "BCM.ACCT.MAX_PULSE_LENGTH"
#define BCMAcctLowerWindowStartString                "BCM.ACCT.LOWER_WINDOW_START"
#define BCMAcctLowerWindowEndString                  "BCM.ACCT.LOWER_WINDOW_END"
#define BCMAcctErrantWindowStartString               "BCM.ACCT.ERRANT_WINDOW_START"
#define BCMAcctErrantWindowEndString                 "BCM.ACCT.ERRANT_WINDOW_END"
#define BCMAcctCalibrationSample1String              "BCM.ACCT.CALIBRATION_SAMPLE1"
#define BCMAcctCalibrationSample2String              "BCM.ACCT.CALIBRATION_SAMPLE2"
#define BCMAcctCalibrationSample3String              "BCM.ACCT.CALIBRATION_SAMPLE3"
#define BCMAcctCalibrationSample4String              "BCM.ACCT.CALIBRATION_SAMPLE4"
#define BCMAcctAuxUpperThresholdString               "BCM.ACCT.AUX_UPPER_THRESHOLD"
#define BCMAcctAuxLowerThresholdString               "BCM.ACCT.AUX_LOWER_THRESHOLD"
#define BCMAcctAuxHysteresisThresholdString          "BCM.ACCT.AUX_HYSTERESIS_THRESHOLD"
#define BCMAcctBeamAboveThresholdString              "BCM.ACCT.BEAM_ABOVE_THRESHOLD"
#define BCMAcctLeakyCoefficientString                "BCM.ACCT.LEAKY_INTEGRATOR_COEFFICIENT"
#define BCMAcctLeakyThresholdString                  "BCM.ACCT.LEAKY_INTEGRATOR_THRESHOLD"
#define BCMAcctTriggerWidthString                    "BCM.ACCT.TRIGGER_WIDTH"
#define BCMAcctAutoFlatTopStartString                "BCM.ACCT.AUTO_FLATTOP_START"
#define BCMAcctAutoFlatTopEndString                  "BCM.ACCT.AUTO_FLATTOP_END"
#define BCMAcctAutoFlatTopEnableString               "BCM.ACCT.AUTO_FLATTOP_ENABLE"
#define BCMAcctBeamAbsenceString                     "BCM.ACCT.BEAM_ABSENCE"
// DIFF block, channel specific
#define BCMDiffSourceAString                         "BCM.DIFF.SOURCE_A"
#define BCMDiffSourceBString                         "BCM.DIFF.SOURCE_B"
#define BCMDiffDelayString                           "BCM.DIFF.DELAY"
#define BCMDiffFastThresholdString                   "BCM.DIFF.FAST_THRESHOLD"
#define BCMDiffFastMinimumString                     "BCM.DIFF.FAST_MINIMUM"
#define BCMDiffFastMaximumString                     "BCM.DIFF.FAST_MAXIMUM"
#define BCMDiffAlarmFastControlString                "BCM.DIFF.ALARM.FAST_CONTROL"
#define BCMDiffAlarmFastHoldString                   "BCM.DIFF.ALARM.FAST_HOLD"
#define BCMDiffAlarmFastFirstString                  "BCM.DIFF.ALARM.FAST_FIRST"
#define BCMDiffAlarmFastDirectString                 "BCM.DIFF.ALARM.FAST_DIRECT"
#define BCMDiffMediumThresholdString                 "BCM.DIFF.MEDIUM_THRESHOLD"
#define BCMDiffMediumMinimumString                   "BCM.DIFF.MEDIUM_MINIMUM"
#define BCMDiffMediumMaximumString                   "BCM.DIFF.MEDIUM_MAXIMUM"
#define BCMDiffAlarmMediumControlString              "BCM.DIFF.ALARM.MEDIUM_CONTROL"
#define BCMDiffAlarmMediumHoldString                 "BCM.DIFF.ALARM.MEDIUM_HOLD"
#define BCMDiffAlarmMediumFirstString                "BCM.DIFF.ALARM.MEDIUM_FIRST"
#define BCMDiffAlarmMediumDirectString               "BCM.DIFF.ALARM.MEDIUM_DIRECT"
#define BCMDiffSlowThresholdString                   "BCM.DIFF.SLOW_THRESHOLD"
#define BCMDiffSlowMinimumString                     "BCM.DIFF.SLOW_MINIMUM"
#define BCMDiffSlowMaximumString                     "BCM.DIFF.SLOW_MAXIMUM"
#define BCMDiffAlarmSlowControlString                "BCM.DIFF.ALARM.SLOW_CONTROL"
#define BCMDiffAlarmSlowHoldString                   "BCM.DIFF.ALARM.SLOW_HOLD"
#define BCMDiffAlarmSlowFirstString                  "BCM.DIFF.ALARM.SLOW_FIRST"
#define BCMDiffAlarmSlowDirectString                 "BCM.DIFF.ALARM.SLOW_DIRECT"
#define BCMDiffRisingWindowStartString               "BCM.DIFF.RISING_WINDOW_START"
#define BCMDiffRisingWindowEndString                 "BCM.DIFF.RISING_WINDOW_END"
#define BCMDiffFallingWindowStartString              "BCM.DIFF.FALLING_WINDOW_START"
#define BCMDiffFallingWindowEndString                "BCM.DIFF.FALLING_WINDOW_END"
#define BCMDiffWsBetweenString                       "BCM.DIFF.WS_BETWEEN"
#define BCMDiffEmuBetweenString                      "BCM.DIFF.EMU_BETWEEN"
#define BCMDiffRfqBetweenString                      "BCM.DIFF.RFQ_BETWEEN"
#define BCMDiffFastWindowWidthString                 "BCM.DIFF.FAST_WINDOW_WIDTH"
#define BCMDiffMediumWindowWidthString               "BCM.DIFF.MEDIUM_WINDOW_WIDTH"
#define BCMDiffFastWindowWidthInverseString          "BCM.DIFF.FAST_WINDOW_WIDTH_INVERSE"
#define BCMDiffMediumWindowWidthInverseString        "BCM.DIFF.MEDIUM_WINDOW_WIDTH_INVERSE"
#define BCMDiffLeakyCoefficientString                "BCM.DIFF.LEAKY_INTEGRATOR_COEFFICIENT"
#define BCMDiffLeakyThresholdString                  "BCM.DIFF.LEAKY_INTEGRATOR_THRESHOLD"
#define BCMDiffAlarmLeakyControlString               "BCM.DIFF.ALARM.LEAKY_CONTROL"
#define BCMDiffAlarmLeakyHoldString                  "BCM.DIFF.ALARM.LEAKY_HOLD"
#define BCMDiffAlarmLeakyFirstString                 "BCM.DIFF.ALARM.LEAKY_FIRST"
#define BCMDiffAlarmLeakyDirectString                "BCM.DIFF.ALARM.LEAKY_DIRECT"
#define BCMDiffInterlockEnableString                 "BCM.DIFF.INTERLOCK_ENABLE"
// PROBE block, channel specific
#define BCMProbeChannelString                        "BCM.PROBE.CHANNEL"
#define BCMProbeSourceString                         "BCM.PROBE.SOURCE"
// FIBER block, channel specific
#define BCMFiberOutDataSelectString                  "BCM.FIBER.OUT_DATA_SELECT"
#define BCMFiberOutDataEnableString                  "BCM.FIBER.OUT_DATA_ENABLE"
#define BCMFiberSFPPresentString                     "BCM.FIBER.SFP_PRESENT"
#define BCMFiberLaneUpString                         "BCM.FIBER.LANE_UP"
#define BCMFiberChannelUpString                      "BCM.FIBER.CHANNEL_UP"
#define BCMFiberHardwareErrorString                  "BCM.FIBER.HARDWARE_ERROR"
#define BCMFiberSoftwareErrorString                  "BCM.FIBER.SOFTWARE_ERROR"
#define BCMFiberResetString                          "BCM.FIBER.RESET"
// LUT, board specific, 16 locations
#define BCMLutDestinationIDString                    "BCM.LUT%d.DEST_ID"
#define BCMLutMinTriggerPeriodString                 "BCM.LUT%d.MIN_TRIGGER_PERIOD"
// LUT, channel specific, 16 locations
#define BCMLutModeIDString                           "BCM.LUT%d.MODE_ID"
#define BCMLutMaxPulseLengthString                   "BCM.LUT%d.MAX_PULSE_LENGTH"
#define BCMLutLowerThresholdString                   "BCM.LUT%d.LOWER_THRESHOLD"
#define BCMLutUpperThresholdString                   "BCM.LUT%d.UPPER_THRESHOLD"
#define BCMLutBeamExistsString                       "BCM.LUT%d.BEAM_EXISTS"
#define BCMLutInterlockEnableString                  "BCM.LUT%d.INTERLOCK_ENABLE"

class epicsShareClass sis8300bcm : public sis8300 {
public:
    sis8300bcm(const char *portName, const char *devicePath,
            int numSamples, int maxBuffers, size_t maxMemory,
            int priority, int stackSize);
    virtual ~sis8300bcm();

    // these are the methods that we override from adsis8300
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);
    virtual void report(FILE *fp, int details);

protected:
    // BCM system parameters
    int BCMFwVersion;
    #define BCM_FIRST_PARAM BCMFwVersion
    int BCMFwGitHash;
    int BCMClockFrequencyMeas;
    int BCMTriggerPeriodMeas;
    int BCMTriggerWidthMeas;
    int BCMAcqCfgMemoryAddress;
    int BCMAcqCfgNumSamples;
    int BCMAcqCfgFractionBits;
    int BCMAcqCfgFactor;
    int BCMAcqCfgOffset;
    int BCMAcqCfgDecimation;
    int BCMAcqCfgScaling;
    int BCMAcqCfgConverting;
    int BCMAcqCfgRecording;
    int BCMAcqCfgTickValue;
    int BCMMinTriggerPeriod;
    int BCMMaxPulseWidth;
    int BCMAlarmsClear;
    int BCMAlarmsControl;
    int BCMAlarmAuxiliaryClockControl;
    int BCMAlarmProcessingClockControl;
    int BCMAlarmTriggerTooWideControl;
    int BCMAlarmTriggerTooNarrowControl;
    int BCMAlarmTriggerPeriodTooShortControl;
    int BCMAlarmAuxiliaryClockHold;
    int BCMAlarmProcessingClockHold;
    int BCMAlarmTriggerTooWideHold;
    int BCMAlarmTriggerTooNarrowHold;
    int BCMAlarmTriggerPeriodTooShortHold;
    int BCMAlarmAuxiliaryClockFirst;
    int BCMAlarmProcessingClockFirst;
    int BCMAlarmTriggerTooWideFirst;
    int BCMAlarmTriggerTooNarrowFirst;
    int BCMAlarmTriggerPeriodTooShortFirst;
    int BCMAlarmAuxiliaryClockDirect;
    int BCMAlarmProcessingClockDirect;
    int BCMAlarmTriggerTooWideDirect;
    int BCMAlarmTriggerTooNarrowDirect;
    int BCMAlarmTriggerPeriodTooShortDirect;
    int BCMAlarmLUTMinTriggerPeriodHold;
    int BCMAlarmLUTMinTriggerPeriodFirst;
    int BCMAlarmLUTMinTriggerPeriodDirect;
    int BCMAlarmDiffLUTDestinationModeDirect;
    int BCMMainClockMissing;
    int BCMAuxiliaryClockMissing;
    int BCMReady;
    int BCMLUTControl;
    int BCMAcquisitionTriggerSource;
    int BCMCrateID;
    int BCMPulseWidthFilter;
    int BCMBeamTriggerSource;
    int BCMReadyFPGA;
    int BCMBeamPermitFPGA;
    int BCMHighVoltagePresence;
    int BCMHighVoltageOK;
    int BCMEnableCalibrationPulse;
    // ACCT block, channel specific
    int BCMAcctTriggerSource;
    int BCMAcctPulseCharge;
    int BCMAcctFlatTopCharge;
    int BCMAcctPulseWidth;
    int BCMAcctAdcScale;
    int BCMAcctAdcOffset;
    int BCMAcctFlatTopStart;
    int BCMAcctFlatTopEnd;
    int BCMAcctFlatTopCurrent;
    int BCMAcctFineDelay;
    int BCMAcctDroopRate;
    int BCMAcctDroopCompensating;
    int BCMAcctNoiseFiltering;
    int BCMAcctBaseliningBefore;
    int BCMAcctBaseliningAfter;
    int BCMAcctDcBlocking;
    int BCMAcctUpperThreshold;
    int BCMAcctLowerThreshold;
    int BCMAcctErrantThreshold;
    int BCMAcctAlarmUpperControl;
    int BCMAcctAlarmLowerControl;
    int BCMAcctAlarmErrantControl;
    int BCMAcctAlarmTriggerControl;
    int BCMAcctAlarmLimitControl;
    int BCMAcctAlarmAdcOverflowControl;
    int BCMAcctAlarmAdcUnderflowControl;
    int BCMAcctAlarmAdcStuckControl;
    int BCMAcctAlarmAiuFaultControl;
    int BCMAcctAlarmChargeTooHighControl;
    int BCMAcctAlarmUpperHold;
    int BCMAcctAlarmLowerHold;
    int BCMAcctAlarmErrantHold;
    int BCMAcctAlarmTriggerHold;
    int BCMAcctAlarmLimitHold;
    int BCMAcctAlarmAdcOverflowHold;
    int BCMAcctAlarmAdcUnderflowHold;
    int BCMAcctAlarmAdcStuckHold;
    int BCMAcctAlarmAiuFaultHold;
    int BCMAcctAlarmChargeTooHighHold;
    int BCMAcctAlarmLUTUpperThresholdHold;
    int BCMAcctAlarmLUTLowerThresholdHold;
    int BCMAcctAlarmLUTPulseLengthHold;
    int BCMAcctAlarmLUTDestinationModeHold;
    int BCMAcctAlarmUpperFirst;
    int BCMAcctAlarmLowerFirst;
    int BCMAcctAlarmErrantFirst;
    int BCMAcctAlarmTriggerFirst;
    int BCMAcctAlarmLimitFirst;
    int BCMAcctAlarmAdcOverflowFirst;
    int BCMAcctAlarmAdcUnderflowFirst;
    int BCMAcctAlarmAdcStuckFirst;
    int BCMAcctAlarmAiuFaultFirst;
    int BCMAcctAlarmChargeTooHighFirst;
    int BCMAcctAlarmLUTUpperThresholdFirst;
    int BCMAcctAlarmLUTLowerThresholdFirst;
    int BCMAcctAlarmLUTPulseLengthFirst;
    int BCMAcctAlarmLUTDestinationModeFirst;
    int BCMAcctAlarmUpperDirect;
    int BCMAcctAlarmLowerDirect;
    int BCMAcctAlarmErrantDirect;
    int BCMAcctAlarmTriggerDirect;
    int BCMAcctAlarmLimitDirect;
    int BCMAcctAlarmAdcOverflowDirect;
    int BCMAcctAlarmAdcUnderflowDirect;
    int BCMAcctAlarmAdcStuckDirect;
    int BCMAcctAlarmAiuFaultDirect;
    int BCMAcctAlarmChargeTooHighDirect;
    int BCMAcctAlarmLUTUpperThresholdDirect;
    int BCMAcctAlarmLUTLowerThresholdDirect;
    int BCMAcctAlarmLUTPulseLengthDirect;
    int BCMAcctAlarmLUTDestinationModeDirect;
    int BCMAcctBeamExists;
    int BCMAcctMaxPulseLength;
    int BCMAcctLowerWindowStart;
    int BCMAcctLowerWindowEnd;
    int BCMAcctErrantWindowStart;
    int BCMAcctErrantWindowEnd;
    int BCMAcctCalibrationSample1;
    int BCMAcctCalibrationSample2;
    int BCMAcctCalibrationSample3;
    int BCMAcctCalibrationSample4;
    int BCMAcctAuxUpperThreshold;
    int BCMAcctAuxLowerThreshold;
    int BCMAcctAuxHysteresisThreshold;
    int BCMAcctBeamAboveThreshold;
    int BCMAcctLeakyCoefficient;
    int BCMAcctLeakyThreshold;
    int BCMAcctTriggerWidth;
    int BCMAcctAutoFlatTopStart;
    int BCMAcctAutoFlatTopEnd;
    int BCMAcctAutoFlatTopEnable;
    int BCMAcctBeamAbsence;
    // DIFF block, channel specific
    int BCMDiffSourceA;
    int BCMDiffSourceB;
    int BCMDiffDelay;
    int BCMDiffFastThreshold;
    int BCMDiffFastMinimum;
    int BCMDiffFastMaximum;
    int BCMDiffAlarmFastControl;
    int BCMDiffAlarmFastHold;
    int BCMDiffAlarmFastFirst;
    int BCMDiffAlarmFastDirect;
    int BCMDiffMediumThreshold;
    int BCMDiffMediumMinimum;
    int BCMDiffMediumMaximum;
    int BCMDiffAlarmMediumControl;
    int BCMDiffAlarmMediumHold;
    int BCMDiffAlarmMediumFirst;
    int BCMDiffAlarmMediumDirect;
    int BCMDiffSlowThreshold;
    int BCMDiffSlowMinimum;
    int BCMDiffSlowMaximum;
    int BCMDiffAlarmSlowControl;
    int BCMDiffAlarmSlowHold;
    int BCMDiffAlarmSlowFirst;
    int BCMDiffAlarmSlowDirect;
    int BCMDiffRisingWindowStart;
    int BCMDiffRisingWindowEnd;
    int BCMDiffFallingWindowStart;
    int BCMDiffFallingWindowEnd;
    int BCMDiffWsBetween;
    int BCMDiffEmuBetween;
    int BCMDiffRfqBetween;
    int BCMDiffFastWindowWidth;
    int BCMDiffMediumWindowWidth;
    int BCMDiffFastWindowWidthInverse;
    int BCMDiffMediumWindowWidthInverse;
    int BCMDiffLeakyCoefficient;
    int BCMDiffLeakyThreshold;
    int BCMDiffAlarmLeakyControl;
    int BCMDiffAlarmLeakyHold;
    int BCMDiffAlarmLeakyFirst;
    int BCMDiffAlarmLeakyDirect;
    int BCMDiffInterlockEnable;
    // PROBE block, channel specific
    int BCMProbeChannel;
    int BCMProbeSource;
    // FIBER block, channel specific
    int BCMFiberOutDataSelect;
    int BCMFiberOutDataEnable;
    int BCMFiberSFPPresent;
    int BCMFiberLaneUp;
    int BCMFiberChannelUp;
    int BCMFiberHardwareError;
    int BCMFiberSoftwareError;
    int BCMFiberReset;
    // LUT, board specific
    int BCMLutDestinationID[BCM_LUT_SIZE];
    int BCMLutMinTriggerPeriod[BCM_LUT_SIZE];
    // LUT, channel specific
    int BCMLutBeamExists[BCM_LUT_SIZE];
    int BCMLutModeID[BCM_LUT_SIZE];
    int BCMLutMaxPulseLength[BCM_LUT_SIZE];
    int BCMLutLowerThreshold[BCM_LUT_SIZE];
    int BCMLutUpperThreshold[BCM_LUT_SIZE];
    int BCMLutInterlockEnable[BCM_LUT_SIZE];

private:
    // these are the methods that we override from sis8300
    int armDevice();
    int acquireArrays();
    int initDevice();
    int readbackParameters();
    int deviceDone();
    // these are the methods that are new to this class
    int setupProbe(unsigned int addr);
    int setupLUTmaxPulseLength(unsigned int addr, int function);
    int setupLUTlowerThreshold(unsigned int addr, int function);
    int setupLUTupperThreshold(unsigned int addr, int function);
    int setupLUTminTriggerPeriod(int function);
    int setupLUTmasking(int function);
    int setupLUTinterlockEnable(int function);
};

#endif /* SIS8300BCM_H_ */
