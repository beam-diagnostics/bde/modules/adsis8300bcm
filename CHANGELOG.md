# Beam Current Monitor module CHANGELOG

## 0.9.14
- [ PBIBCM-241 ](https://jira.esss.lu.se/browse/PBIBCM-241)  Add differential interlock enable/disable
- [ PBIBCM-243 ](https://jira.esss.lu.se/browse/PBIBCM-243)  Add range limit to Fast and Medium Window Width
- [ ICSHWI-19374](https://jira.esss.lu.se/browse/ICSHWI-19374) Fix PV security access ASG fields

## 0.9.13
- [ PBIBCM-236 ](https://jira.esss.lu.se/browse/PBIBCM-236)  Increase medium speed diff moving averager 

## 0.9.12
- [ PBIBCM-209 ](https://jira.esss.lu.se/browse/PBIBCM-209)  Extend options of Trigger Source PVs
- [ PBIBCM-143 ](https://jira.esss.lu.se/browse/PBIBCM-143)  Add Leaky Integrator
- [ PBIBCM-213 ](https://jira.esss.lu.se/browse/PBIBCM-213)  Fix timestamp discrepancy
- [ PBIBCM-215 ](https://jira.esss.lu.se/browse/PBIBCM-215)  Add autosave to LUT PVs (+ changes on st.cmd file)
- [ ICSHWI-9569](https://jira.esss.lu.se/browse/ICSHWI-9569) Fix error log on setting AdcScale with auto calibration
- [ PBIBCM-211 ](https://jira.esss.lu.se/browse/PBIBCM-211)  Fiber Optic PVs
- [ PBIBCM-217 ](https://jira.esss.lu.se/browse/PBIBCM-217)  Flattop ROI automatic mode
- [ PBIBCM-227 ](https://jira.esss.lu.se/browse/PBIBCM-227)  Fix Droop Rate and Adc Scale calculation
- [ PBIBCM-225 ](https://jira.esss.lu.se/browse/PBIBCM-225)  Fix AlarmControl PV issue
- [ PBIBCM-228 ](https://jira.esss.lu.se/browse/PBIBCM-228)  Add auxiliary upper/lower/hysteresis threshold PVs
- [ PBIBCM-223 ](https://jira.esss.lu.se/browse/PBIBCM-223)  Add beam absence signals per ACCT channel

## 0.9.11
- [ PBIBCM-146 ](https://jira.esss.lu.se/browse/PBIBCM-146)  Add crate ID PV
- [ PBIBCM-152 ](https://jira.esss.lu.se/browse/PBIBCM-152)  Auto-calibrator
- [ PBIBCM-200 ](https://jira.esss.lu.se/browse/PBIBCM-200)  Remove restricted access from ROI PVs
- [ PBIBCM-201 ](https://jira.esss.lu.se/browse/PBIBCM-201)  Define more discrete signals on probe channel 4
- [ PBIBCM-206 ](https://jira.esss.lu.se/browse/PBIBCM-206)  Add pulse width filter length PV
- [ PBIBCM-207 ](https://jira.esss.lu.se/browse/PBIBCM-207)  Independent beam exist threshold for each channel 		

## 0.9.10
- [ PBIBCM-195 ](https://jira.esss.lu.se/browse/PBIBCM-195)  Add BeamOverThreshold PV as critical
- [ ICSHWI-5688](https://jira.esss.lu.se/browse/ICSHWI-5688) Remove NumSamples per channel from autosave

## 0.9.9
- [ PBIBCM-189 ](https://jira.esss.lu.se/browse/PBIBCM-189)  Add LUT PVs for MinTriggerPeriod  
- [ PBIBCM-190 ](https://jira.esss.lu.se/browse/PBIBCM-190)  Add ACCT beam exists PVs

## 0.9.8
- [ PBIBCM-188 ](https://jira.esss.lu.se/browse/PBIBCM-188)  Fix MinTriggerPeriod readback parameter type 

## 0.9.7
- [ PBIBCM-185 ](https://jira.esss.lu.se/browse/PBIBCM-185)  Add High Voltage Presence and OK signals
 
## 0.9.6
- [ CAP-26     ](https://jira.esss.lu.se/browse/CAP-26)      Add access security

## 0.9.5
- [ PBIBCM-183 ](https://jira.esss.lu.se/browse/PBIBCM-183)  Update minimum value of MinTriggerPeriod

## 0.9.4
- [ PBIBCM-178 ](https://jira.esss.lu.se/browse/PBIBCM-178)  Update resolution of trigger repetition rate threshold

## 0.9.3
- [ PBIBCM-174 ](https://jira.esss.lu.se/browse/PBIBCM-174)  Update measured trigger width window to 32-bit

