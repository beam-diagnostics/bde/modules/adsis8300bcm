
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: ACCT record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Name
    - ACCT name.
    - stringout
    -
    -
  * - $(P)$(R)TriggerSource, $(P)$(R)TriggerSourceR
    - Trigger synchronized to the beam.
      See also :ref:`trigger_types`.
    - mbbo, mbbi
    - write, read
    - BCM.ACCT.TRIGGER_SOURCE
  * - $(P)$(R)PulseChargeR
    - Measured pulse charge.
    - ai
    - read
    - BCM.ACCT.PULSE_CHARGE
  * - $(P)$(R)FlatTopChargeR
    - Measured pulse flat top charge.
    - ai
    - read
    - BCM.ACCT.FLATTOP_CHARGE
  * - $(P)$(R)PulseWidthR
    - Measured pulse width.
    - ai
    - read
    - BCM.ACCT.PULSE_WIDTH
  * - $(P)$(R)AdcScale, $(P)$(R)AdcScaleR
    - ADC scale factor.
    - ao, ai
    - write, read
    - BCM.ACCT.ADC_SCALE
  * - $(P)$(R)AdcOffset, $(P)$(R)AdcOffsetR
    - ADC offset value.
    - longout, longin
    - write, read
    - BCM.ACCT.ADC_OFFSET
  * - $(P)$(R)FlatTopStart, $(P)$(R)FlatTopStartR
    - Start of flat top region.
    - ao, ai
    - write, read
    - BCM.ACCT.FLATTOP_START
  * - $(P)$(R)FlatTopEnd, $(P)$(R)FlatTopEndR
    - End of flat top region.
    - ao, ai
    - write, read
    - BCM.ACCT.FLATTOP_END
  * - $(P)$(R)FlatTopCurrentR
    - Measured pulse flat top current.
    - ai
    - read
    - BCM.ACCT.FLATTOP_CURRENT
  * - $(P)$(R)FineDelay, $(P)$(R)FineDelayR
    - Trigger fine delay to align the pulse edges with the trigger edges for each ADC individually.
    - ao, ai
    - write, read
    - BCM.ACCT.FINE_DELAY
  * - $(P)$(R)DroopRate, $(P)$(R)DroopRateR
    - Droop compensation rate.
    - ao, ai
    - write, read
    - BCM.ACCT.DROOP_RATE
  * - $(P)$(R)DroopCompensating, $(P)$(R)DroopCompensatingR
    - Droop compensation control.
    - bo, bi
    - write, read
    - BCM.ACCT.DROOP_COMPENSATING
  * - $(P)$(R)NoiseFiltering, $(P)$(R)NoiseFilteringR
    - Noise filtering control.
    - bo, bi
    - write, read
    - BCM.ACCT.NOISE_FILTERING
  * - $(P)$(R)BaseliningBefore, $(P)$(R)BaseliningBeforeR
    - Baseline correction before applying droop compensation.
    - bo, bi
    - write, read
    - BCM.ACCT.BASELINING_BEFORE
  * - $(P)$(R)BaseliningAfter, $(P)$(R)BaseliningAfterR
    - Baseline correction after applying droop compensation.
    - bo, bi
    - write, read
    - BCM.ACCT.BASELINING_AFTER
  * - $(P)$(R)DcBlocking, $(P)$(R)DcBlockingR
    - DC blocking filter control.
    - bo, bi
    - write, read
    - BCM.ACCT.DC_BLOCKING
  * - $(P)$(R)UpperThreshold, $(P)$(R)UpperThresholdR
    - Upper current level threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.UPPER_THRESHOLD
  * - $(P)$(R)LowerThreshold, $(P)$(R)LowerThresholdR
    - Lower current level threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.LOWER_THRESHOLD
  * - $(P)$(R)ErrantThreshold, $(P)$(R)ErrantThresholdR
    - Errant current level threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.ERRANT_THRESHOLD
  * - $(P)$(R)MaxPulseLength, $(P)$(R)MaxPulseLengthR
    - Maximum pulse length.
    - longout, longin
    - write, read
    - BCM.ACCT.MAX_PULSE_LENGTH
  * - $(P)$(R)LowerWindowStart, $(P)$(R)LowerWindowStartR
    - The activation window start time for alarm on lower threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.LOWER_WINDOW_START
  * - $(P)$(R)LowerWindowEnd, $(P)$(R)LowerWindowEndR
    - The activation window end time for alarm on lower threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.LOWER_WINDOW_END
  * - $(P)$(R)ErrantWindowStart, $(P)$(R)ErrantWindowStartR
    - The activation window start time for alarm on errant threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.ERRANT_WINDOW_START
  * - $(P)$(R)ErrantWindowEnd, $(P)$(R)ErrantWindowEndR
    - The activation window end time for alarm on errant threshold.
    - ao, ai
    - write, read
    - BCM.ACCT.ERRANT_WINDOW_END

The table below lists ACCT alarm records.
See also :ref:`alarm_sets_and_types` for alarm type description.

.. cssclass:: table-bordered table-striped table-hover
.. list-table:: ACCT alarm record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)AlarmUpperControl, $(P)$(R)AlarmUpperControlR
    - Pulse amplitude too high alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.UPPER_CONTROL
  * - $(P)$(R)AlarmLowerControl, $(P)$(R)AlarmLowerControlR
    - Pulse amplitude too low alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.LOWER_CONTROL
  * - $(P)$(R)AlarmErrantControl, $(P)$(R)AlarmErrantControlR
    - Errant beam alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.ERRANT_CONTROL
  * - $(P)$(R)AlarmTriggerControl, $(P)$(R)AlarmTriggerControlR
    - Beam pulse longer than trigger pulse alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.TRIGGER_CONTROL
  * - $(P)$(R)AlarmLimitControl, $(P)$(R)AlarmLimitControlR
    - Beam pulse longer than limit parameter from timing system alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.LIMIT_CONTROL
  * - $(P)$(R)AlarmAdcOverflowControl, $(P)$(R)AlarmAdcOverflowControlR
    - ADC overflow alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.ADC_OVERFLOW_CONTROL
  * - $(P)$(R)AlarmAdcUnderflowControl, $(P)$(R)AlarmAdcUnderflowControlR
    - ADC underflow alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.ADC_UNDERFLOW_CONTROL
  * - $(P)$(R)AlarmAdcStuckControl, $(P)$(R)AlarmAdcStuckControlR
    - ADC stuck alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.ADC_STUCK_CONTROL
  * - $(P)$(R)AlarmAiuFaultControl, $(P)$(R)AlarmAiuFaultControlR
    - AIU power supply alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.AIU_FAULT_CONTROL
  * - $(P)$(R)AlarmChargeTooHighControl
    - Charge too high alarm control.
    - bo, bi
    - write, read
    - BCM.ACCT.ALARM.CHARGE_TOO_HIGH_CONTROL
  * - $(P)$(R)AlarmUpperHoldR
    - Latched alarm status reporting pulse amplitude is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.UPPER_HOLD
  * - $(P)$(R)AlarmLowerHoldR
    - Latched alarm status reporting for pulse amplitude is too low.
    - bi
    - read
    - BCM.ACCT.ALARM.LOWER_HOLD
  * - $(P)$(R)AlarmErrantHoldR
    - Latched alarm status reporting errant beam condition.
    - bi
    - read
    - BCM.ACCT.ALARM.ERRANT_HOLD
  * - $(P)$(R)AlarmTriggerHoldR
    - Latched alarm status reporting beam pulse is longer than trigger pulse.
    - bi
    - read
    - BCM.ACCT.ALARM.TRIGGER_HOLD
  * - $(P)$(R)AlarmLimitHoldR
    - Latched alarm status reporting beam pulse is longer than limit parameter from timing system.
    - bi
    - read
    - BCM.ACCT.ALARM.LIMIT_HOLD
  * - $(P)$(R)AlarmAdcOverflowHoldR
    - Latched alarm status reporting ADC overflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_OVERFLOW_HOLD
  * - $(P)$(R)AlarmAdcUnderflowHoldR
    - Latched alarm status reporting ADC underflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_UNDERFLOW_HOLD
  * - $(P)$(R)AlarmAdcStuckHoldR
    - Latched alarm status reporting ADC stuck is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_STUCK_HOLD
  * - $(P)$(R)AlarmAiuFaultHoldR
    - Latched alarm status reporting AIU power supply fault or cable disconnected.
    - bi
    - read
    - BCM.ACCT.ALARM.AIU_FAULT_HOLD
  * - $(P)$(R)AlarmChargeTooHighHoldR
    - Latched alarm status reporting charge is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.CHARGE_TOO_HIGH_HOLD
  * - $(P)$(R)AlarmLUTUpperThresholdHoldR
    - Latched alarm status reporting LUT upper threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_HOLD
  * - $(P)$(R)AlarmLUTLowerThresholdHoldR
    - Latched alarm status reporting LUT lower threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_HOLD
  * - $(P)$(R)AlarmLUTPulseLengthHoldR
    - Latched alarm status reporting LUT max pulse length threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_PULSE_LENGTH_HOLD
  * - $(P)$(R)AlarmLUTDestinationModeHoldR
    - Latched alarm status reporting LUT beam destination ID not found.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_DESTINATION_MODE_HOLD
  * - $(P)$(R)AlarmUpperFirstR
    - First alarm status reporting pulse amplitude is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.UPPER_FIRST
  * - $(P)$(R)AlarmLowerFirstR
    - First alarm status reporting for pulse amplitude is too low.
    - bi
    - read
    - BCM.ACCT.ALARM.LOWER_FIRST
  * - $(P)$(R)AlarmErrantFirstR
    - First alarm status reporting errant beam condition.
    - bi
    - read
    - BCM.ACCT.ALARM.ERRANT_FIRST
  * - $(P)$(R)AlarmTriggerFirstR
    - First alarm status reporting beam pulse is longer than trigger pulse.
    - bi
    - read
    - BCM.ACCT.ALARM.TRIGGER_FIRST
  * - $(P)$(R)AlarmLimitFirstR
    - First alarm status reporting beam pulse is longer than limit parameter from timing system.
    - bi
    - read
    - BCM.ACCT.ALARM.LIMIT_FIRST
  * - $(P)$(R)AlarmAdcOverflowFirstR
    - First alarm status reporting ADC overflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_OVERFLOW_FIRST
  * - $(P)$(R)AlarmAdcUnderflowFirstR
    - First alarm status reporting ADC underflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_UNDERFLOW_FIRST
  * - $(P)$(R)AlarmAdcStuckFirstR
    - First alarm status reporting ADC stuck is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_STUCK_FIRST
  * - $(P)$(R)AlarmAiuFaultFirstR
    - First alarm status reporting AIU power supply fault or cable disconnected.
    - bi
    - read
    - BCM.ACCT.ALARM.AIU_FAULT_FIRST
  * - $(P)$(R)AlarmChargeTooHighFirstR
    - First alarm status reporting charge is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.CHARGE_TOO_HIGH_FIRST
  * - $(P)$(R)AlarmLUTUpperThresholdFirstR
    - First alarm status reporting LUT upper threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_FIRST
  * - $(P)$(R)AlarmLUTLowerThresholdFirstR
    - First alarm status reporting LUT lower threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_FIRST
  * - $(P)$(R)AlarmLUTPulseLengthFirstR
    - First alarm status reporting LUT max pulse length threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_PULSE_LENGTH_FIRST
  * - $(P)$(R)AlarmLUTDestinationModeFirstR
    - First alarm status reporting LUT beam destination ID not found.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_DESTINATION_MODE_FIRST
  * - $(P)$(R)AlarmUpperDirectR
    - Immediate alarm status reporting pulse amplitude is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.UPPER_DIRECT
  * - $(P)$(R)AlarmLowerDirectR
    - Immediate alarm status reporting for pulse amplitude is too low.
    - bi
    - read
    - BCM.ACCT.ALARM.LOWER_DIRECT
  * - $(P)$(R)AlarmErrantDirectR
    - Immediate alarm status reporting errant beam condition.
    - bi
    - read
    - BCM.ACCT.ALARM.ERRANT_DIRECT
  * - $(P)$(R)AlarmTriggerDirectR
    - Immediate alarm status reporting beam pulse is longer than trigger pulse.
    - bi
    - read
    - BCM.ACCT.ALARM.TRIGGER_DIRECT
  * - $(P)$(R)AlarmLimitDirectR
    - Immediate alarm status reporting beam pulse is longer than limit parameter from timing system.
    - bi
    - read
    - BCM.ACCT.ALARM.LIMIT_DIRECT
  * - $(P)$(R)AlarmAdcOverflowDirectR
    - Immediate alarm status reporting ADC overflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_OVERFLOW_DIRECT
  * - $(P)$(R)AlarmAdcUnderflowDirectR
    - Immediate alarm status reporting ADC underflow is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_UNDERFLOW_DIRECT
  * - $(P)$(R)AlarmAdcStuckDirectR
    - Immediate alarm status reporting ADC stuck is detected.
    - bi
    - read
    - BCM.ACCT.ALARM.ADC_STUCK_DIRECT
  * - $(P)$(R)AlarmAiuFaultDirectR
    - Immediate alarm status reporting AIU power supply fault or cable disconnected.
    - bi
    - read
    - BCM.ACCT.ALARM.AIU_FAULT_DIRECT
  * - $(P)$(R)AlarmChargeTooHighDirectR
    - Immediate alarm status reporting charge is too high.
    - bi
    - read
    - BCM.ACCT.ALARM.CHARGE_TOO_HIGH_DIRECT
  * - $(P)$(R)AlarmLUTUpperThresholdDirectR
    - Immediate alarm status reporting LUT upper threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_UPPER_THRESHOLD_DIRECT
  * - $(P)$(R)AlarmLUTLowerThresholdDirectR
    - Immediate alarm status reporting LUT lower threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_LOWER_THRESHOLD_DIRECT
  * - $(P)$(R)AlarmLUTPulseLengthDirectR
    - Immediate alarm status reporting LUT max pulse length threshold value is not consistent with beam mode ID.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_PULSE_LENGTH_DIRECT
  * - $(P)$(R)AlarmLUTDestinationModeDirectR
    - Immediate alarm status reporting LUT beam destination ID not found.
    - bi
    - read
    - BCM.ACCT.ALARM.LUT_DESTINATION_MODE_DIRECT
