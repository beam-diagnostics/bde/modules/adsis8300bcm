
.. cssclass:: table-bordered table-striped table-hover
.. flat-table::
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Name
    - Differential channel name.
    - stringout
    -
    -
  * - $(P)$(R)SourceA, $(P)$(R)SourceAR
    - Differential channel A source selection.

      * ACCT 1
      * ACCT 2
      * ACCT 3
      * ACCT 4
      * ACCT 5
      * ACCT 6
      * ACCT 7
      * ACCT 8
      * ACCT 9
      * ACCT 10
      * Fiber 1
      * Fiber 2

    - mbbo, mbbi
    - write, read
    - BCM.DIFF.SOURCE_A
  * - $(P)$(R)SourceB, $(P)$(R)SourceBR
    - Differential channel B source selection.

      * ACCT 1
      * ACCT 2
      * ACCT 3
      * ACCT 4
      * ACCT 5
      * ACCT 6
      * ACCT 7
      * ACCT 8
      * ACCT 9
      * ACCT 10
      * Fiber 1
      * Fiber 2

    - mbbo, mbbi
    - write, read
    - BCM.DIFF.SOURCE_B
  * - $(P)$(R)Delay, $(P)$(R)DelayR
    - Parameter to add a delay in one of the channels of a differential pair to compensate beam arrival time differences and different cable lengths.
    - ao, ai
    - write, read
    - BCM.DIFF.DELAY
  * - $(P)$(R)FastThreshold, $(P)$(R)FastThresholdR
    - Threshold for fast transmission alarm.
    - ao, ai
    - write, read
    - BCM.DIFF.FAST_THRESHOLD
  * - $(P)$(R)FastMinimumR
    - Minimum value of the fast differential detection.
    - ai
    - read
    - BCM.DIFF.FAST_MINIMUM
  * - $(P)$(R)FastMaximumR
    - Maximum value of the differential detection.
    - ai
    - read
    - BCM.DIFF.FAST_MAXIMUM
  * - $(P)$(R)MediumThreshold, $(P)$(R)MediumThresholdR
    - Threshold for medium transmission alarm.
    - ao, ai
    - write, read
    - BCM.DIFF.MEDIUM_THRESHOLD
  * - $(P)$(R)MediumMinimumR
    - Minimum value of the medium differential detection.
    - ai
    - read
    - BCM.DIFF.MEDIUM_MINIMUM
  * - $(P)$(R)MediumMaximumR
    - Maximum value of the medium differential detection.
    - ai
    - read
    - BCM.DIFF.MEDIUM_MAXIMUM
  * - $(P)$(R)SlowThreshold, $(P)$(R)SlowThresholdR
    - Threshold for slow transmission alarm.
    - ao, ai
    - write, read
    - BCM.DIFF.SLOW_THRESHOLD
  * - $(P)$(R)SlowMinimumR
    - Minimum value of the slow differential detection.
    - ai
    - read
    - BCM.DIFF.SLOW_MINIMUM
  * - $(P)$(R)SlowMaximumR
    - Maximum value of the slow differential detection.
    - ai
    - read
    - BCM.DIFF.SLOW_MAXIMUM
  * - $(P)$(R)RisingWindowStart, $(P)$(R)RisingWindowStartR
    - The activation window start time for alarm on signal rise time.
    - ao, ai
    - write, read
    - BCM.DIFF.RISING_WINDOW_START
  * - $(P)$(R)RisingWindowEnd, $(P)$(R)RisingWindowEndR
    - The activation window end time for alarm on signal rise time.
    - ao, ai
    - write, read
    - BCM.DIFF.RISING_WINDOW_END
  * - $(P)$(R)FallingWindowStart, $(P)$(R)FallingWindowStartR
    - The activation window start time for alarm on signal fall time.
    - ao, ai
    - write, read
    - BCM.DIFF.FALLING_WINDOW_START
  * - $(P)$(R)FallingWindowEnd, $(P)$(R)FallingWindowEndR
    - The activation window end time for alarm on signal fall time.
    - ao, ai
    - write, read
    - BCM.DIFF.FALLING_WINDOW_END
  * - $(P)$(R)WsBetween, $(P)$(R)WsBetweenR
    - Wire scan exists between pair.
    - bo, bi
    - write, read
    - BCM.DIFF.WS_BETWEEN
  * - $(P)$(R)EmuBetween, $(P)$(R)EmuBetweenR
    - EMU exists between pair.
    - bo, bi
    - write, read
    - BCM.DIFF.EMU_BETWEEN
  * - $(P)$(R)RfqBetween, $(P)$(R)RfqBetweenR
    - RFQ exists between pair.
    - bo, bi
    - write, read
    - BCM.DIFF.RFQ_BETWEEN
  * - $(P)$(R)FastWindowWidth, $(P)$(R)FastWindowWidthR
    - Length of moving average filter for fast transmission.
    - ao, ai
    - write, read
    - BCM.DIFF.FAST_WINDOW_WIDTH
  * - $(P)$(R)MediumWindowWidth, $(P)$(R)MediumWindowWidthR
    - Length of moving average filter for medium transmission.
    - ao, ai
    - write, read
    - BCM.DIFF.MEDIUM_WINDOW_WIDTH
  * - $(P)$(R)FastWindowWidthInverseR
    - Inverse value of *$(P)$(R)FastWindowWidthR*.
    - ai
    - read
    - BCM.DIFF.FAST_WINDOW_WIDTH_INVERSE
  * - $(P)$(R)MediumWindowWidthInverseR
    - Inverse value of *$(P)$(R)MediumWindowWidthR*.
    - ai
    - read
    - BCM.DIFF.MEDIUM_WINDOW_WIDTH_INVERSE

The table below lists differential alarm records.
See also :ref:`alarm_sets_and_types` for alarm type description.

.. cssclass:: table-bordered table-striped table-hover
.. list-table:: Differential alarm record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)AlarmFastControl, $(P)$(R)AlarmFastControlR
    - Fast transmission alarm control.
    - bo, bi
    - write, read
    - BCM.DIFF.ALARM.FAST_CONTROL
  * - $(P)$(R)AlarmFastHoldR
    - Latched alarm status reporting fast transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.FAST_HOLD
  * - $(P)$(R)AlarmFastFirstR
    - First alarm status reporting fast transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.FAST_FIRST
  * - $(P)$(R)AlarmFastDirectR
    - Immediate alarm status reporting fast transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.FAST_DIRECT
  * - $(P)$(R)AlarmMediumControl, $(P)$(R)AlarmMediumControlR
    - Medium transmission alarm control.
    - bo, bi
    - write, read
    - BCM.DIFF.ALARM.MEDIUM_CONTROL
  * - $(P)$(R)AlarmMediumHoldR
    - Latched alarm status reporting medium transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.MEDIUM_HOLD
  * - $(P)$(R)AlarmMediumFirstR
    - First alarm status reporting medium transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.MEDIUM_FIRST
  * - $(P)$(R)AlarmMediumDirectR
    - Immediate alarm status reporting medium transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.MEDIUM_DIRECT
  * - $(P)$(R)AlarmSlowControl, $(P)$(R)AlarmSlowControlR
    - Slow transmission alarm control.
    - bo, bi
    - write, read
    - BCM.DIFF.ALARM.SLOW_CONTROL
  * - $(P)$(R)AlarmSlowHoldR
    - Latched alarm status reporting slow transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.SLOW_HOLD
  * - $(P)$(R)AlarmSlowFirstR
    - First alarm status reporting slow transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.SLOW_FIRST
  * - $(P)$(R)AlarmSlowDirectR
    - Immediate alarm status reporting slow transmission alarm.
    - bi
    - read
    - BCM.DIFF.ALARM.SLOW_DIRECT
