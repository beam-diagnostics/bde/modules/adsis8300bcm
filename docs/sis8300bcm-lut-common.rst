
Common LUT records define beam mode and beam destination name and ID.
Name records are meant for users, and are not used by the firmware.
ID record values are used by the firmware as part of the LUT row definition.
See also :doc:`sis8300bcm-lut-acct.rst`.

.. cssclass:: table-bordered table-striped table-hover
.. list-table:: LUT common record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Lut$(N)ModeName
    - Descriptive beam mode name.
    - stringout
    -
    -
  * - $(P)$(R)Lut$(N)ModeID, $(P)$(R)Lut$(N)ModeIDR
    - Beam mode ID.
    - longout, longin
    - write, read
    - BCM.LUT$(N).MODE_ID
  * - $(P)$(R)Lut$(N)DestinationName
    - Descriptive beam destination name.
    - stringout
    -
    -
  * - $(P)$(R)Lut$(N)DestinationID, $(P)$(R)Lut$(N)DestinationIDR
    - Beam destination ID.
    - longout, longin
    - write, read
    - BCM.LUT$(N).DEST_ID
