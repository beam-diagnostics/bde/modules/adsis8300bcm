
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: BCM record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)AcquisitionTriggerSource, $(P)$(R)AcquisitionTriggerSourceR
    - Acquisition trigger source selection:

      * Software
      * Internal
      * FrontPanel
      * BackPlane1
      * BackPlane2
      * BackPlane3
      * BackPlane4

      This trigger is used to start data acquisition.
      See also :ref:`trigger_types`.

    - mbbo, mbbi
    - write, read
    - BCM.ACQUISITION_TRIGGER_SOURCE
  * - $(P)$(R)BeamTriggerSource, $(P)$(R)BeamTriggerSourceR
    - Beam trigger source selection:

      * Software
      * Internal
      * FrontPanel
      * BackPlane1
      * BackPlane2
      * BackPlane3
      * BackPlane4

      This trigger is used to define beam envelope for protection function.
      See also :ref:`trigger_types`.

    - mbbo, mbbi
    - write, read
    - BCM.BEAM_TRIGGER_SOURCE
  * - $(P)$(R)BCMFWVersionR
    - Firmware version.
    - longin
    - read
    - BCM.FW_VERSION
  * - $(P)$(R)BCMFWGitHashR
    - Firmware GIT hash string.
    - longin
    - read
    - BCM.FW_GIT_HASH
  * - $(P)$(R)ClockFrequencyMeasR
    - Measured clock frequency. Measurement is done in the firmware.
    - ai
    - read
    - BCM.CLOCK_FREQUENCY_MEAS
  * - $(P)$(R)TriggerPeriodMeasR
    - Measured trigger period. Measurement is done in the firmware.
    - ai
    - read
    - BCM.TRIGGER_PERIOD_MEAS
  * - $(P)$(R)TriggerWidthMeasR
    - Measured trigger width. Measurement is done in the firmware.
    - ai
    - read
    - BCM.TRIGGER_WIDTH_MEAS
  * - $(P)$(R)MinTriggerPeriod, $(P)$(R)MinTriggerPeriodR
    - Desired minimum trigger period.
    - longout, longin
    - write, read
    - BCM.MIN_TRIGGER_PERIOD
  * - $(P)$(R)MaxPulseWidth, $(P)$(R)MaxPulseWidthR
    - Desired maximum trigger width.
    - longout, longin
    - write, read
    - BCM.MAX_PULSE_WIDTH
  * - $(P)$(R)MainClockMissingR
    - Clock missing indicator. Detection is done in firmware.
    - bi
    - read
    - BCM.MAIN_CLOCK_MISSING
  * - $(P)$(R)AuxiliaryClockMissingR
    - Auxiliary clock missing indicator. Detection is done in firmware.
    - bi
    - read
    - BCM.AUXILIARY_CLOCK_MISSING
  * - $(P)$(R)Ready, $(P)$(R)ReadyR
    - System ready for use.
    - bo, bi
    - write, read
    - BCM.READY
  * - $(P)$(R)BeamMode, $(P)$(R)BeamModeR
    - User specified beam mode when not using look-up table.
    - longout, longin
    - write, read
    - BCM.BEAM_MODE
  * - $(P)$(R)BeamDestination, $(P)$(R)BeamDestinationR
    - User specified beam destination when not using look-up table.
    - longout, longin
    - write, read
    - BCM.BEAM_DESTINATION
  * - $(P)$(R)LutControl
    - Look-up table control enable / disable.
    - bo, bi
    - write, read
    - BCM.LUT_CONTROL
  * - $(P)$(R)ReadyFPGAR
    - FPGA ready indicator.
    - bi
    - read
    - BCM.READY_FPGA
  * - $(P)$(R)BeamPermitFPGAR
    - FPGA beam permit indicator.
    - bi
    - read
    - BCM.BEAM_PERMIT_FPGA

The table below lists common alarm records.
See also :ref:`alarm_sets_and_types` for alarm type description.

.. cssclass:: table-bordered table-striped table-hover
.. list-table:: BCM alarm record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)AlarmsClear
    - Clear all hold and first alarms.
    - bo
    - write
    - BCM.ALARMS_CLEAR
  * - $(P)$(R)AlarmsControl, $(P)$(R)AlarmsControlR
    - Alarms enable / disable control.
    - bo, bi
    - write, read
    - BCM.ALARMS_CONTROL
  * - $(P)$(R)AlarmAuxClockControl, $(P)$(R)AlarmAuxClockControlR
    - Auxiliary clock alarm reporting control.
    - bo, bi
    - write, read
    - BCM.ALARM.AUXILIARY_CLOCK_CONTROL
  * - $(P)$(R)AlarmProcClockControl, $(P)$(R)AlarmProcClockControlR
    - Processing clock alarm reporting control.
    - bo, bi
    - write, read
    - BCM.ALARM.PROCESSING_CLOCK_CONTROL
  * - $(P)$(R)AlarmTriggerTooWideControl, $(P)$(R)AlarmTriggerTooWideControlR
    - Trigger too wide alarm reporting control.
    - bo, bi
    - write, read
    - BCM.ALARM.TRIGGER_TOO_WIDE_CONTROL
  * - $(P)$(R)AlarmTriggerTooNarrowControl, $(P)$(R)AlarmTriggerTooNarrowControlR
    - Trigger too narrow alarm reporting control.
    - bo, bi
    - write, read
    - BCM.ALARM.TRIGGER_TOO_NARROW_CONTROL
  * - $(P)$(R)AlarmTriggerPeriodTooShortControl, $(P)$(R)AlarmTriggerPeriodTooShortControlR
    - Trigger period too short alarm reporting control.
    - bo, bi
    - write, read
    - BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_CONTROL
  * - $(P)$(R)AlarmAuxClockHoldR
    - Latched auxiliary clock alarm status.
    - bi
    - read
    - BCM.ALARM.AUXILIARY_CLOCK_HOLD
  * - $(P)$(R)AlarmProcClockHoldR
    - Latched processing clock alarm status.
    - bi
    - read
    - BCM.ALARM.PROCESSING_CLOCK_HOLD
  * - $(P)$(R)AlarmTriggerTooWideHoldR
    - Latched trigger too wide alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_WIDE_HOLD
  * - $(P)$(R)AlarmTriggerTooNarrowHoldR
    - Latched trigger too narrow alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_NARROW_HOLD
  * - $(P)$(R)AlarmTriggerPeriodTooShortHoldR
    - Latched trigger period too short alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_HOLD
  * - $(P)$(R)AlarmAuxClockFirstR
    - First auxiliary clock alarm status.
    - bi
    - read
    - BCM.ALARM.AUXILIARY_CLOCK_FIRST
  * - $(P)$(R)AlarmProcClockFirstR
    - First processing clock alarm status.
    - bi
    - read
    - BCM.ALARM.PROCESSING_CLOCK_FIRST
  * - $(P)$(R)AlarmTriggerTooWideFirstR
    - First trigger too wide alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_WIDE_FIRST
  * - $(P)$(R)AlarmTriggerTooNarrowFirstR
    - First trigger too narrow alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_NARROW_FIRST
  * - $(P)$(R)AlarmTriggerPeriodTooShortFirstR
    - First trigger period too short alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_FIRST
  * - $(P)$(R)AlarmAuxClockDirectR
    - Direct auxiliary clock alarm status.
    - bi
    - read
    - BCM.ALARM.AUXILIARY_CLOCK_DIRECT
  * - $(P)$(R)AlarmProcClockDirectR
    - Direct processing clock alarm status.
    - bi
    - read
    - BCM.ALARM.PROCESSING_CLOCK_DIRECT
  * - $(P)$(R)AlarmTriggerTooWideDirectR
    - Direct trigger too wide alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_WIDE_DIRECT
  * - $(P)$(R)AlarmTriggerTooNarrowDirectR
    - Direct trigger too narrow alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_TOO_NARROW_DIRECT
  * - $(P)$(R)AlarmTriggerPeriodTooShortDirectR
    - Direct trigger period too short alarm status.
    - bi
    - read
    - BCM.ALARM.TRIGGER_PERIOD_TOO_SHORT_DIRECT
