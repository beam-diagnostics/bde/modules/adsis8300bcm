/**
 * Struck 8300 BCM Linux userspace library.
 * Copyright (C) 2016-2019  ESS ERIC
 * Copyright (C) 2014-2015  Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300drvbcm.c
 * @brief Implementation of sis8300 BCM userspace API.
 * @author Joao Paulo Martins, Hinko Kocevar
 *
 * Based on ADSIS8300 and ADSIS8300BPM projects
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <libudev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <math.h>

#include <epicsTime.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>
#include <sis8300_reg.h>

#include <sis8300drvbcm.h>
#include <sis8300bcm_reg.h>

const sis8300drv_Qmn sis8300drvbcm_Qmn_acq_conversion =
        { .int_bits_m = 17, .frac_bits_n = 15, .is_signed = 1 };
const sis8300drv_Qmn sis8300drvbcm_Qmn_adc_scale =
        { .int_bits_m = 1, .frac_bits_n = 15, .is_signed = 0 };
const sis8300drv_Qmn sis8300drvbcm_Qmn_diff_fast_window_width =
        { .int_bits_m = 1, .frac_bits_n = 15, .is_signed = 1 };
const sis8300drv_Qmn sis8300drvbcm_Qmn_diff_medium_window_width =
        { .int_bits_m = 1, .frac_bits_n = 23, .is_signed = 1 };
const sis8300drv_Qmn sis8300drvbcm_Qmn_leaky_coefficient =
        { .int_bits_m = 9, .frac_bits_n = 23, .is_signed = 1 };


// default sampling frequency
// NOTES: * should use firmware register for storing this parameter
//        * see register 0x409 which is currently in kHz (requested Hz from FW developers)
static double s_sampling_frequency = 88052500.0;

void sis8300drvbcm_set_sampling_frequency(sis8300drv_usr *sisuser, double frequency)
{
    s_sampling_frequency = frequency;
}

void sis8300drvbcm_get_sampling_frequency(sis8300drv_usr *sisuser, double *frequency)
{
    *frequency = s_sampling_frequency;
}

// used to control trigger selection for beam start
int sis8300drvbcm_set_beam_trigger_source(sis8300drv_usr *sisuser, unsigned int source)
{
    // register takes 4-bit unsigned value
    unsigned int value = source & 0xF;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_BEAM_TRIGGER_SOURCE_REG, value);
    return ret;
}

int sis8300drvbcm_get_beam_trigger_source(sis8300drv_usr *sisuser, unsigned int *source)
{
    // register provides 4-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_BEAM_TRIGGER_SOURCE_REG, source);
    if (ret) {
        return ret;
    }
    *source &= 0xF;
    return ret;
}

// used to control trigger selection for data acquisition
int sis8300drvbcm_set_acquisition_trigger_source(sis8300drv_usr *sisuser, unsigned int source)
{
    // register takes 4-bit unsigned value
    unsigned int value = source & 0xF;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQUITISION_TRIGGER_SOURCE_REG, value);
    return ret;
}

int sis8300drvbcm_get_acquisition_trigger_source(sis8300drv_usr *sisuser, unsigned int *source)
{
    // register provides 4-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQUITISION_TRIGGER_SOURCE_REG, source);
    if (ret) {
        return ret;
    }
    *source &= 0xF;
    return ret;
}

// used to control crate ID 
int sis8300drvbcm_set_crate_id(sis8300drv_usr *sisuser, unsigned int crate_id)
{
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_CRATE_ID_REG, crate_id);
    return ret;
}

int sis8300drvbcm_get_crate_id(sis8300drv_usr *sisuser, unsigned int *crate_id)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_CRATE_ID_REG, crate_id);
    return ret;
}

// used to control pulse width filter 
int sis8300drvbcm_set_pulse_width_filter(sis8300drv_usr *sisuser, double width)
{
    // register takes 32-bit unsigned value
    unsigned int value = (unsigned int)(width / 1000000.0 * s_sampling_frequency);
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_PULSE_WIDTH_FILTER_REG, value);
    return ret;
}

int sis8300drvbcm_get_pulse_width_filter(sis8300drv_usr *sisuser, double *width)
{
    // register provides 32-bit unsigned value
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_PULSE_WIDTH_FILTER_REG, &value);
    if (ret) {
        return ret;
    }
    *width = (double)value * 1000000.0 / s_sampling_frequency;
    return ret;
}

// used to control trigger selection for beam start for individual ACCTs
int sis8300drvbcm_set_channel_trigger_source(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_TRIGGER_SOURCE_OFF;
    // register takes 4-bit unsigned value
    unsigned int value = source & 0xF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_trigger_source(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_TRIGGER_SOURCE_OFF;
    // register provides 4-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, source);
    if (ret) {
        return ret;
    }
    *source &= 0xF;
    return ret;
}

int sis8300drvbcm_set_channel_number_of_samples(sis8300drv_usr *sisuser, unsigned int channel, unsigned int count)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register takes 32-bit unsigned value
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_NUM_SAMPLES_REG, count);
    return ret;
}

int sis8300drvbcm_get_channel_number_of_samples(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *count)
{
    // select channel to get parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register provides 32-bit unsigned value
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_NUM_SAMPLES_REG, count);
    return ret;
}

int sis8300drvbcm_set_channel_memory_address(sis8300drv_usr *sisuser, unsigned int channel, unsigned int address)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register takes 32-bit unsigned value
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_MEM_ADDR_REG, address);
    return ret;
}

int sis8300drvbcm_get_channel_memory_address(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *address)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register provides 32-bit unsigned value
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_MEM_ADDR_REG, address);
    return ret;
}

int sis8300drvbcm_set_channel_sample_fraction_bits(sis8300drv_usr *sisuser, unsigned int channel, unsigned int count)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register takes 6-bit unsigned value
    count &= 0x3F;
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_FRAC_BITS_REG, count);
    return ret;
}

int sis8300drvbcm_get_channel_sample_fraction_bits(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *count)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_FRAC_BITS_REG, count);
    // register provides 6-bit unsigned value
    *count &= 0x3F;
    return ret;
}

int sis8300drvbcm_set_channel_conversion_factor(sis8300drv_usr *sisuser, unsigned int channel, double factor)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    double error;
    unsigned int value;
    ret = sis8300drv_double_2_Qmn(factor, sis8300drvbcm_Qmn_acq_conversion, &value, &error);
    if (ret) {
        return ret;
    }
    // register takes Q17.15 value
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CONV_FACTOR_REG, value);
    return ret;
}

int sis8300drvbcm_get_channel_conversion_factor(sis8300drv_usr *sisuser, unsigned int channel, double *factor)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register provides Q17.15 value
    unsigned int value;
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CONV_FACTOR_REG, &value);
    if (ret) {
        return ret;
    }
    ret = sis8300drv_Qmn_2_double(value, sis8300drvbcm_Qmn_acq_conversion, factor);
    return ret;
}

int sis8300drvbcm_set_channel_conversion_offset(sis8300drv_usr *sisuser, unsigned int channel, double offset)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    double error;
    unsigned int value;
    ret = sis8300drv_double_2_Qmn(offset, sis8300drvbcm_Qmn_acq_conversion, &value, &error);
    if (ret) {
        return ret;
    }
    // register takes Q17.15 value
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CONV_OFFSET_REG, value);
    return ret;
}

int sis8300drvbcm_get_channel_conversion_offset(sis8300drv_usr *sisuser, unsigned int channel, double *offset)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register provides Q17.15 value
    unsigned int value;
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CONV_OFFSET_REG, &value);
    if (ret) {
        return ret;
    }
    ret = sis8300drv_Qmn_2_double(value, sis8300drvbcm_Qmn_acq_conversion, offset);
    return ret;
}

int sis8300drvbcm_set_channel_decimation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int decimation)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register takes 16-bit unsigned value
    decimation &= 0xFFFF;
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CONV_DECIM_REG, decimation);
    return ret;
}

int sis8300drvbcm_get_channel_decimation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *decimation)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    // register provides 16-bit unsigned value
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CONV_DECIM_REG, decimation);
    *decimation &= 0xFFFF;
    return ret;
}

int sis8300drvbcm_set_channel_recording(sis8300drv_usr *sisuser, unsigned int channel, unsigned int recording)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    value &= 0x7;
    // recording is controlled by bit 0
    if (recording) {
        value |= (1 << 0);
    } else {
        value &= ~(1 << 0);
    }
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_recording(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *recording)
{
    // select channel to get parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    // recording status is in bit 0
    *recording = (value & (1 << 0)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_scaling(sis8300drv_usr *sisuser, unsigned int channel, unsigned int scaling)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    value &= 0x7;
    // scaling is controlled by bit 1
    if (scaling) {
        value |= (1 << 1);
    } else {
        value &= ~(1 << 1);
    }
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_scaling(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *scaling)
{
    // select channel to get parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    // scaling status is in bit 1
    *scaling = (value & (1 << 1)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_data_type_converting(sis8300drv_usr *sisuser, unsigned int channel, unsigned int converting)
{
    // select channel to update parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    value &= 0x7;
    // converting is controlled by bit 2
    if (converting) {
        value |= (1 << 2);
    } else {
        value &= ~(1 << 2);
    }
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_data_type_converting(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *converting)
{
    // select channel to get parameters for
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ACQ_CFG_CH_NUM_REG, channel);
    if (ret) {
        return ret;
    }
    unsigned int value;
    // register has lowest 3 bits defined
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ACQ_CFG_CH_CTRL_REG, &value);
    if (ret) {
        return ret;
    }
    // converting status is in bit 2
    *converting = (value & (1 << 2)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_adc_scale(sis8300drv_usr *sisuser, unsigned int channel, double factor)
{
    double error;
    unsigned int value;
    int ret = sis8300drv_double_2_Qmn(factor, sis8300drvbcm_Qmn_adc_scale, &value, &error);
    if (ret) {
        return ret;
    }
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_SCALE_OFF;
    // register takes Q1.15 value
    value &= 0xFFFF;
    ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_adc_scale(sis8300drv_usr *sisuser, unsigned int channel, double *factor)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_SCALE_OFF;
    unsigned int value;
    // register provides Q1.15 value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    ret = sis8300drv_Qmn_2_double(value, sis8300drvbcm_Qmn_adc_scale, factor);
    return ret;
}

int sis8300drvbcm_set_channel_adc_offset(sis8300drv_usr *sisuser, unsigned int channel, int offset)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_OFFSET_OFF;
    // register takes 16-bit signed value
    unsigned int value = (unsigned int)(offset & 0xFFFF);
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_adc_offset(sis8300drv_usr *sisuser, unsigned int channel, int *offset)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_OFFSET_OFF;
    unsigned int value;
    // register provides 16-bit signed value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *offset = (int)(value & 0xFFFF);
    return ret;
}

int sis8300drvbcm_set_channel_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_RISING_OFF;
    // register takes 32-bit unsigned value
    unsigned int value = (unsigned int)(start / 1000.0 * s_sampling_frequency);
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_RISING_OFF;
    // register provides 32-bit unsigned value
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *start = (double)value * 1000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_channel_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_FALLING_OFF;
    // register takes 32-bit unsigned value
    unsigned int value = (unsigned int)(end / 1000.0 * s_sampling_frequency);
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_FALLING_OFF;
    // register provides 32-bit unsigned value
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *end = (double)value * 1000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_channel_fine_delay(sis8300drv_usr *sisuser, unsigned int channel, double delay)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FINE_DELAY_OFF;
    // register takes 11-bit value
    int value = (int)(delay / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_fine_delay(sis8300drv_usr *sisuser, unsigned int channel, double *delay)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FINE_DELAY_OFF;
    int value;
    // register provides 11-bit value
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *delay = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_get_channel_measured_pulse_width(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_PULSE_WIDTH_OFF;
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // register holds 32-bit unsigned value in 'sampling periods',
    // function returns double value in miliseconds
    *width = 1.0 / s_sampling_frequency * (double)value * 1000.0;
    return ret;
}

int sis8300drvbcm_get_channel_measured_pulse_charge(sis8300drv_usr *sisuser, unsigned int channel, double *charge)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_PULSE_CHARGE_LOW_OFF;
    unsigned int value_low;
    int ret = sis8300drv_reg_read(sisuser, reg, &value_low);
    if (ret) {
        return ret;
    }
    reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_PULSE_CHARGE_HIGH_OFF;
    unsigned int value_high;
    ret = sis8300drv_reg_read(sisuser, reg, &value_high);
    if (ret) {
        return ret;
    }
    // combined low & high registers hold 64-bit signed value in '???',
    // function returns double value in microcoulombs
    *charge = (double)((long)value_high << 32 | value_low) * (0.1 / (s_sampling_frequency * 32768.0)) * 1000000.0;
    return ret;
}

int sis8300drvbcm_get_channel_measured_flattop_charge(sis8300drv_usr *sisuser, unsigned int channel, double *charge)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_CHARGE_LOW_OFF;
    unsigned int value_low;
    int ret = sis8300drv_reg_read(sisuser, reg, &value_low);
    if (ret) {
        return ret;
    }
    reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_FLATTOP_CHARGE_HIGH_OFF;
    unsigned int value_high;
    ret = sis8300drv_reg_read(sisuser, reg, &value_high);
    if (ret) {
        return ret;
    }
    // combined low & high registers hold 64-bit signed value in '???',
    // function returns double value in microcoulombs
    *charge = (double)((long)value_high << 32 | value_low) * (0.1 / (s_sampling_frequency * 32768.0)) * 1000000.0;
    return ret;
}

int sis8300drvbcm_get_channel_flattop_current(sis8300drv_usr *sisuser, unsigned int channel, double window, double charge, double *current)
{
    if (window != 0.0) {
        if (charge == 0.0) {
            *current = 0.0;
        } else {
            // charge is in microcoulombs, time window in miliseconds
            // function returns current in miliamperes
            *current = (charge / 1000000.0) / (window / 1000.0) * 1000.0;
        }
    }
    return 0;
}

int sis8300drvbcm_get_firmware_version(sis8300drv_usr *sisuser, unsigned int *version)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_FW_VER_REG, version);
    return ret;
}

int sis8300drvbcm_get_firmware_git_hash(sis8300drv_usr *sisuser, unsigned int *git_hash)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_FW_GIT_HASH_REG, git_hash);
    return ret;
}

int sis8300drvbcm_get_measured_sampling_frequency(sis8300drv_usr *sisuser, double *frequency)
{
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_CLOCK_FREQUENCY_MEAS_REG, &value);
    if (ret) {
        return ret;
    }
    // register holds 20-bit unsigned value in kHz,
    // function returns double value in Hz
    value &= 0xFFFFF;
    *frequency = (double)value * 1000.0;
    return ret;
}

int sis8300drvbcm_get_measured_trigger_period(sis8300drv_usr *sisuser, double *period)
{
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_TRIGGER_PERIOD_MEAS_REG, &value);
    if (ret) {
        return ret;
    }
    // register holds 32-bit unsigned value in 'sampling periods',
    // function returns double value in miliseconds
    *period = 1.0 / s_sampling_frequency * (double)value * 1000.0;
    return ret;
}

int sis8300drvbcm_get_measured_trigger_width(sis8300drv_usr *sisuser, double *width)
{
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_TRIGGER_WIDTH_MEAS_REG, &value);
    if (ret) {
        return ret;
    }
    // register holds 32-bit unsigned value in 'sampling periods',
    // function returns double value in miliseconds
    *width = 1.0 / s_sampling_frequency * (double)value * 1000.0;
    return ret;
}

int sis8300drvbcm_set_channel_droop_rate(sis8300drv_usr *sisuser, unsigned int channel, double droop)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_RATE_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(droop * 65536.0 / 8.4);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_droop_rate(sis8300drv_usr *sisuser, unsigned int channel, double *droop)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_RATE_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *droop = (double)value / 65536.0 * 8.4;
    return ret;
}

int sis8300drvbcm_set_channel_droop_compensating(sis8300drv_usr *sisuser, unsigned int channel, unsigned int compensating)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // droop compensating is controlled by bit 0
    if (compensating) {
        value |= (1 << 0);
    } else {
        value &= ~(1 << 0);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_droop_compensating(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *compensating)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // droop compensating status is in bit 0
    *compensating = (value & (1 << 0)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_noise_filtering(sis8300drv_usr *sisuser, unsigned int channel, unsigned int filtering)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // noise filtering is controlled by bit 1
    if (filtering) {
        value |= (1 << 1);
    } else {
        value &= ~(1 << 1);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_noise_filtering(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *filtering)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // noise filtering status is in bit 1
    *filtering = (value & (1 << 1)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_baselining_before_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int correcting)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // baseline correcting before droop compensation is controlled by bit 2
    if (correcting) {
        value |= (1 << 2);
    } else {
        value &= ~(1 << 2);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_baselining_before_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *correcting)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // baseline correcting before droop compensation status is in bit 2
    *correcting = (value & (1 << 2)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_baselining_after_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int correcting)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // baseline correcting after droop compensation is controlled by bit 3
    if (correcting) {
        value |= (1 << 3);
    } else {
        value &= ~(1 << 3);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_baselining_after_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *correcting)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // baseline correcting after droop compensation status is in bit 3
    *correcting = (value & (1 << 3)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_dc_blocking(sis8300drv_usr *sisuser, unsigned int channel, unsigned int dcblocking)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // DC blocker filter is controlled by bit 4
    if (dcblocking) {
        value |= (1 << 4);
    } else {
        value &= ~(1 << 4);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, setup 0x%x\n", __func__, channel, value);
    return ret;
}

int sis8300drvbcm_get_channel_dc_blocking(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *dcblocking)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF;
    unsigned int value;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // DC blocker filter is controlled by bit 4
    *dcblocking = (value & (1 << 4)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_lut_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // register takes 1-bit unsigned value
    disable &= 0x1;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_LUT_CONTROL_REG, disable);
    return ret;
}

int sis8300drvbcm_get_lut_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_LUT_CONTROL_REG, disable);
    if (ret) {
        return ret;
    }
    // register provides 1-bit unsigned value
    *disable &= 0x1;
    return ret;
}

int sis8300drvbcm_set_channel_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_UPPER_THRESHOLD_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_UPPER_THRESHOLD_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_channel_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_THRESHOLD_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_THRESHOLD_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_channel_errant_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_THRESHOLD_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_errant_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_THRESHOLD_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

static int set_channel_alarms_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ALARM_CONTROL_OFF;
    unsigned int value;
    // register has lowest 10 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x3FF;
    // set / clear the desired bit
    if (disable) {
        value |= (1 << bit);
    } else {
        value &= ~(1 << bit);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: channel %d, alarm control 0x%x\n", __func__, channel, value);
    return ret;
}

static int get_channel_alarms_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int *disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ALARM_CONTROL_OFF;
    unsigned int value;
    // register has lowest 10 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // check the desired bit
    *disable = (value & (1 << bit)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_channel_alarm_upper_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // upper alarm is controlled by bit 0
    return set_channel_alarms_control(sisuser, channel, 0, disable);
}

int sis8300drvbcm_get_channel_alarm_upper_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // upper alarm status is in bit 0
    return get_channel_alarms_control(sisuser, channel, 0, disable);
}

int sis8300drvbcm_set_channel_alarm_lower_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // lower alarm is controlled by bit 1
    return set_channel_alarms_control(sisuser, channel, 1, disable);
}

int sis8300drvbcm_get_channel_alarm_lower_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // lower alarm status is in bit 1
    return get_channel_alarms_control(sisuser, channel, 1, disable);
}

int sis8300drvbcm_set_channel_alarm_errant_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // errant alarm is controlled by bit 2
    return set_channel_alarms_control(sisuser, channel, 2, disable);
}

int sis8300drvbcm_get_channel_alarm_errant_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // errant alarm status is in bit 2
    return get_channel_alarms_control(sisuser, channel, 2, disable);
}

int sis8300drvbcm_set_channel_alarm_trigger_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // pulse longer than trigger alarm is controlled by bit 3
    return set_channel_alarms_control(sisuser, channel, 3, disable);
}

int sis8300drvbcm_get_channel_alarm_trigger_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // pulse longer than trigger alarm status is in bit 3
    return get_channel_alarms_control(sisuser, channel, 3, disable);
}

int sis8300drvbcm_set_channel_alarm_limit_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // pulse longer than limit alarm is controlled by bit 4
    return set_channel_alarms_control(sisuser, channel, 4, disable);
}

int sis8300drvbcm_get_channel_alarm_limit_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // pulse longer than limit alarm status is in bit 4
    return get_channel_alarms_control(sisuser, channel, 4, disable);
}

int sis8300drvbcm_set_channel_alarm_adc_overflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // ADC overflow alarm is controlled by bit 5
    return set_channel_alarms_control(sisuser, channel, 5, disable);
}

int sis8300drvbcm_get_channel_alarm_adc_overflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // ADC overflow alarm status is in bit 5
    return get_channel_alarms_control(sisuser, channel, 5, disable);
}

int sis8300drvbcm_set_channel_alarm_adc_underflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // ADC underflow alarm is controlled by bit 6
    return set_channel_alarms_control(sisuser, channel, 6, disable);
}

int sis8300drvbcm_get_channel_alarm_adc_underflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // ADC underflow alarm status is in bit 6
    return get_channel_alarms_control(sisuser, channel, 6, disable);
}

int sis8300drvbcm_set_channel_alarm_adc_stuck_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // ADC stuck alarm is controlled by bit 7
    return set_channel_alarms_control(sisuser, channel, 7, disable);
}

int sis8300drvbcm_get_channel_alarm_adc_stuck_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // ADC stuck alarm status is in bit 7
    return get_channel_alarms_control(sisuser, channel, 7, disable);
}

int sis8300drvbcm_set_channel_alarm_aiu_fault_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // AIU fault alarm is controlled by bit 8
    return set_channel_alarms_control(sisuser, channel, 8, disable);
}

int sis8300drvbcm_get_channel_alarm_aiu_fault_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // AIU fault alarm status is in bit 8
    return get_channel_alarms_control(sisuser, channel, 8, disable);
}

int sis8300drvbcm_set_channel_alarm_charge_too_high_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // charge too high alarm is controlled by bit 9
    return set_channel_alarms_control(sisuser, channel, 9, disable);
}

int sis8300drvbcm_get_channel_alarm_charge_too_high_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // charge too high alarm status is in bit 9
    return get_channel_alarms_control(sisuser, channel, 9, disable);
}

int sis8300drvbcm_get_channel_alarm_hold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ALARM_HOLD_OFF;
    // register has lowest 14 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3FFF;
    return ret;
}

int sis8300drvbcm_get_channel_alarm_first(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ALARM_FIRST_OFF;
    // register has lowest 14 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3FFF;
    return ret;
}

int sis8300drvbcm_get_channel_alarm_direct(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ALARM_DIRECT_OFF;
    // register has lowest 14 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3FFF;
    return ret;
}

// For BCM we are always working with the 4-bytes samples, and not 2 bytes samples
// as in generic firmware; memory access constrain now changes to 64 byte alignment,
// instead of 32 byte alignment as in the generic firmware.
#define SIS8300DRVBCM_BLOCK_BYTES       64

static int sis8300drvbcm_read_ram_unlocked(sis8300drv_dev *sisdevice, unsigned offset, unsigned size, void *data)
{
    int status;

    if (offset + size > sisdevice->mem_size) {
        return status_argument_range;
    }
    
    if (size % SIS8300DRVBCM_BLOCK_BYTES || offset % SIS8300DRVBCM_BLOCK_BYTES) {
        return status_argument_invalid;
    }

    // printf("%s: read RAM at 0x%08X, size 0x%08X\n", __func__, offset, size);
    status = lseek(sisdevice->handle, offset, SEEK_SET);
    if (status < 0) {
        return status_device_access;
    }

    status = read(sisdevice->handle, data, size);
    if (status < 0) {
        return status_read;
    }

    return status_success;
}

int sis8300drvbcm_read_ram(sis8300drv_usr *sisuser, unsigned offset, unsigned size, void *data)
{
    int             status;
    sis8300drv_dev  *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }
     
    status = sis8300drvbcm_read_ram_unlocked(sisdevice, offset, size, data);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }

    pthread_mutex_unlock(&sisdevice->lock);

    return status_success;
}

int sis8300drvbcm_set_minimum_trigger_period(sis8300drv_usr *sisuser, double period)
{
    // register takes 32-bit unsigned value
    unsigned int value = (unsigned int)(period / 1000.0 * s_sampling_frequency);
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_MIN_TRIG_PERIOD_REG, value);
    return ret;
}

int sis8300drvbcm_get_minimum_trigger_period(sis8300drv_usr *sisuser, double *period)
{
    // register provides 32-bit unsigned value
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_MIN_TRIG_PERIOD_REG, &value);
    if (ret) {
        return ret;
    }
    *period = (double)value * 1000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_maximum_pulse_width(sis8300drv_usr *sisuser, unsigned int width)
{
    // register takes 13-bit unsigned value
    width &= 0x1FFF;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_MAX_PULSE_WIDTH_REG, width);
    return ret;
}

int sis8300drvbcm_get_maximum_pulse_width(sis8300drv_usr *sisuser, unsigned int *width)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_MAX_PULSE_WIDTH_REG, width);
    if (ret) {
        return ret;
    }
    // register provides 13-bit unsigned value
    *width &= 0x1FFF;
    return ret;
}

int sis8300drvbcm_set_alarms_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // register takes 1-bit unsigned value
    disable &= 0x1;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ALARMS_CONTROL_REG, disable);
    return ret;
}

int sis8300drvbcm_get_alarms_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARMS_CONTROL_REG, disable);
    if (ret) {
        return ret;
    }
    // register provides 1-bit unsigned value
    *disable &= 0x1;
    return ret;
}

int sis8300drvbcm_clear_alarms(sis8300drv_usr *sisuser)
{
    // register takes 1-bit unsigned value
    unsigned clear = 0x1;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ALARMS_CLEAR_REG, clear);
    return ret;
}

static int set_alarms_control(sis8300drv_usr *sisuser, unsigned int bit, unsigned int disable)
{
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARM_CONTROL_REG, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // set / clear the desired bit
    if (disable) {
        value |= (1 << bit);
    } else {
        value &= ~(1 << bit);
    }
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ALARM_CONTROL_REG, value);
    // printf("%s: alarm control 0x%x\n", __func__, value);
    return ret;
}

static int get_alarms_control(sis8300drv_usr *sisuser, unsigned int bit, unsigned int *disable)
{
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARM_CONTROL_REG, &value);
    if (ret) {
        return ret;
    }
    // check the desired bit
    *disable = (value & (1 << bit)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_alarm_auxiliary_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // auxiliary clock frequency out of range alarm is controlled by bit 0
    return set_alarms_control(sisuser, 0, disable);
}

int sis8300drvbcm_get_alarm_auxiliary_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    // auxiliary clock frequency out of range alarm status is in bit 0
    return get_alarms_control(sisuser, 0, disable);
}

int sis8300drvbcm_set_alarm_processing_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // processing clock frequency out of range alarm is controlled by bit 1
    return set_alarms_control(sisuser, 1, disable);
}

int sis8300drvbcm_get_alarm_processing_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    // processing clock frequency out of range alarm status is in bit 1
    return get_alarms_control(sisuser, 1, disable);
}

int sis8300drvbcm_set_alarm_trigger_too_wide_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // trigger too wide alarm is controlled by bit 2
    return set_alarms_control(sisuser, 2, disable);
}

int sis8300drvbcm_get_alarm_trigger_too_wide_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    // trigger too wide alarm status is in bit 2
    return get_alarms_control(sisuser, 2, disable);
}

int sis8300drvbcm_set_alarm_trigger_too_narrow_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // trigger too narrow alarm is controlled by bit 3
    return set_alarms_control(sisuser, 3, disable);
}

int sis8300drvbcm_get_alarm_trigger_too_narrow_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    // trigger too narrow alarm status is in bit 3
    return get_alarms_control(sisuser, 3, disable);
}

int sis8300drvbcm_set_alarm_trigger_period_too_short_control(sis8300drv_usr *sisuser, unsigned int disable)
{
    // trigger period too short alarm is controlled by bit 4
    return set_alarms_control(sisuser, 4, disable);
}

int sis8300drvbcm_get_alarm_trigger_period_too_short_control(sis8300drv_usr *sisuser, unsigned int *disable)
{
    // trigger period too short alarm status is in bit 4
    return get_alarms_control(sisuser, 4, disable);
}

int sis8300drvbcm_get_alarm_hold(sis8300drv_usr *sisuser, unsigned int *alarms)
{
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARM_HOLD_REG, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3F;
    return ret;
}

int sis8300drvbcm_get_alarm_first(sis8300drv_usr *sisuser, unsigned int *alarms)
{
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARM_FIRST_REG, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3F;
    return ret;
}

int sis8300drvbcm_get_alarm_direct(sis8300drv_usr *sisuser, unsigned int *alarms)
{
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_ALARM_DIRECT_REG, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0x3F;
    return ret;
}

int sis8300drvbcm_set_status_ready(sis8300drv_usr *sisuser, unsigned int status)
{
    // set / clear the BCM ready bit
    unsigned int value = 0;
    if (status) {
        value |= (1 << 2);
    } else {
        value &= ~(1 << 2);
    }
    // register takes 1-bit unsigned value
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_STATUS_REG, value);
    return ret;
}

int sis8300drvbcm_get_status_bits(sis8300drv_usr *sisuser, unsigned int *status)
{
    // register has lowest 3 bits defined
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_STATUS_REG, status);
    if (ret) {
        return ret;
    }
    *status &= 0xFFFF;
    return ret;
}

int sis8300drvbcm_get_channel_beam_exists(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *exist)
{
    unsigned int reg = SIS8300BCM_ACCT_BEAM_EXISTS_REG;
    // register has lowest 10 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, exist);
    if (ret) {
        return ret;
    }
    // 1 bit per channel masking
    *exist = (*exist>>channel) & 0x01; 
    return ret;
}

int sis8300drvbcm_set_channel_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int length)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_MAX_PULSE_LENGTH_REG;
    // register has lowest 16 bits defined
    length &= 0xFFFF;
    return sis8300drv_reg_write(sisuser, reg, length);
}

int sis8300drvbcm_get_channel_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *length)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_MAX_PULSE_LENGTH_REG;
    // register has lowest 16 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, length);
    if (ret) {
        return ret;
    }
    *length &= 0xFFFF;
    return ret;
}

int sis8300drvbcm_set_channel_lower_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_START_REG;
    // register has lowest 13 bits defined
    int value = (int)(start / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_channel_lower_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_START_REG;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *start = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_channel_lower_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_END_REG;
    // register has lowest 13 bits defined
    int value = (int)(end / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_channel_lower_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LOWER_END_REG;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *end = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_channel_errant_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_START_REG;
    // register has lowest 13 bits defined
    int value = (int)(start / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}
int sis8300drvbcm_get_channel_errant_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_START_REG;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *start = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_channel_errant_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_END_REG;
    // register has lowest 13 bits defined
    int value = (int)(end / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_channel_errant_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_ERRANT_END_REG;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *end = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_get_calibration_sample(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, double *sample)
{
    int calibration_sample_reg_offset = 0; 
    if (index == 1) {
        calibration_sample_reg_offset = SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE1_OFF; 
    } else if (index == 2) {
        calibration_sample_reg_offset = SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE2_OFF; 
    } else if (index == 3) {
        calibration_sample_reg_offset = SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE3_OFF; 
    } else if (index == 4) {
        calibration_sample_reg_offset = SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE4_OFF; 
    } else {
        printf("%s: index %d is out of range (only 1 to 4 are accepted)\n", __func__, index);
        return -1;
    }
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            calibration_sample_reg_offset;
    // register provides 16-bit signed value
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *sample = (double)((short int)value);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, (short int)value);
    return ret;
}

int sis8300drvbcm_set_channel_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double coefficient)
{
    double error;
    unsigned int value;
    // leaky_coefficient_reg = Q9.23 of e^(-Ts/Tcons)
    // Ts = 1/sampling_freq and Tcons = leaky_coefficient [sec]
    coefficient = exp(-1000 / (s_sampling_frequency * coefficient));
    int ret = sis8300drv_double_2_Qmn(coefficient, sis8300drvbcm_Qmn_leaky_coefficient, &value, &error);
    if (ret) {
        return ret;
    }
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LEAKY_COFF_REG;
    // register takes Q9.23 value
    ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_channel_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double *coefficient)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LEAKY_COFF_REG;
    // register takes Q9.23 value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    ret = sis8300drv_Qmn_2_double(value, sis8300drvbcm_Qmn_leaky_coefficient, coefficient);
    // leaky_coefficient in ms
    *coefficient = -1000 / (log(*coefficient) * s_sampling_frequency);
    return ret;
}

int sis8300drvbcm_set_channel_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LEAKY_THR_REG;
    // register takes 32-bit value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_channel_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_LEAKY_THR_REG;
    // register provides 32-bit value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_probe_setup(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_PROB_BANK_OFF + \
            SIS8300BCM_PROB_X_SOURCE_OFF;
    // register has lowest 8 bits defined
    source &= 0xFF;
    int ret = sis8300drv_reg_write(sisuser, reg, source);
    return ret;
}

int sis8300drvbcm_get_probe_setup(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_PROB_BANK_OFF + \
            SIS8300BCM_PROB_X_SOURCE_OFF;
    // register has lowest 8 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, source);
    if (ret) {
        return ret;
    }
    *source &= 0xFF;
    return ret;
}

int sis8300drvbcm_set_timestamp(sis8300drv_usr *sisuser, unsigned int sec, unsigned int nsec)
{
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_TIMESTAMP_LOW_REG, nsec);
    if (ret) {
        return ret;
    }
    // EPICS and POSIX Epoch differs in 20 year (631152000 seconds)
    unsigned int value = sec + POSIX_TIME_AT_EPICS_EPOCH;
    ret = sis8300drv_reg_write(sisuser, SIS8300BCM_TIMESTAMP_HIGH_REG, value);
    if (ret) {
        return ret;
    }
    return ret;
}

int sis8300drvbcm_get_timestamp(sis8300drv_usr *sisuser, unsigned int *sec, unsigned int *nsec)
{
    int ret = sis8300drv_reg_read(sisuser, SIS8300BCM_TIMESTAMP_LOW_REG, nsec);
    if (ret) {
        return ret;
    }
    unsigned int value;
    ret = sis8300drv_reg_read(sisuser, SIS8300BCM_TIMESTAMP_HIGH_REG, &value);
    if (ret) {
        return ret;
    }
    // EPICS and POSIX Epoch differs in 20 year (631152000 seconds)
    *sec = value - POSIX_TIME_AT_EPICS_EPOCH;
    return ret;
}

int sis8300drvbcm_set_beam_mode(sis8300drv_usr *sisuser, unsigned int mode)
{
    return sis8300drv_reg_write(sisuser, SIS8300BCM_BEAM_MODE_REG, mode);
}

int sis8300drvbcm_get_beam_mode(sis8300drv_usr *sisuser, unsigned int *mode)
{
    return sis8300drv_reg_read(sisuser, SIS8300BCM_BEAM_MODE_REG, mode);
}

int sis8300drvbcm_set_beam_destination(sis8300drv_usr *sisuser, unsigned int dest)
{
    return sis8300drv_reg_write(sisuser, SIS8300BCM_BEAM_DESTINATION_REG, dest);
}

int sis8300drvbcm_get_beam_destination(sis8300drv_usr *sisuser, unsigned int *dest)
{
    return sis8300drv_reg_read(sisuser, SIS8300BCM_BEAM_DESTINATION_REG, dest);
}

int sis8300drvbcm_set_differential_source_a(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SOURCE_A_OFF;
    // register has lowest 4 bits defined
    source &= 0xF;
    int ret = sis8300drv_reg_write(sisuser, reg, source);
    return ret;
}

int sis8300drvbcm_get_differential_source_a(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SOURCE_A_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, source);
    if (ret) {
        return ret;
    }
    *source &= 0xF;
    return ret;
}

int sis8300drvbcm_set_differential_source_b(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SOURCE_B_OFF;
    // register has lowest 4 bits defined
    source &= 0xF;
    int ret = sis8300drv_reg_write(sisuser, reg, source);
    return ret;
}

int sis8300drvbcm_get_differential_source_b(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SOURCE_B_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, source);
    if (ret) {
        return ret;
    }
    *source &= 0xF;
    return ret;
}

int sis8300drvbcm_set_differential_delay(sis8300drv_usr *sisuser, unsigned int channel, double delay)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_DELAY_OFF;
    // register takes 11-bit value
    int value = (int)(delay / 1000000000.0 * s_sampling_frequency);
    // XXX: do not mask off higher bits because value is signed value ??
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_differential_delay(sis8300drv_usr *sisuser, unsigned int channel, double *delay)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_DELAY_OFF;
    int value;
    // register provides 11-bit value
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // XXX: do not mask off higher bits because value is signed value ??
    *delay = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_differential_fast_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_THRESHOLD_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_differential_fast_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_THRESHOLD_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_differential_medium_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_THRESHOLD_OFF;
    // register takes 16-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    value &= 0xFFFF;
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_differential_medium_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_THRESHOLD_OFF;
    unsigned int value;
    // register provides 16-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xFFFF;
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_differential_slow_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SLOW_THRESHOLD_OFF;
    // register takes 32-bit unsigned value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    int ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_differential_slow_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_SLOW_THRESHOLD_OFF;
    unsigned int value;
    // register provides 32-bit unsigned value
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

static int set_differential_alarms_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_ALARM_CONTROL_OFF;
    unsigned int value;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0xF;
    // set / clear the desired bit
    if (disable) {
        value |= (1 << bit);
    } else {
        value &= ~(1 << bit);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: diff %d, alarm control 0x%x\n", __func__, channel, value);
    return ret;
}

static int get_differential_alarms_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int *disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_ALARM_CONTROL_OFF;
    unsigned int value;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // check the desired bit
    *disable = (value & (1 << bit)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_differential_alarm_fast_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // fast alarm is controlled by bit 0
    return set_differential_alarms_control(sisuser, channel, 0, disable);
}

int sis8300drvbcm_get_differential_alarm_fast_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // fast alarm status is in bit 0
    return get_differential_alarms_control(sisuser, channel, 0, disable);
}

int sis8300drvbcm_set_differential_alarm_medium_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // medium alarm is controlled by bit 1
    return set_differential_alarms_control(sisuser, channel, 1, disable);
}

int sis8300drvbcm_get_differential_alarm_medium_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // medium alarm status is in bit 1
    return get_differential_alarms_control(sisuser, channel, 1, disable);
}

int sis8300drvbcm_set_differential_alarm_slow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // slow alarm is controlled by bit 2
    return set_differential_alarms_control(sisuser, channel, 2, disable);
}

int sis8300drvbcm_get_differential_alarm_slow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // slow alarm status is in bit 2
    return get_differential_alarms_control(sisuser, channel, 2, disable);
}

int sis8300drvbcm_set_differential_alarm_leaky_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable)
{
    // leaky alarm is controlled by bit 3 
    return set_differential_alarms_control(sisuser, channel, 3, disable);
}

int sis8300drvbcm_get_differential_alarm_leaky_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable)
{
    // leaky alarm status is in bit 3
    return get_differential_alarms_control(sisuser, channel, 3, disable);
}

int sis8300drvbcm_get_differential_alarm_hold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_ALARM_HOLD_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0xF;
    return ret;
}

int sis8300drvbcm_get_differential_alarm_first(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_ALARM_FIRST_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0xF;
    return ret;
}

int sis8300drvbcm_get_differential_alarm_direct(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_ALARM_DIRECT_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, alarms);
    if (ret) {
        return ret;
    }
    *alarms &= 0xF;
    return ret;
}

int sis8300drvbcm_get_differential_interlock_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable)
{
    unsigned int reg = SIS8300BCM_DIFF_INTERLOCK_ENABLE_REG;
    // register has lowest 10 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, enable);
    if (ret) {
        return ret;
    }
    // 1 bit per channel masking
    *enable = (*enable>>channel) & 0x01; 
    return ret;
}

int sis8300drvbcm_set_differential_rising_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_RISING_WINDOW_START_REG;
    // register has lowest 16 bits defined
    int value = (int)(start / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_rising_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_RISING_WINDOW_START_REG;
    // register has lowest 16 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *start = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_differential_rising_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_RISING_WINDOW_END_REG;
    // register has lowest 16 bits defined
    int value = (int)(end / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_rising_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_RISING_WINDOW_END_REG;
    // register has lowest 16 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *end = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_differential_falling_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FALLING_WINDOW_START_REG;
    // register has lowest 16 bits defined
    int value = (int)(start / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_falling_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FALLING_WINDOW_START_REG;
    // register has lowest 16 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *start = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

int sis8300drvbcm_set_differential_falling_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FALLING_WINDOW_END_REG;
    // register has lowest 16 bits defined
    int value = (int)(end / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_falling_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FALLING_WINDOW_END_REG;
    // register has lowest 16 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *end = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

static int set_differential_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_BETWEEN_REG;
    unsigned int value;
    // register has lowest 3 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x7;
    // set / clear the desired bit
    if (disable) {
        value |= (1 << bit);
    } else {
        value &= ~(1 << bit);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    // printf("%s: diff %d, between 0x%x\n", __func__, channel, value);
    return ret;
}

static int get_differential_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int bit, unsigned int *disable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_BETWEEN_REG;
    unsigned int value;
    // register has lowest 3 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // check the desired bit
    *disable = (value & (1 << bit)) ? 1 : 0;
    return ret;
}

int sis8300drvbcm_set_differential_ws_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between)
{
    // wire scanner between is controlled by bit 0
    return set_differential_between(sisuser, channel, 0, between);
}

int sis8300drvbcm_get_differential_ws_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between)
{
    // wire scanner between status is in bit 0
    return get_differential_between(sisuser, channel, 0, between);
}

int sis8300drvbcm_set_differential_emu_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between)
{
    // emittance unit between is controlled by bit 1
    return set_differential_between(sisuser, channel, 1, between);
}

int sis8300drvbcm_get_differential_emu_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between)
{
    // emittance unit between status is in bit 1
    return get_differential_between(sisuser, channel, 1, between);
}

int sis8300drvbcm_set_differential_rfq_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between)
{
    // RFQ between is controlled by bit 2
    return set_differential_between(sisuser, channel, 2, between);
}

int sis8300drvbcm_get_differential_rfq_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between)
{
    // RFQ between status is in bit 2
    return get_differential_between(sisuser, channel, 2, between);
}

static int set_lut_value(sis8300drv_usr *sisuser, unsigned int address, unsigned int data)
{
    // printf("%s: address = 0x%04X, data = %d (0x%08X)\n", __func__, address, data, data);

    // register takes 32-bit unsigned value, but mask off the reserved bits
    address &= 0x1FFF;
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_LUT_ADDRESS_REG, address);
    if (ret) {
        return ret;
    }
    // register takes 32-bit unsigned value
    return sis8300drv_reg_write(sisuser, SIS8300BCM_LUT_DATA_REG, data);
}

int sis8300drvbcm_set_lut_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, unsigned int length)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 parameter type (0 = max pulse length, 1 = lower thr., 2 = upper thr., 3 = reserved)
    address |= (0 << 4);
    // 6 .. 9 ACCT number (0 .. 9)
    address |= (channel << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 2 .. 15 = reserved)
    address |= (0 << 10);
    unsigned int data = length;
    data |= (beam_mode << 16);
    return set_lut_value(sisuser, address, data);
}

int sis8300drvbcm_set_lut_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, double threshold)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 parameter type (0 = max pulse length, 1 = lower thr., 2 = upper thr., 3 = reserved)
    address |= (1 << 4);
    // 6 .. 9 ACCT number (0 .. 9)
    address |= (channel << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 2 .. 15 = reserved)
    address |= (0 << 10);
    unsigned int data = (unsigned int)(threshold * 32768.0 / 200.0);
    data |= (beam_mode << 16);
    return set_lut_value(sisuser, address, data);
}

int sis8300drvbcm_set_lut_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, double threshold)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 parameter type (0 = max pulse length, 1 = lower thr., 2 = upper thr., 3 = reserved)
    address |= (2 << 4);
    // 6 .. 9 ACCT number (0 .. 9)
    address |= (channel << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 2 .. 15 = reserved)
    address |= (0 << 10);
    unsigned int data = (unsigned int)(threshold * 32768.0 / 200.0);
    data |= (beam_mode << 16);
    return set_lut_value(sisuser, address, data);
}

int sis8300drvbcm_set_lut_min_trigger_period(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_mode, double min_trigger_period)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 parameter type (0 = max pulse length, 1 = lower thr., 2 = upper thr., 3 = reserved)
    address |= (0 << 4);
    // 6 .. 9 ACCT number (0 .. 9)
    address |= (0 << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 2 .. 15 = reserved)
    address |= (1 << 11);
    unsigned int data = (unsigned int)(round(min_trigger_period * 1000.0));
    data |= (beam_mode << 25);
    // printf("%s: min_trigger_period = %f, data = %d (0x%08X)\n", __func__, min_trigger_period, data, data);
    return set_lut_value(sisuser, address, data);
}

int sis8300drvbcm_set_lut_interlock_masking(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_dest, unsigned int mask)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 0
    address |= (0 << 4);
    // 6 .. 9 0
    address |= (0 << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 2 .. 15 = reserved)
    address |= (1 << 10);
    unsigned int data = mask;
    data |= (beam_dest << 16);
    return set_lut_value(sisuser, address, data);
}

int sis8300drvbcm_set_lut_interlock_enable(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_dest, unsigned int mask)
{
    unsigned int address = 0;
    // 0 .. 3 LUT index (0 .. 15)
    address |= (index & 0xFF);
    // 4 .. 5 0
    address |= (0 << 4);
    // 6 .. 9 0
    address |= (0 << 6);
    // 10 .. 11 LUT type (0 = ACCT, 1 = interlock masking, 3 = DIFF , 2 .. 15 = reserved)
    address |= (3 << 10);
    unsigned int data = mask;
    data |= (beam_dest << 16);
    return set_lut_value(sisuser, address, data);
}

// TODO?
// int sis8300drvbcm_get_differential_fast_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
// int sis8300drvbcm_get_differential_fast_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);
// int sis8300drvbcm_get_differential_medium_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
// int sis8300drvbcm_get_differential_medium_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);
// int sis8300drvbcm_get_differential_slow_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
// int sis8300drvbcm_get_differential_slow_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);

int sis8300drvbcm_set_clock_source(sis8300drv_usr *sisuser, sis8300drv_clk_src source)
{
    uint32_t ui32_reg_val;

    switch (source) {
        case clk_src_backplane_a:
            ui32_reg_val = SIS8300DRV_CLKSRC_BPA;
            break;
        default:
            return status_argument_invalid;
    }
    return sis8300drv_reg_write(sisuser, SIS8300_CLOCK_DISTRIBUTION_MUX_REG, ui32_reg_val);
}

int sis8300drvbcm_get_clock_source(sis8300drv_usr *sisuser, sis8300drv_clk_src *source)
{
    uint32_t ui32_reg_val;
    int ret = sis8300drv_reg_read(sisuser, SIS8300_CLOCK_DISTRIBUTION_MUX_REG, &ui32_reg_val);
    if (ret) {
        return ret;
    }
    switch (ui32_reg_val) {
        case SIS8300DRV_CLKSRC_BPA:
            *source = clk_src_backplane_a;
            break;
        default:
            return status_argument_invalid;
    }
    return status_success;
}

int sis8300drvbcm_set_differential_fast_window_width(sis8300drv_usr *sisuser, unsigned int channel, double width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_OFF;
    unsigned value = (unsigned)((width - (1000000000.0 / s_sampling_frequency)) / (1000000000.0 / s_sampling_frequency));
    // register takes 8 bit unsigned value
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_differential_fast_window_width(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_OFF;
    // register has lowest 8 bits defined
    unsigned value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *width = (double)value * (1000000000.0 / s_sampling_frequency) + (1000000000.0 / s_sampling_frequency);
    return ret;
}

int sis8300drvbcm_set_differential_medium_window_width(sis8300drv_usr *sisuser, unsigned int channel, double width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_OFF;
    unsigned value = width - 2.0;
    // register takes 8 bit unsigned value
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_differential_medium_window_width(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_OFF;
    // register has lowest 8 bits defined
    unsigned value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *width = (double)value + 2.0;
    return ret;
}

int sis8300drvbcm_set_differential_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double coefficient)
{
    double error;
    unsigned int value;
    // leaky_coefficient_reg = Q9.23 of e^(-Ts/Tcons)
    // Ts = 1/sampling_freq and Tcons = leaky_coefficient [sec]
    coefficient = exp(-1000 / (s_sampling_frequency * coefficient));
    int ret = sis8300drv_double_2_Qmn(coefficient, sis8300drvbcm_Qmn_leaky_coefficient, &value, &error);
    if (ret) {
        return ret;
    }
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_LEAKY_COFF_REG;
    // register takes Q9.23 value
    ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double *coefficient)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_LEAKY_COFF_REG;
    // register takes Q9.23 value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    ret = sis8300drv_Qmn_2_double(value, sis8300drvbcm_Qmn_leaky_coefficient, coefficient);
    // leaky_coefficient in ms
    *coefficient = -1000 / (log(*coefficient) * s_sampling_frequency);
    return ret;
}

int sis8300drvbcm_set_differential_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_LEAKY_THR_REG;
    // register takes 32-bit value
    unsigned int value = (unsigned int)(threshold * 32768.0 / 200.0);
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}

int sis8300drvbcm_get_differential_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_LEAKY_THR_REG;
    // register provides 32-bit value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (double)value / 32768.0 * 200.0;
    return ret;
}

int sis8300drvbcm_set_differential_fast_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_INV_OFF;
    // non-inverted value
    unsigned value = (unsigned)((width - (1000000000.0 / s_sampling_frequency)) / (1000000000.0 / s_sampling_frequency));
    // inverted value
    value = (int)((1 << 15) / (value + 1));
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_differential_fast_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    // get the N value
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_OFF;
    // register has lowest 8 bits defined
    unsigned n_value;
    int ret = sis8300drv_reg_read(sisuser, reg, &n_value);
    if (ret) {
        return ret;
    }

    reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_INV_OFF;
    // register has lowest 16 bits defined
    unsigned value;
    ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *width = ((double)value * (n_value + 1.0)) / (double)(1 << 15);
    return ret;
}

int sis8300drvbcm_set_differential_medium_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_INV_OFF;
    // non-inverted value
    unsigned int value = (unsigned int) (width - 2.0);
    // inverted value
    value = (1 << 31) / (88 * (value + 2));
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_differential_medium_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    // get the N value
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_OFF;
    // register has lowest 8 bits defined
    unsigned n_value;
    int ret = sis8300drv_reg_read(sisuser, reg, &n_value);
    if (ret) {
        return ret;
    }

    reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_DIFF_BANK_OFF + \
            SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_INV_OFF;
    // register has lowest 32 bits defined
    unsigned int value;
    ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    *width = ((double)value * 88.0 * (n_value + 2.0)) / (double)(1 << 31);
    return ret;

}

// used to control enable calibration pulse
int sis8300drvbcm_set_enable_calibration_pulse(sis8300drv_usr *sisuser, unsigned int enable_calibration_pulse)
{
    int ret = sis8300drv_reg_write(sisuser, SIS8300BCM_ENABLE_CALIBRATION_PULSE_REG, enable_calibration_pulse);
    return ret;
}

// used to control aux upper threshold
int sis8300drvbcm_set_channel_aux_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_UPPER_THRESHOLD_OFF;
    // register takes a signed value
    int value = (int)(threshold * 32768);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_channel_aux_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_UPPER_THRESHOLD_OFF;
    // register holds a signed value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (value / 32768.0);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return ret;
}

// used to control aux lower threshold
int sis8300drvbcm_set_channel_aux_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_LOWER_THRESHOLD_OFF;
    // register takes a signed value
    int value = (int)(threshold * 32768);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_channel_aux_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_LOWER_THRESHOLD_OFF;
    // register holds a signed value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (value / 32768.0);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return ret;
}

// used to control aux hysteresis threshold
int sis8300drvbcm_set_channel_aux_hysteresis_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_HYSTERESIS_THRESHOLD_OFF;
    // register takes a signed value
    int value = (int)(threshold * 32768);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_channel_aux_hysteresis_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_AUX_HYSTERESIS_THRESHOLD_OFF;
    // register holds a signed value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (value / 32768.0);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return ret;
}

// used to control beam above threshold
int sis8300drvbcm_set_channel_beam_above_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_BEAM_ABOVE_THRESHOLD_REG;
    // register takes a signed value
    int value = (int)(threshold * 512);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return sis8300drv_reg_write(sisuser, reg, value);
}

int sis8300drvbcm_get_channel_beam_above_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_BEAM_ABOVE_THRESHOLD_REG;
    // register holds a signed value
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    *threshold = (value / 512.0);
    // printf("%s: channel %d, reg 0x%x, value: %d\n", __func__, channel, reg, value);
    return ret;
}

int sis8300drvbcm_get_channel_trigger_width(sis8300drv_usr *sisuser, unsigned int channel, double *width)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
                        (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
                        SIS8300BCM_ACCT_BANK_OFF + \
                        SIS8300BCM_ACCT_X_TRIGGER_WIDTH_OFF;
    unsigned int value;
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // register holds 32-bit unsigned value in 'sampling periods',
    // function returns double value in miliseconds
    *width = 1.0 / s_sampling_frequency * (double)value * 1000.0;
    return ret;
}

// used to control auto flat top start
int sis8300drvbcm_set_channel_auto_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_RISING_OFF;
    // register has lowest 13 bits defined
    int value = (int)(start / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}
int sis8300drvbcm_get_channel_auto_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double *start)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_RISING_OFF;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *start = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

// used to control auto flat top end 
int sis8300drvbcm_set_channel_auto_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_FALLING_OFF;
    // register has lowest 13 bits defined
    int value = (int)(end / 1000000000.0 * s_sampling_frequency);
    // do not mask off higher bits because value is signed value!
    int ret = sis8300drv_reg_write(sisuser, reg, (unsigned)value);
    return ret;
}
int sis8300drvbcm_get_channel_auto_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double *end)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_FALLING_OFF;
    // register has lowest 13 bits defined
    int value;
    int ret = sis8300drv_reg_read(sisuser, reg, (unsigned *)&value);
    if (ret) {
        return ret;
    }
    // do not mask off higher bits because value is signed value!
    *end = (double)value * 1000000000.0 / s_sampling_frequency;
    return ret;
}

// used to control auto flat top enable
int sis8300drvbcm_set_channel_auto_flattop_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_ENABLE_OFF;
    int ret = sis8300drv_reg_write(sisuser, reg, enable);
    return ret;
}
int sis8300drvbcm_get_channel_auto_flattop_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_AUTO_FLATTOP_ENABLE_OFF;
    int ret = sis8300drv_reg_read(sisuser, reg, enable);
    return ret;
}

// used to control beam absence enable
int sis8300drvbcm_set_channel_beam_absence(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_BEAM_ABSENCE_OFF;
    int ret = sis8300drv_reg_write(sisuser, reg, enable);
    return ret;
}
int sis8300drvbcm_get_channel_beam_absence(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_ACCT_BANK_OFF + \
            SIS8300BCM_ACCT_X_BEAM_ABSENCE_OFF;
    int ret = sis8300drv_reg_read(sisuser, reg, enable);
    return ret;
}

// used to control fiber out data select
int sis8300drvbcm_set_fiber_out_data_select(sis8300drv_usr *sisuser, unsigned int channel, unsigned int selection)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_OUT_DATA_SELECT_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // out data select is controlled by lowest 4 bits
    selection &= 0xF;
    selection += (value & (1 << 4));  // preserve enable bit
    ret = sis8300drv_reg_write(sisuser, reg, selection);
    return ret;
}

int sis8300drvbcm_get_fiber_out_data_select(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *selection)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_OUT_DATA_SELECT_OFF;
    // register has lowest 4 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, selection);
    if (ret) {
        return ret;
    }
    *selection &= 0xF;
    return ret;
}

int sis8300drvbcm_set_fiber_out_data_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_OUT_DATA_SELECT_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    value &= 0x1F;
    // out data enable is controlled by bit 4
    if (enable) {
        value |= (1 << 4);
    } else {
        value &= ~(1 << 4);
    }
    ret = sis8300drv_reg_write(sisuser, reg, value);
    return ret;
}

int sis8300drvbcm_get_fiber_out_data_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_OUT_DATA_SELECT_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // out data select enable is in bit 4
    *enable = (value & (1 << 4)) ? 1 : 0;
    return ret;
}

// used to control fiber status bits
int sis8300drvbcm_get_fiber_sfp_present(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *sfp_present)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_STATUS_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // SFP present status is in bit 0
    *sfp_present = (value & (1 << 0)) ? 1 : 0;
    //printf("%s: channel %d, fiber status: 0x%x, sfp_present: %d\n", __func__, channel, value, *sfp_present);
    return ret;
}

int sis8300drvbcm_get_fiber_lane_up(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *lane_up)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_STATUS_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // lane up status is in bit 1
    *lane_up = (value & (1 << 1)) ? 1 : 0;
    //printf("%s: channel %d, fiber status: 0x%x, lane_up: %d\n", __func__, channel, value, *lane_up);
    return ret;
}

int sis8300drvbcm_get_fiber_channel_up(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *channel_up)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_STATUS_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // channel up status is in bit 2
    *channel_up = (value & (1 << 2)) ? 1 : 0;
    //printf("%s: channel %d, fiber status: 0x%x, channel_up: %d\n", __func__, channel, value, *channel_up);
    return ret;
}

int sis8300drvbcm_get_fiber_hardware_error(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *hardware_error)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_STATUS_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // hardware error status is in bit 3
    *hardware_error = (value & (1 << 3)) ? 1 : 0;
    //printf("%s: channel %d, fiber status: 0x%x, hardware_error: %d\n", __func__, channel, value, *hardware_error);
    return ret;
}

int sis8300drvbcm_get_fiber_software_error(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *software_error)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_STATUS_OFF;
    unsigned int value;
    // register has lowest 5 bits defined
    int ret = sis8300drv_reg_read(sisuser, reg, &value);
    if (ret) {
        return ret;
    }
    // software error status is in bit 4
    *software_error = (value & (1 << 4)) ? 1 : 0;
    //printf("%s: channel %d, fiber status: 0x%x, software_error: %d\n", __func__, channel, value, *software_error);
    return ret;
}

int sis8300drvbcm_set_fiber_reset(sis8300drv_usr *sisuser, unsigned int channel, unsigned int reset)
{
    unsigned int reg = SIS8300BCM_CHANNEL_BLOCK_BASE + \
            (SIS8300BCM_CHANNEL_BLOCK_SIZE * channel) + \
            SIS8300BCM_FIBR_BANK_OFF + \
            SIS8300BCM_FIBR_X_RESET_OFF;
    int ret = sis8300drv_reg_write(sisuser, reg, reset);
    return ret;
}

