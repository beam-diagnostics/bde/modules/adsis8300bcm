/**
 * Struck 8300 BCM Linux userspace library.
 * Copyright (C) 2016-2019  ESS ERIC
 * Copyright (C) 2014-2015  Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300bcm_reg.h
 * @brief Header of sis8300 BCM registers.
 * @author Joao Paulo Martins, Hinko Kocevar
 */

#ifndef SIS8300BCM_REG_H_
#define SIS8300BCM_REG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 List of BCM specific registers.
 Note that only software implemeted registers are listed here.
 Add new registers as needed, obey order by address.
 */

/* board wide registers */
#define SIS8300BCM_FW_VER_REG                           0x400
#define SIS8300BCM_TIMESTAMP_LOW_REG                    0x401
#define SIS8300BCM_TIMESTAMP_HIGH_REG                   0x402
#define SIS8300BCM_FW_GIT_HASH_REG                      0x404
#define SIS8300BCM_MIN_TRIG_PERIOD_REG                  0x40D
#define SIS8300BCM_MAX_PULSE_WIDTH_REG                  0x40E
#define SIS8300BCM_ENABLE_CALIBRATION_PULSE_REG         0x40F
#define SIS8300BCM_STATUS_REG                           0x411
#define SIS8300BCM_CLOCK_FREQUENCY_MEAS_REG             0x412
#define SIS8300BCM_TRIGGER_PERIOD_MEAS_REG              0x413
#define SIS8300BCM_TRIGGER_WIDTH_MEAS_REG               0x414
#define SIS8300BCM_ALARMS_CLEAR_REG                     0x416
#define SIS8300BCM_ALARMS_CONTROL_REG                   0x417
#define SIS8300BCM_ALARM_CONTROL_REG                    0x418
#define SIS8300BCM_ALARM_HOLD_REG                       0x419
#define SIS8300BCM_ALARM_FIRST_REG                      0x41A
#define SIS8300BCM_ALARM_DIRECT_REG                     0x41B
#define SIS8300BCM_BEAM_TRIGGER_SOURCE_REG              0x41D
#define SIS8300BCM_ACCT_BEAM_EXISTS_REG                 0x424
#define SIS8300BCM_BEAM_MODE_REG                        0x425
#define SIS8300BCM_BEAM_DESTINATION_REG                 0x426
#define SIS8300BCM_ACQ_CFG_CH_NUM_REG                   0x427
#define SIS8300BCM_ACQ_CFG_NUM_SAMPLES_REG              0x428
#define SIS8300BCM_ACQ_CFG_MEM_ADDR_REG                 0x429
#define SIS8300BCM_ACQ_CFG_FRAC_BITS_REG                0x42A
#define SIS8300BCM_ACQ_CFG_CONV_FACTOR_REG              0x42B
#define SIS8300BCM_ACQ_CFG_CONV_OFFSET_REG              0x42C
#define SIS8300BCM_ACQ_CFG_CONV_DECIM_REG               0x42D
#define SIS8300BCM_ACQ_CFG_CH_CTRL_REG                  0x42E
#define SIS8300BCM_LUT_ADDRESS_REG                      0x42F
#define SIS8300BCM_LUT_DATA_REG                         0x430
#define SIS8300BCM_LUT_CONTROL_REG                      0x431
#define SIS8300BCM_ACQUITISION_TRIGGER_SOURCE_REG       0x436
#define SIS8300BCM_CRATE_ID_REG                         0x437
#define SIS8300BCM_PULSE_WIDTH_FILTER_REG               0x43E
#define SIS8300BCM_DIFF_INTERLOCK_ENABLE_REG            0x443

/* channel specific registers start at 0x500 */
#define SIS8300BCM_CHANNEL_BLOCK_BASE                   0x500
/*
 Each channel specific register block size is 0x100.
 channel    offset
       0    0x500
       1    0x600
       2    0x700
       ..
       9    0xE00
*/
#define SIS8300BCM_CHANNEL_BLOCK_SIZE                   0x100
/*
 Within the channel block, each bank is 0x40 long.
 channel 0
    bank    offset
    ACCT    0x500
    DIFF    0x540
    FIBR    0x580
    PROB    0x5C0
 channel 1
    bank    offset
    ACCT    0x600
    DIFF    0x640
    FIBR    0x680
    PROB    0x6C0
 ..
  channel 9
    bank    offset
    ACCT    0xE00
    DIFF    0xE40
    FIBR    0xE80
    PROB    0xEC0
*/
#define SIS8300BCM_BANK_BLOCK_SIZE                      0x040
#define SIS8300BCM_ACCT_BANK_OFF                        0x000
#define SIS8300BCM_DIFF_BANK_OFF                        0x040
#define SIS8300BCM_FIBR_BANK_OFF                        0x080
#define SIS8300BCM_PROB_BANK_OFF                        0x0C0


/*
 ACCT block registers
 */
#define SIS8300BCM_ACCT_X_FINE_DELAY_OFF                0x000
#define SIS8300BCM_ACCT_X_SCALE_OFF                     0x001
#define SIS8300BCM_ACCT_X_UPPER_THRESHOLD_OFF           0x002
#define SIS8300BCM_ACCT_X_LOWER_THRESHOLD_OFF           0x003
#define SIS8300BCM_ACCT_X_ERRANT_THRESHOLD_OFF          0x004
#define SIS8300BCM_ACCT_X_OFFSET_OFF                    0x005
#define SIS8300BCM_ACCT_X_DROOP_RATE_OFF                0x006
#define SIS8300BCM_ACCT_X_DROOP_NF_BC_DCB_OFF           0x007
#define SIS8300BCM_ACCT_X_ALARM_CONTROL_OFF             0x008
#define SIS8300BCM_ACCT_X_PULSE_WIDTH_OFF               0x014
#define SIS8300BCM_ACCT_X_PULSE_CHARGE_LOW_OFF          0x015
#define SIS8300BCM_ACCT_X_PULSE_CHARGE_HIGH_OFF         0x016
#define SIS8300BCM_ACCT_X_FLATTOP_CHARGE_LOW_OFF        0x017
#define SIS8300BCM_ACCT_X_FLATTOP_CHARGE_HIGH_OFF       0x018
#define SIS8300BCM_ACCT_X_ALARM_HOLD_OFF                0x019
#define SIS8300BCM_ACCT_X_ALARM_FIRST_OFF               0x01A
#define SIS8300BCM_ACCT_X_ALARM_DIRECT_OFF              0x01B
#define SIS8300BCM_ACCT_X_MAX_PULSE_LENGTH_REG          0x01C
#define SIS8300BCM_ACCT_X_LOWER_START_REG               0x01E
#define SIS8300BCM_ACCT_X_LOWER_END_REG                 0x01F
#define SIS8300BCM_ACCT_X_ERRANT_START_REG              0x020
#define SIS8300BCM_ACCT_X_ERRANT_END_REG                0x021
#define SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE1_OFF       0x022
#define SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE2_OFF       0x023
#define SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE3_OFF       0x024
#define SIS8300BCM_ACCT_X_CALIBRATION_SAMPLE4_OFF       0x025
#define SIS8300BCM_ACCT_X_TRIGGER_SOURCE_OFF            0x02C
#define SIS8300BCM_ACCT_X_LEAKY_COFF_REG                0x02D
#define SIS8300BCM_ACCT_X_LEAKY_THR_REG                 0x02E
#define SIS8300BCM_ACCT_X_FLATTOP_RISING_OFF            0x02F
#define SIS8300BCM_ACCT_X_FLATTOP_FALLING_OFF           0x030
#define SIS8300BCM_ACCT_X_AUX_UPPER_THRESHOLD_OFF       0x031
#define SIS8300BCM_ACCT_X_AUX_LOWER_THRESHOLD_OFF       0x032
#define SIS8300BCM_ACCT_X_AUX_HYSTERESIS_THRESHOLD_OFF  0x033
#define SIS8300BCM_ACCT_X_BEAM_ABOVE_THRESHOLD_REG      0x034
#define SIS8300BCM_ACCT_X_TRIGGER_WIDTH_OFF             0x035
#define SIS8300BCM_ACCT_X_AUTO_FLATTOP_RISING_OFF       0x036
#define SIS8300BCM_ACCT_X_AUTO_FLATTOP_FALLING_OFF      0x037
#define SIS8300BCM_ACCT_X_AUTO_FLATTOP_ENABLE_OFF       0x038
#define SIS8300BCM_ACCT_X_BEAM_ABSENCE_OFF              0x039

/*
 DIFF block registers
 */
#define SIS8300BCM_DIFF_X_SOURCE_A_OFF                  0x000
#define SIS8300BCM_DIFF_X_SOURCE_B_OFF                  0x001
#define SIS8300BCM_DIFF_X_DELAY_OFF                     0x002
#define SIS8300BCM_DIFF_X_FAST_THRESHOLD_OFF            0x004
#define SIS8300BCM_DIFF_X_MEDIUM_THRESHOLD_OFF          0x005
#define SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_OFF         0x006
#define SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_OFF       0x007
#define SIS8300BCM_DIFF_X_SLOW_THRESHOLD_OFF            0x008
#define SIS8300BCM_DIFF_X_ALARM_CONTROL_OFF             0x009
#define SIS8300BCM_DIFF_X_FAST_MINIMUM_OFF              0x00A
#define SIS8300BCM_DIFF_X_FAST_MAXIMUM_OFF              0x00B
#define SIS8300BCM_DIFF_X_MEDIUM_MINIMUM_OFF            0x00C
#define SIS8300BCM_DIFF_X_MEDIUM_MAXIMUM_OFF            0x00D
#define SIS8300BCM_DIFF_X_SLOW_MINIMUM_OFF              0x00E
#define SIS8300BCM_DIFF_X_SLOW_MAXIMUM_OFF              0x00F
#define SIS8300BCM_DIFF_X_ALARM_HOLD_OFF                0x010
#define SIS8300BCM_DIFF_X_ALARM_FIRST_OFF               0x011
#define SIS8300BCM_DIFF_X_ALARM_DIRECT_OFF              0x012
#define SIS8300BCM_DIFF_X_RISING_WINDOW_START_REG       0x013
#define SIS8300BCM_DIFF_X_RISING_WINDOW_END_REG         0x014
#define SIS8300BCM_DIFF_X_FALLING_WINDOW_START_REG      0x015
#define SIS8300BCM_DIFF_X_FALLING_WINDOW_END_REG        0x016
#define SIS8300BCM_DIFF_X_BETWEEN_REG                   0x017
#define SIS8300BCM_DIFF_X_FAST_WINDOW_WIDTH_INV_OFF     0x019
#define SIS8300BCM_DIFF_X_MEDIUM_WINDOW_WIDTH_INV_OFF   0x01A
#define SIS8300BCM_DIFF_X_LEAKY_COFF_REG                0x01B
#define SIS8300BCM_DIFF_X_LEAKY_THR_REG                 0x01C

/*
 PROBE block registers
 */
#define SIS8300BCM_PROB_X_SOURCE_OFF                    0x000

/*
 FIBER block registers
 */
#define SIS8300BCM_FIBR_X_OUT_DATA_SELECT_OFF           0x000
#define SIS8300BCM_FIBR_X_STATUS_OFF                    0x001
#define SIS8300BCM_FIBR_X_RESET_OFF                     0x002

#ifdef __cplusplus
}
#endif

#endif /* SIS8300BCM_REG_H_ */
