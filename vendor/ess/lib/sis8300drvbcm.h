/**
 * Struck 8300 BCM Linux userspace library.
 * Copyright (C) 2016-2019  ESS ERIC
 * Copyright (C) 2014-2015  Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file sis8300drvbcm.h
 * @brief Header of sis8300 BCM userspace API.
 * @author Joao Paulo Martins, Hinko Kocevar
 *
 * Based on ADSIS8300BPM project
 *
 */

#ifndef SIS8300DRVBCM_H_
#define SIS8300DRVBCM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum {
    bcm_trg_src_soft,
    bcm_trg_src_internal,

    // XXX: Does not handle all 4x RJ45 external triggers on front panel
    //      which line out of 4 lines available is this?
    bcm_trg_src_rj45,

    // XXX: Does not handle all 8x LVDS external triggers on back plane
    //      where are the backplane lines 4 .. 7?
    bcm_trg_src_backplane0,
    bcm_trg_src_backplane1,
    bcm_trg_src_backplane2,
    bcm_trg_src_backplane3,
} sis8300drvbcm_trg_src;

/* BCM acquisition channel indexes */
#define SIS8300BCM_CHANNEL_RAW1         0
#define SIS8300BCM_CHANNEL_RAW2         1
#define SIS8300BCM_CHANNEL_RAW3         2
#define SIS8300BCM_CHANNEL_RAW4         3
#define SIS8300BCM_CHANNEL_RAW5         4
#define SIS8300BCM_CHANNEL_RAW6         5
#define SIS8300BCM_CHANNEL_RAW7         6
#define SIS8300BCM_CHANNEL_RAW8         7
#define SIS8300BCM_CHANNEL_RAW9         8
#define SIS8300BCM_CHANNEL_RAW10        9
#define SIS8300BCM_NUM_RAW_CHANNELS     10

#define SIS8300BCM_CHANNEL_PROC1        10
#define SIS8300BCM_CHANNEL_PROC2        11
#define SIS8300BCM_CHANNEL_PROC3        12
#define SIS8300BCM_CHANNEL_PROC4        13
#define SIS8300BCM_CHANNEL_PROC5        14
#define SIS8300BCM_CHANNEL_PROC6        15
#define SIS8300BCM_CHANNEL_PROC7        16
#define SIS8300BCM_CHANNEL_PROC8        17
#define SIS8300BCM_CHANNEL_PROC9        18
#define SIS8300BCM_CHANNEL_PROC10       19
#define SIS8300BCM_NUM_PROC_CHANNELS    10

#define SIS8300BCM_NUM_DIFF_CHANNELS    10

#define SIS8300BCM_CHANNEL_PROBE1       20
#define SIS8300BCM_CHANNEL_PROBE2       21
#define SIS8300BCM_CHANNEL_PROBE3       22
#define SIS8300BCM_CHANNEL_PROBE4       23
#define SIS8300BCM_NUM_PROBE_CHANNELS   4
#define SIS8300BCM_NUM_CHANNELS         24

#define SIS8300BCM_NUM_FIBER_CHANNELS   2

void sis8300drvbcm_set_sampling_frequency(sis8300drv_usr *sisuser, double frequency);
void sis8300drvbcm_get_sampling_frequency(sis8300drv_usr *sisuser, double *frequency);
int sis8300drvbcm_set_beam_trigger_source(sis8300drv_usr *sisuser, unsigned int source);
int sis8300drvbcm_get_beam_trigger_source(sis8300drv_usr *sisuser, unsigned int *source);
int sis8300drvbcm_set_acquisition_trigger_source(sis8300drv_usr *sisuser, unsigned int source);
int sis8300drvbcm_get_acquisition_trigger_source(sis8300drv_usr *sisuser, unsigned int *source);
int sis8300drvbcm_set_crate_id(sis8300drv_usr *sisuser, unsigned int crate_id);
int sis8300drvbcm_get_crate_id(sis8300drv_usr *sisuser, unsigned int *crate_id);
int sis8300drvbcm_set_pulse_width_filter(sis8300drv_usr *sisuser, double width);
int sis8300drvbcm_get_pulse_width_filter(sis8300drv_usr *sisuser, double *width);
int sis8300drvbcm_set_channel_trigger_source(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source);
int sis8300drvbcm_get_channel_trigger_source(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source);
int sis8300drvbcm_set_channel_number_of_samples(sis8300drv_usr *sisuser, unsigned int channel, unsigned int count);
int sis8300drvbcm_get_channel_number_of_samples(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *count);
int sis8300drvbcm_set_channel_memory_address(sis8300drv_usr *sisuser, unsigned int channel, unsigned int address);
int sis8300drvbcm_get_channel_memory_address(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *address);
int sis8300drvbcm_set_channel_sample_fraction_bits(sis8300drv_usr *sisuser, unsigned int channel, unsigned int count);
int sis8300drvbcm_get_channel_sample_fraction_bits(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *count);
int sis8300drvbcm_set_channel_conversion_factor(sis8300drv_usr *sisuser, unsigned int channel, double factor);
int sis8300drvbcm_get_channel_conversion_factor(sis8300drv_usr *sisuser, unsigned int channel, double *factor);
int sis8300drvbcm_set_channel_conversion_offset(sis8300drv_usr *sisuser, unsigned int channel, double offset);
int sis8300drvbcm_get_channel_conversion_offset(sis8300drv_usr *sisuser, unsigned int channel, double *offset);
int sis8300drvbcm_set_channel_decimation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int decimation);
int sis8300drvbcm_get_channel_decimation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *decimation);
int sis8300drvbcm_set_channel_recording(sis8300drv_usr *sisuser, unsigned int channel, unsigned int recording);
int sis8300drvbcm_get_channel_recording(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *recording);
int sis8300drvbcm_set_channel_scaling(sis8300drv_usr *sisuser, unsigned int channel, unsigned int scaling);
int sis8300drvbcm_get_channel_scaling(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *scaling);
int sis8300drvbcm_set_channel_data_type_converting(sis8300drv_usr *sisuser, unsigned int channel, unsigned int converting);
int sis8300drvbcm_get_channel_data_type_converting(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *converting);
int sis8300drvbcm_set_channel_adc_scale(sis8300drv_usr *sisuser, unsigned int channel, double factor);
int sis8300drvbcm_get_channel_adc_scale(sis8300drv_usr *sisuser, unsigned int channel, double *factor);
int sis8300drvbcm_set_channel_adc_offset(sis8300drv_usr *sisuser, unsigned int channel, int offset);
int sis8300drvbcm_get_channel_adc_offset(sis8300drv_usr *sisuser, unsigned int channel, int *offset);
int sis8300drvbcm_set_channel_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_channel_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_channel_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_channel_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_channel_fine_delay(sis8300drv_usr *sisuser, unsigned int channel, double delay);
int sis8300drvbcm_get_channel_fine_delay(sis8300drv_usr *sisuser, unsigned int channel, double *delay);
int sis8300drvbcm_get_channel_measured_pulse_width(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_get_channel_measured_pulse_charge(sis8300drv_usr *sisuser, unsigned int channel, double *charge);
int sis8300drvbcm_get_channel_measured_flattop_charge(sis8300drv_usr *sisuser, unsigned int channel, double *charge);
int sis8300drvbcm_get_channel_flattop_current(sis8300drv_usr *sisuser, unsigned int channel, double window, double charge, double *current);
int sis8300drvbcm_get_firmware_version(sis8300drv_usr *sisuser, unsigned int *version);
int sis8300drvbcm_get_firmware_git_hash(sis8300drv_usr *sisuser, unsigned int *git_hash);
int sis8300drvbcm_get_measured_sampling_frequency(sis8300drv_usr *sisuser, double *frequency);
int sis8300drvbcm_get_measured_trigger_period(sis8300drv_usr *sisuser, double *period);
int sis8300drvbcm_get_measured_trigger_width(sis8300drv_usr *sisuser, double *width);
int sis8300drvbcm_set_channel_droop_rate(sis8300drv_usr *sisuser, unsigned int channel, double droop);
int sis8300drvbcm_get_channel_droop_rate(sis8300drv_usr *sisuser, unsigned int channel, double *droop);
int sis8300drvbcm_set_channel_droop_compensating(sis8300drv_usr *sisuser, unsigned int channel, unsigned int compensating);
int sis8300drvbcm_get_channel_droop_compensating(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *compensating);
int sis8300drvbcm_set_channel_noise_filtering(sis8300drv_usr *sisuser, unsigned int channel, unsigned int filtering);
int sis8300drvbcm_get_channel_noise_filtering(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *filtering);
int sis8300drvbcm_set_channel_baselining_before_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int correcting);
int sis8300drvbcm_get_channel_baselining_before_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *correcting);
int sis8300drvbcm_set_channel_baselining_after_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int correcting);
int sis8300drvbcm_get_channel_baselining_after_droop_compensation(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *correcting);
int sis8300drvbcm_set_channel_dc_blocking(sis8300drv_usr *sisuser, unsigned int channel, unsigned int dcblocking);
int sis8300drvbcm_get_channel_dc_blocking(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *dcblocking);
int sis8300drvbcm_set_lut_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_lut_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_set_channel_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_errant_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_errant_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_alarm_upper_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_upper_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_lower_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_lower_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_errant_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_errant_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_trigger_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_trigger_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_limit_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_limit_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_adc_overflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_adc_overflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_adc_underflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_adc_underflow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_adc_stuck_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_adc_stuck_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_aiu_fault_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_aiu_fault_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_channel_alarm_charge_too_high_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_channel_alarm_charge_too_high_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_get_channel_alarm_hold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_channel_alarm_first(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_channel_alarm_direct(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_read_ram(sis8300drv_usr *sisuser, unsigned offset, unsigned size, void *data);
int sis8300drvbcm_set_minimum_trigger_period(sis8300drv_usr *sisuser, double period);
int sis8300drvbcm_get_minimum_trigger_period(sis8300drv_usr *sisuser, double *period);
int sis8300drvbcm_set_maximum_pulse_width(sis8300drv_usr *sisuser, unsigned int width);
int sis8300drvbcm_get_maximum_pulse_width(sis8300drv_usr *sisuser, unsigned int *width);
int sis8300drvbcm_set_alarms_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarms_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_clear_alarms(sis8300drv_usr *sisuser);
int sis8300drvbcm_set_alarm_auxiliary_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarm_auxiliary_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_set_alarm_processing_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarm_processing_clock_out_of_range_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_set_alarm_trigger_too_wide_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarm_trigger_too_wide_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_set_alarm_trigger_too_narrow_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarm_trigger_too_narrow_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_set_alarm_trigger_period_too_short_control(sis8300drv_usr *sisuser, unsigned int disable);
int sis8300drvbcm_get_alarm_trigger_period_too_short_control(sis8300drv_usr *sisuser, unsigned int *disable);
int sis8300drvbcm_get_alarm_hold(sis8300drv_usr *sisuser, unsigned int *alarms);
int sis8300drvbcm_get_alarm_first(sis8300drv_usr *sisuser, unsigned int *alarms);
int sis8300drvbcm_get_alarm_direct(sis8300drv_usr *sisuser, unsigned int *alarms);
int sis8300drvbcm_set_status_ready(sis8300drv_usr *sisuser, unsigned int status);
int sis8300drvbcm_get_status_bits(sis8300drv_usr *sisuser, unsigned int *status);
int sis8300drvbcm_get_channel_beam_exists(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *exist);
int sis8300drvbcm_set_channel_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int length);
int sis8300drvbcm_get_channel_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *length);
int sis8300drvbcm_set_channel_lower_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_channel_lower_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_channel_lower_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_channel_lower_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_channel_errant_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_channel_errant_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_channel_errant_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_channel_errant_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_channel_aux_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_aux_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_aux_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_aux_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_aux_hysteresis_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_aux_hysteresis_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_channel_beam_above_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_beam_above_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_get_channel_trigger_width(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_set_channel_auto_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_channel_auto_flattop_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_channel_auto_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_channel_auto_flattop_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_channel_auto_flattop_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable);
int sis8300drvbcm_get_channel_auto_flattop_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable);
int sis8300drvbcm_set_channel_beam_absence(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable);
int sis8300drvbcm_get_channel_beam_absence(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable);
int sis8300drvbcm_set_channel_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double coefficient);
int sis8300drvbcm_get_channel_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double *coefficient);
int sis8300drvbcm_set_channel_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_channel_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_probe_setup(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source);
int sis8300drvbcm_get_probe_setup(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source);
int sis8300drvbcm_set_timestamp(sis8300drv_usr *sisuser, unsigned int sec, unsigned int nsec);
int sis8300drvbcm_get_timestamp(sis8300drv_usr *sisuser, unsigned int *sec, unsigned int *nsec);
int sis8300drvbcm_set_beam_mode(sis8300drv_usr *sisuser, unsigned int mode);
int sis8300drvbcm_get_beam_mode(sis8300drv_usr *sisuser, unsigned int *mode);
int sis8300drvbcm_set_beam_destination(sis8300drv_usr *sisuser, unsigned int dest);
int sis8300drvbcm_get_beam_destination(sis8300drv_usr *sisuser, unsigned int *dest);
int sis8300drvbcm_set_differential_source_a(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source);
int sis8300drvbcm_get_differential_source_a(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source);
int sis8300drvbcm_set_differential_source_b(sis8300drv_usr *sisuser, unsigned int channel, unsigned int source);
int sis8300drvbcm_get_differential_source_b(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *source);
int sis8300drvbcm_set_differential_delay(sis8300drv_usr *sisuser, unsigned int channel, double delay);
int sis8300drvbcm_get_differential_delay(sis8300drv_usr *sisuser, unsigned int channel, double *delay);
int sis8300drvbcm_set_differential_fast_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_differential_fast_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_get_differential_fast_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
int sis8300drvbcm_get_differential_fast_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);
int sis8300drvbcm_set_differential_alarm_fast_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_differential_alarm_fast_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_differential_medium_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_differential_medium_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_get_differential_medium_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
int sis8300drvbcm_get_differential_medium_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);
int sis8300drvbcm_set_differential_alarm_medium_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_differential_alarm_medium_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_set_differential_slow_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_differential_slow_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_get_differential_slow_minimum(sis8300drv_usr *sisuser, unsigned int channel, double *minimum);
int sis8300drvbcm_get_differential_slow_maximum(sis8300drv_usr *sisuser, unsigned int channel, double *maximum);
int sis8300drvbcm_set_differential_alarm_slow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_differential_alarm_slow_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_get_differential_alarm_hold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_differential_alarm_first(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_differential_alarm_direct(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_set_differential_rising_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_differential_rising_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_differential_rising_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_differential_rising_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_differential_falling_window_start(sis8300drv_usr *sisuser, unsigned int channel, double start);
int sis8300drvbcm_get_differential_falling_window_start(sis8300drv_usr *sisuser, unsigned int channel, double *start);
int sis8300drvbcm_set_differential_falling_window_end(sis8300drv_usr *sisuser, unsigned int channel, double end);
int sis8300drvbcm_get_differential_falling_window_end(sis8300drv_usr *sisuser, unsigned int channel, double *end);
int sis8300drvbcm_set_differential_ws_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between);
int sis8300drvbcm_get_differential_ws_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between);
int sis8300drvbcm_set_differential_emu_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between);
int sis8300drvbcm_get_differential_emu_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between);
int sis8300drvbcm_set_differential_rfq_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int between);
int sis8300drvbcm_get_differential_rfq_between(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *between);
int sis8300drvbcm_set_lut_maximum_pulse_length(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, unsigned int length);
int sis8300drvbcm_set_lut_lower_threshold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, double threshold);
int sis8300drvbcm_set_lut_upper_threshold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, unsigned int beam_mode, double threshold);
int sis8300drvbcm_set_lut_min_trigger_period(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_mode, double min_trigger_period);
int sis8300drvbcm_set_lut_interlock_masking(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_dest, unsigned int mask);
int sis8300drvbcm_set_lut_interlock_enable(sis8300drv_usr *sisuser, unsigned int index, unsigned int beam_dest, unsigned int mask);
int sis8300drvbcm_set_clock_source(sis8300drv_usr *sisuser, sis8300drv_clk_src source);
int sis8300drvbcm_get_clock_source(sis8300drv_usr *sisuser, sis8300drv_clk_src *source);
int sis8300drvbcm_set_differential_fast_window_width(sis8300drv_usr *sisuser, unsigned int channel, double width);
int sis8300drvbcm_get_differential_fast_window_width(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_set_differential_medium_window_width(sis8300drv_usr *sisuser, unsigned int channel, double width);
int sis8300drvbcm_get_differential_medium_window_width(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_set_differential_fast_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double width);
int sis8300drvbcm_get_differential_fast_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_set_differential_medium_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double width);
int sis8300drvbcm_get_differential_medium_window_width_inverse(sis8300drv_usr *sisuser, unsigned int channel, double *width);
int sis8300drvbcm_set_differential_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double coefficient);
int sis8300drvbcm_get_differential_leaky_coefficient(sis8300drv_usr *sisuser, unsigned int channel, double *coefficient);
int sis8300drvbcm_set_differential_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double threshold);
int sis8300drvbcm_get_differential_leaky_threshold(sis8300drv_usr *sisuser, unsigned int channel, double *threshold);
int sis8300drvbcm_set_differential_alarm_leaky_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int disable);
int sis8300drvbcm_get_differential_alarm_leaky_control(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *disable);
int sis8300drvbcm_get_differential_alarm_hold(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_differential_alarm_first(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_differential_alarm_direct(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *alarms);
int sis8300drvbcm_get_differential_interlock_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable);
int sis8300drvbcm_set_enable_calibration_pulse(sis8300drv_usr *sisuser, unsigned int enable_calibration_pulse);
int sis8300drvbcm_get_calibration_sample(sis8300drv_usr *sisuser, unsigned int channel, unsigned int index, double *sample);
int sis8300drvbcm_set_fiber_out_data_select(sis8300drv_usr *sisuser, unsigned int channel, unsigned int selection);
int sis8300drvbcm_get_fiber_out_data_select(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *selection);
int sis8300drvbcm_set_fiber_out_data_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int enable);
int sis8300drvbcm_get_fiber_out_data_enable(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *enable);
int sis8300drvbcm_get_fiber_sfp_present(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *sfp_present);
int sis8300drvbcm_get_fiber_lane_up(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *lane_up);
int sis8300drvbcm_get_fiber_channel_up(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *channel_up);
int sis8300drvbcm_get_fiber_hardware_error(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *hardware_error);
int sis8300drvbcm_get_fiber_software_error(sis8300drv_usr *sisuser, unsigned int channel, unsigned int *software_error);
int sis8300drvbcm_set_fiber_reset(sis8300drv_usr *sisuser, unsigned int channel, unsigned int reset);

#ifdef __cplusplus
}
#endif

#endif /* SIS8300DRVBCM_H_ */
