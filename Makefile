#Makefile at top of application tree
TOP = .
include $(TOP)/configure/CONFIG
DIRS := $(DIRS) $(filter-out $(DIRS), configure)
DIRS := $(DIRS) $(filter-out $(DIRS), $(wildcard *App))
DIRS := $(DIRS) $(filter-out $(DIRS), vendor)

define DIR_template
 $(1)_DEPEND_DIRS = configure
endef
$(foreach dir, $(filter-out configure,$(DIRS)),$(eval $(call DIR_template,$(dir))))

define DIR_template_vendor
 $(1)_DEPEND_DIRS += vendor
endef
$(foreach dir, $(filter-out configure vendor,$(DIRS)),$(eval $(call DIR_template_vendor,$(dir))))

include $(TOP)/configure/RULES_TOP
